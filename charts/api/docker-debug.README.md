# HOW TO

Steps 2,3 can be replaced with running bash script `run_docker_debug.sh`
1. Build docker image ./dockerfile.debug with tag `pbl-chart-api-debug`
2. Run `docker run -it --rm -p 5000:5000 --name pbl-chart-api-debug pbl-chart-api-debug`
3. Start debugging with vs code using `.NET Core Docker Attach` to process with executed .dll path 
