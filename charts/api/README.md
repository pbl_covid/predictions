# ABOUT

Main API service for COVID predictions.

# REQUIREMENTS

- MCR 2020b
- ASP.NET Core 3.1 runtime

__OR__ 

- docker 20.10

# HOW TO RUN

1. Windows locally 
- run PblCovid19Api project, either via Visual Studio 2019 or using command `dotnet run`

2. Linux locally 
- run PblCovid19Api project, either via Visual Studio 2019 or using command `dotnet run`

3. Docker
- Docker image is using runtime from private azure repository for now: `pblregistry.azurecr.io/aspnetcore31-matlab-runtime:1.0`. You can create your own image using: 
`docker/docker-images/aspnetcore3.1_matlab-runtime.PROD.dockerfile` and replacing old image with created one in dockerfile.prod file.
- Run following commands:
    - `docker build -t charts-api -f dockerfile.prod .`
    - `docker run -d -p 5000:80 charts-api`


## Updating SIR model
After updating SIR model you will have to compile it to executable file (.exe for windows and .sh + binary file for linux)

1. Windows locally
Replace existing executable file in `PblCovid19Api/Resources/Windows`, keep file name the same. 

2. Linux locally / Docker
Replace existing executable files in `PblCovid19Api/Resources/Linux`, keep file names the same. For docker you should also replace .zip file with new code (also keep name the same) 

__You can also check debugging Linux version on using docker container in docker-debug.README.md__

# ENDPOINTS
If you want to check existing endpoints use url `/swagger/index.html`

# COMMON PROBLEMS
__! BE CAREFUL - SIR bash script has often its whitespace edited due to end of line environment differences in Windows and Linux. It may cause script to stop working.__