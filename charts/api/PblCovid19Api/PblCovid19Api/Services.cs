﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.DependencyInjection;
using PblCovid19Api.Common;
using PblCovid19Api.Infrastructure;
using PblCovid19Api.Logic;
using PblCovid19Api.ModelProxy;

namespace PblCovid19Api
{
    public static class Services
    {
        /// <summary>
        /// Register all services from whole solution.
        /// </summary>
        public static IServiceCollection AddServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddLogicServices();
            serviceCollection.AddModelServices();
            serviceCollection.AddInfrastructureServices();
            serviceCollection.AddCommonServices();

            return serviceCollection;
        }
    }
}
