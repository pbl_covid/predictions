﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;
using System.Collections;

namespace PblCovid19Api.Models.Api
{
    /// <summary>
    /// Error model for ResponseModel
    /// </summary>
    public class ErrorModel
    {
        public string Message { get; set; }
        public IDictionary Data { get; set; }
        public string StackTrace { get; set; }

        public ErrorModel(Exception ex)
        {
            Message = ex.Message;
            Data = ex.Data;
            StackTrace = ex.StackTrace;
        }
    }
}
