﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.Collections.Generic;

namespace PblCovid19Api.Models.Api
{
    /// <summary>
    /// Response model from API.
    /// </summary>
    public sealed class ResponseModel
    {
        public object Data { get; set; }
        public IEnumerable<ErrorModel> Errors { get; set; }

        public ResponseModel(object data)
        {
            Data = data;
        }

        public ResponseModel(params ErrorModel[] errors)
        {
            Errors = errors;
        }
    }
}
