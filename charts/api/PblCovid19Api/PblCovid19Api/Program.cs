// Gliwice, Politechnika Slaska 2020/2021
// Paczu�a Piotr, �abia�owicz Marek

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace PblCovid19Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
.ConfigureWebHostDefaults(webBuilder =>
{
    webBuilder.UseStartup<Startup>();
});
        }
    }
}
