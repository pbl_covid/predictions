﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.AspNetCore.Mvc;
using PblCovid19Api.Logic.Restrictions;
using System.Threading.Tasks;

namespace PblCovid19Api.Controllers
{
    /// <summary>
    /// Controller related to restrictions.
    /// </summary>
    [Produces("application/json")]
    [ApiController]
    [Route("api/[controller]")]
    public class RestrictionsController : BaseController
    {
        private readonly IRestrictionsProvider _restrictionsProvider;

        public RestrictionsController(IRestrictionsProvider restrictionsProvider)
        {
            _restrictionsProvider = restrictionsProvider;
        }

        /// <summary>
        /// Gets list of all avaiable enum restrictions and their values
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var restrictions = _restrictionsProvider.GetAvailableRestrictions();
            return Ok(restrictions);
        }
        
        /// <summary>
        /// Gets provided country's last registered restrictions.
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        [HttpGet("{countryCode}")]
        public async Task<IActionResult> GetCurrentRestrictionsAsync(string countryCode)
        {
            var restrictions = await _restrictionsProvider.GetCountryCurrentRestrictionsAsync(countryCode).ConfigureAwait(false);
            return Ok(restrictions);
        }
    }
}
