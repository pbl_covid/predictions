﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.AspNetCore.Mvc;
using PblCovid19Api.Logic.Countries;
using System.Threading.Tasks;

namespace PblCovid19Api.Controllers
{
    /// <summary>
    /// Controller related to countries.
    /// </summary>
    [Produces("application/json")]
    [ApiController]
    [Route("api/[controller]")]
    public class CountriesController : BaseController
    {
        private readonly ICountriesProvider _countriesProvider;

        public CountriesController(ICountriesProvider countriesProvider)
        {
            _countriesProvider = countriesProvider;
        }

        /// <summary>
        /// Gets a list of all available countries.
        /// </summary>
        /// <returns>List of countries. Name, code and alpha3 code.</returns>
        [HttpGet]
        public async Task<IActionResult> GetAvailableCountriesAsync()
        {
            var models = await _countriesProvider.GetAvailableCountriesAsync().ConfigureAwait(false);
            return Ok(models);
        }
    }
}
