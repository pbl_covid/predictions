﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.AspNetCore.Mvc;
using PblCovid19Api.ModelProxy.Models;

namespace PblCovid19Api.Controllers
{
    /// <summary>
    /// Controller related to prediction models.
    /// </summary>
    [Produces("application/json")]
    [ApiController]
    [Route("api/[controller]")]
    public class ModelsController : BaseController
    {
        private readonly IEstimationModelProxy _modelFactory;

        public ModelsController(IEstimationModelProxy modelFactory)
        {
            _modelFactory = modelFactory;
        }

        /// <summary>
        /// Gets a list of all registered prediction models.
        /// </summary>
        [HttpGet]
        public IActionResult GetAvailableModels()
        {
            var models = _modelFactory.GetAvailableModels();
            return Ok(models);
        }
    }
}
