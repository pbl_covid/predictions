﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.AspNetCore.Mvc;
using PblCovid19Api.Models.Api;

namespace PblCovid19Api.Controllers
{
    /// <summary>
    /// Base class for controllers. It has overriden `OK(object)`.
    /// </summary>
    public abstract class BaseController : ControllerBase
    {
        public override OkObjectResult Ok(object payload)
        {
            return base.Ok(new ResponseModel(payload));
        }
    }
}
