﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.AspNetCore.Mvc;
using PblCovid19Api.Common.Models.Charts;
using PblCovid19Api.Logic.Charts;
using System.Threading.Tasks;

namespace PblCovid19Api.Controllers
{
    /// <summary>
    /// Controller related to charts.
    /// </summary>
    [Produces("application/json")]
    [ApiController]
    [Route("api/[controller]")]
    public class ChartsController : BaseController
    {
        private readonly ICountryChartProvider _countryChartsProvider;

        public ChartsController(ICountryChartProvider countryChartsProvider)
        {
            _countryChartsProvider = countryChartsProvider;
        }

        /// <summary>
        /// Returns a chart data.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("country")]
        public async Task<IActionResult> GetSimulationForCountryAsync(CountryChartRequest request)
        {
            var result = await _countryChartsProvider.GetCountryChartAsync(request).ConfigureAwait(false);
            return Ok(result);
        }
    }
}
