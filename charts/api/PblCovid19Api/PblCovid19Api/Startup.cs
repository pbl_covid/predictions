// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using PblCovid19Api.Infrastructure.Database;
using System;
using System.IO;
using System.Reflection;

namespace PblCovid19Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Environment.GetEnvironmentVariable("SQL_STRING") ?? Configuration.GetConnectionString("DefaultConnection");

            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddDbContext<PblCovidDbContext>(options
                => options.UseSqlServer(connectionString),
                ServiceLifetime.Transient);

            services.AddCors();
            services.AddMvc().AddNewtonsoftJson(opts =>
            {
                //opts.SerializerSettings.Converters.Add(new StringEnumConverter());
                opts.SerializerSettings.DateFormatString = "yyyy-MM-dd";
            });

            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(GlobalExceptionFilter));
            });

            services.AddLogging(builder =>
            {
                builder.AddFile("Logs/{Date}.log");
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Pbl Covid19 Api",
                    Description = "PBL - Modelowanie i przewidywanie rozwoju epidemii na przykładzie pandemii sars-cov-2. Gliwice, Politechnika Slaska, 2020/2021",
                    Contact = new OpenApiContact
                    {
                        Name = "Paczuła Piotr, Marek Żabiałowicz",
                    },
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
            services.AddServices();
            services.AddHttpClient();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("CorsPolicy");

            app.UseSwagger(c =>
            {
                c.SerializeAsV2 = true;
            });

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "PBL - Modelowanie i przewidywanie rozwoju epidemii na przykładzie pandemii sars-cov-2");
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
