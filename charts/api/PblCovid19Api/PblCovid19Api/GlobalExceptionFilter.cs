﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using PblCovid19Api.Common.Models.Exceptions;
using PblCovid19Api.Controllers;
using PblCovid19Api.Models.Api;
using System;
using System.Net;

namespace PblCovid19Api
{
    /// <summary>
    /// Global Exception filter. It handles `BaseException`.
    /// </summary>
    public class GlobalExceptionFilter : BaseController, IExceptionFilter
    {
        private readonly ILogger _logger;

        public GlobalExceptionFilter(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger(GetType());
        }

        public void OnException(ExceptionContext context)
        {
            context.Result = HandleException(context.Exception);
        }

        private IActionResult HandleException(Exception ex)
        {
            return ex switch
            {
                BaseException e => BaseException(e),
                _ => InternalServerError(ex)
            };
        }

        private IActionResult BaseException(BaseException e)
        {
            _logger.LogError(e, $"{typeof(BaseException)}");
            return StatusCode((int)e.StatusCode, new ResponseModel(errors: new ErrorModel(e)));
        }

        private IActionResult InternalServerError(Exception e)
        {
            _logger.LogError(e, $"Unexpected exception");
            return StatusCode((int)HttpStatusCode.InternalServerError, new ResponseModel(errors: new ErrorModel(e)));
        }
    }
}
