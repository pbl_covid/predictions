﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using NUnit.Framework;
using PblCovid19Api.Common.Models.Charts;
using PblCovid19Api.Logic.Charts;
using System.Threading.Tasks;

namespace PblCovid19Api.Logic.Tests.Charts
{
    [TestFixture]
    internal class CountryChartProviderTests
    {
        private IFixture fixture;

        [SetUp]
        public void SetUp()
        {
            fixture = new Fixture().Customize(new AutoMoqCustomization());
        }

        private ICountryChartProvider CreateSut()
        {
            return fixture.Create<CountryChartProvider>();
        }

        [Test]
        public async Task Test()
        {
            var request = new CountryChartRequest
            {

            };

            var sut = CreateSut();
            var result = await sut.GetCountryChartAsync(request).ConfigureAwait(false);
            result.Should().NotBeNull();
        }
    }
}
