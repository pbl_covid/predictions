﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using FluentAssertions;
using NUnit.Framework;
using PblCovid19Api.Logic.Charts.Errors;

namespace PblCovid19Api.Logic.Tests.Charts.Errors
{
    [TestFixture]
    internal class ErrorsCalculatorTests
    {
        private readonly int[] first = new[] { 1, 2, 3, 4, 5 };
        private readonly int[] second = new[] { 1, 2, 3, 4, 5 };
        private readonly int[] third = new[] { 3, 4, 5, 6, 7 };

        private ErrorsCalculator CreateSut()
        {
            return new ErrorsCalculator();
        }

        [Test]
        public void ShouldBeZero()
        {
            var sut = CreateSut();
            var result = sut.CalculateAllErrors(first, second);

            result.MeanAbsoluteError.Should().Be(0);
            result.MeanError.Should().Be(0);
            result.MeanSquareError.Should().Be(0);
        }

        [Test]
        public void MAE()
        {
            var sut = CreateSut();
            var result = sut.CalculateMAE(first, third);
            result.Should().Be(2);
        }

        [Test]
        public void MSE()
        {
            var sut = CreateSut();
            var result = sut.CalculateMSE(first, third);
            result.Should().Be(4);
        }

        [Test]
        public void ME()
        {
            var sut = CreateSut();
            var result = sut.CalculateME(first, third);
            result.Should().Be(-2);
        }
    }
}
