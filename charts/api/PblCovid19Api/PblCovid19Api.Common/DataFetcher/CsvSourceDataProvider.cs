﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Helpers.Files;
using PblCovid19Api.Common.Models.Csv;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PblCovid19Api.Common.DataFetcher
{
    internal sealed class CsvSourceDataProvider : ICsvSourceDataProvider
    {
        private readonly IDataFetcherService _dataFetcher;
        private readonly ICsvReaderHelper _csvReader;

        public CsvSourceDataProvider(IDataFetcherService dataFetcher, ICsvReaderHelper csvReader)
        {
            _dataFetcher = dataFetcher;
            _csvReader = csvReader;
        }

        public async Task<IEnumerable<CsvDataRecord>> GetCsvSourceDataAsync(string countryAlpha)
        {
            IEnumerable<CsvDataRecord> data = null;

            await _dataFetcher.UseCsvDataFileAsync(countryAlpha, async file =>
            {
                using (var stream = file.OpenText())
                {
                    data = await _csvReader.ReadCsvAsync<CsvDataRecord>(stream).ConfigureAwait(false);
                }
            }).ConfigureAwait(false);

            return data;
        }
    }
}
