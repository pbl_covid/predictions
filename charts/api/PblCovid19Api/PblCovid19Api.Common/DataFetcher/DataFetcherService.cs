﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.Configuration;
using PblCovid19Api.Common.Extensions;
using PblCovid19Api.Common.Models;
using PblCovid19Api.Common.Models.Exceptions;
using PblCovid19Api.Common.Utilities;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace PblCovid19Api.Common.DataFetcher
{
    internal sealed class DataFetcherService : IDataFetcherService
    {
        private readonly string[] _sourceCsvFolderPath;
        private readonly string _dataFetcherUrl;
        private readonly IFileService _fileService;
        private readonly IHttpClientFactory _clientFactory;

        public DataFetcherService(IFileService fileService, IConfiguration configuration, IHttpClientFactory clientFactory)
        {
            _dataFetcherUrl = Environment.GetEnvironmentVariable("FETCHER_URL") ?? configuration[ConfigurationKeys.DataFetcherUrl];
            _fileService = fileService;
            _clientFactory = clientFactory;
            _sourceCsvFolderPath = configuration[ConfigurationKeys.InputCsvPath].PathToFolderArray();
            EnsureSourceFolderCreated();
        }

        private void EnsureSourceFolderCreated()
        {
            _fileService.CreateDirectory(_sourceCsvFolderPath);
        }

        public async Task UseCsvDataFileAsync(string countryAlpha, Func<FileInfo, Task> action)
        {
            await _fileService.UseFileAsync(async file =>
            {
                if (file == null
                 || !file.Exists
                 || file.LastWriteTimeUtc.Date < DateTime.UtcNow.Date)
                {
                    file = await FetchAndSaveCsvDataAsync(countryAlpha);
                }
                await action(file);
            }, GetFileName(countryAlpha), _sourceCsvFolderPath);
        }

        public async Task UseCsvDataFileAsync(string countryAlpha, Action<FileInfo> action)
        {
            await _fileService.UseFileAsync(async file =>
            {
                if (file == null
                 || !file.Exists
                 || file.LastWriteTimeUtc.Date < DateTime.UtcNow.Date)
                {
                    file = await FetchAndSaveCsvDataAsync(countryAlpha);
                }
                action(file);
            }, GetFileName(countryAlpha), _sourceCsvFolderPath);
        }

        private HttpClient CreateHttpClient()
        {
            var client = _clientFactory.CreateClient();
            client.BaseAddress = new Uri(_dataFetcherUrl);
            client.Timeout = TimeSpan.FromSeconds(30);
            return client;
        }

        private string CreateQueryString(string countryAlpha, DateTime? startDate = null, DateTime? endDate = null)
        {
            // alpha=USA&start=2021-01-01&end=2021-01-02
            var queryString = HttpUtility.ParseQueryString(string.Empty);
            queryString.Add("alpha", countryAlpha);

            if (startDate.HasValue)
            {
                queryString.Add("start", startDate.Value.ToString("YYYY-mm-dd"));
            }
            if (endDate.HasValue)
            {
                queryString.Add("end", endDate.Value.ToString("YYYY-mm-dd"));
            }

            return queryString.ToString();
        }

        private Task<FileInfo> SaveFileAsync(Stream fs, string countryAlpha)
        {
            var fileName = GetFileName(countryAlpha);
            return _fileService.CreateFileAsync(fs, fileName, true, true, _sourceCsvFolderPath);
        }

        private string GetFileName(string countryAlpha)
        {
            return $"{countryAlpha}.csv";
        }

        private async Task<FileInfo> FetchAndSaveCsvDataAsync(string countryAlpha)
        {
            using (var client = CreateHttpClient())
            {
                var response = await client.GetAsync($"/csv?{CreateQueryString(countryAlpha)}");
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    try
                    {
                        return await SaveFileAsync(await response.Content.ReadAsStreamAsync(), countryAlpha);
                    }
                    catch (FileOperationException)
                    {
                        // log error
                        throw;
                    }
                }
                else
                {
                    // ...
                    throw new Exception();
                }
            }
        }
    }
}
