﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Csv;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PblCovid19Api.Common.DataFetcher
{
    public interface ICsvSourceDataProvider
    {
        Task<IEnumerable<CsvDataRecord>> GetCsvSourceDataAsync(string countryAlpha);
    }
}