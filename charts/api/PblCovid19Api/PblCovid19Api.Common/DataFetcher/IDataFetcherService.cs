﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;
using System.IO;
using System.Threading.Tasks;

namespace PblCovid19Api.Common.DataFetcher
{
    public interface IDataFetcherService
    {
        Task UseCsvDataFileAsync(string countryAlpha, Func<FileInfo, Task> action);
        Task UseCsvDataFileAsync(string countryAlpha, Action<FileInfo> action);
    }
}
