﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.DependencyInjection;
using PblCovid19Api.Common.DataFetcher;
using PblCovid19Api.Common.Helpers.Files;
using PblCovid19Api.Common.Http;
using PblCovid19Api.Common.Utilities;

namespace PblCovid19Api.Common
{
    public static class Services
    {
        /// <summary>
        /// Register all services from PblCovid19Api.Common.
        /// </summary>
        public static IServiceCollection AddCommonServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IHttpClientWrapper, HttpClientWrapper>();
            serviceCollection.AddTransient<IFileHelper, FileHelper>();
            serviceCollection.AddTransient<ICsvReaderHelper, CsvReaderHelper>();
            serviceCollection.AddSingleton<IFileService, FileService>();
            serviceCollection.AddTransient<IDataFetcherService, DataFetcherService>();
            serviceCollection.AddTransient<ICsvSourceDataProvider, CsvSourceDataProvider>();

            return serviceCollection;
        }
    }
}
