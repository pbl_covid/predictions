﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

namespace PblCovid19Api.Common.Extensions
{
    public static class StringExtensions
    {
        public static string[] PathToFolderArray(this string path)
        {
            return path.Replace("/", ":")
                           .Replace("\\", ":")
                           .Split(":");
        }
    }
}
