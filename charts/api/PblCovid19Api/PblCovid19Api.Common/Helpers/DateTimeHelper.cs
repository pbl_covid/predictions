﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;

namespace PblCovid19Api.Common.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime GetDateTimeFromUnixMiliseconds(long ticks)
        {
            var firstDate = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return firstDate.AddMilliseconds(ticks);
        }
    }
}
