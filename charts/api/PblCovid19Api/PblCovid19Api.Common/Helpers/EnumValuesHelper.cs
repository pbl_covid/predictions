﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;
using System.Collections.Generic;
using System.Linq;

namespace PblCovid19Api.Common.Helpers
{
    public static class EnumValuesHelper
    {
        public static IEnumerable<TEnum> GetAllEnumValues<TEnum>() where TEnum : struct, Enum
        {
            return Enum.GetValues(typeof(TEnum)).Cast<TEnum>();
        }
    }
}
