﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace PblCovid19Api.Common.Helpers.Files
{
    public interface ICsvReaderHelper
    {
        Task<IEnumerable<T>> ReadCsvAsync<T>(string path);
        Task<IEnumerable<T>> ReadCsvAsync<T>(StreamReader streamReader);
    }
}