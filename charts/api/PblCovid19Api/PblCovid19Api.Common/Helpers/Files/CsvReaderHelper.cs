﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using CsvHelper;
using PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment;
using PblCovid19Api.Common.Models.Restrictions.EconomicResponse;
using PblCovid19Api.Common.Models.Restrictions.HealthSystem;
using PblCovid19Api.Common.Models.Restrictions.Miscellaneous;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PblCovid19Api.Common.Helpers.Files
{
    internal class CsvReaderHelper : ICsvReaderHelper
    {
        private const string NULL = "null";

        public async Task<IEnumerable<T>> ReadCsvAsync<T>(string path)
        {
            using var streamReader = File.OpenText(path);
            using (var csvReader = new CsvReader(streamReader, CultureInfo.InvariantCulture))
            {
                RegisterNullValues(csvReader);

                var records = csvReader.GetRecordsAsync<T>();
                return await records.ToListAsync().ConfigureAwait(false);
            }
        }

        public async Task<IEnumerable<T>> ReadCsvAsync<T>(StreamReader streamReader)
        {
            using (var csvReader = new CsvReader(streamReader, CultureInfo.InvariantCulture))
            {
                RegisterNullValues(csvReader);

                var records = csvReader.GetRecordsAsync<T>();
                return await records.ToListAsync().ConfigureAwait(false);
            }
        }

        private void RegisterNullValues(CsvReader csvReader)
        {
            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<long?>().NullValues.Add(NULL);

            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<SchoolClosing?>().NullValues.Add(NULL);
            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<WorkplaceClosing?>().NullValues.Add(NULL);
            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<CancelPublicEvents?>().NullValues.Add(NULL);
            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<RestrictionsOnGatherings?>().NullValues.Add(NULL);
            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<ClosePublicTransport?>().NullValues.Add(NULL);
            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<StayAtHome?>().NullValues.Add(NULL);
            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<InternalMovement?>().NullValues.Add(NULL);
            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<InternationalTravelControls?>().NullValues.Add(NULL);

            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<PublicInfoCampaings?>().NullValues.Add(NULL);
            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<TestingPolicy?>().NullValues.Add(NULL);
            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<ContactTracing?>().NullValues.Add(NULL);

            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<E1?>().NullValues.Add(NULL);
            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<E2?>().NullValues.Add(NULL);

            csvReader.Configuration.TypeConverterOptionsCache.GetOptions<M1?>().NullValues.Add(NULL);
        }
    }
}
