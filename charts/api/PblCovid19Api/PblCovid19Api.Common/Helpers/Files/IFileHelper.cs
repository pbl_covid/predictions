﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

namespace PblCovid19Api.Common.Helpers.Files
{
    public interface IFileHelper
    {
        bool CheckIfFileExists(string path);
        string GetFullPathForConfigurationKey(string configurationKey);
    }
}