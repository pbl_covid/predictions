﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.Configuration;
using System.IO;

namespace PblCovid19Api.Common.Helpers.Files
{
    internal class FileHelper : IFileHelper
    {
        private readonly IConfiguration configuration;

        public FileHelper(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public bool CheckIfFileExists(string path)
        {
            return File.Exists(path);
        }

        public string GetFullPathForConfigurationKey(string configurationKey)
        {
            return Path.GetFullPath(configuration[configurationKey]);
        }
    }
}
