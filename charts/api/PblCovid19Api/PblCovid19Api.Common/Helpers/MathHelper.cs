﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;
using System.Collections.Generic;
using System.Linq;

namespace PblCovid19Api.Common.Helpers
{
    public static class MathHelper
    {
        public static double StandardDeviation(IEnumerable<double> numbers)
        {
            var mean = numbers.Average();

            return Math.Sqrt(numbers.Average(n => Math.Pow(n - mean, 2)));
        }
    }
}
