﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.Threading.Tasks;

namespace PblCovid19Api.Common.Http
{
    /// <summary>
    /// Use this interface if you want to make an HTTP request.
    /// </summary>
    public interface IHttpClientWrapper
    {
        Task<T> GetAsync<T>(string url);
        Task<T> PostAsync<T>(string url, object payLoad);
    }
}