﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PblCovid19Api.Common.Http
{
    internal class HttpClientWrapper : IHttpClientWrapper
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly ILogger _logger;

        public HttpClientWrapper(IHttpClientFactory clientFactory, ILoggerFactory loggerFactory)
        {
            _clientFactory = clientFactory;
            _logger = loggerFactory.CreateLogger<HttpClientWrapper>();
        }

        private HttpClient GetHttpClient()
        {
            return _clientFactory.CreateClient();
        }

        public async Task<T> GetAsync<T>(string url)
        {
            using (var client = GetHttpClient())
            {
                using (var response = await client.GetAsync(url).ConfigureAwait(false))
                {
                    await CheckResponseAsync(response).ConfigureAwait(false);

                    var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    return JsonConvert.DeserializeObject<T>(content);
                }
            }
        }

        public async Task<T> PostAsync<T>(string url, object payload)
        {
            using (var json = PrepareContent(payload))
            {
                using (var client = GetHttpClient())
                {
                    using (var response = await client.PostAsync(url, json).ConfigureAwait(false))
                    {
                        await CheckResponseAsync(response).ConfigureAwait(false);

                        var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        return JsonConvert.DeserializeObject<T>(content);
                    }
                }
            }
        }

        private HttpContent PrepareContent(object paylaod)
        {
            var json = JsonConvert.SerializeObject(paylaod);
            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        private async Task CheckResponseAsync(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                return;
            }
            else
            {
                var message = $"Response status code: {response.StatusCode}  from url {response.RequestMessage.RequestUri} does not indicate success.";
                try
                {
                    if (response.Content != null)
                    {
                        var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        message += "Payload: " + content;
                    }
                }
                catch { }
                _logger.LogWarning(message);
                throw new Exception(message);
            }
        }
    }
}
