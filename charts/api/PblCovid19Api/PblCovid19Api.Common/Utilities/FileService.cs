﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PblCovid19Api.Common.Utilities
{
    public class FileService : IFileService
    {
        private readonly string _rootFolder = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory);

        private readonly int MAX_LOCKS = 10;
        private readonly SemaphoreSlim _obsoletingLock = new SemaphoreSlim(1, 1);
        private readonly IDictionary<string, SemaphoreSlim> _fileLocks = new Dictionary<string, SemaphoreSlim>();

        public string GetAbsoluteFilePath(string fileName, params string[] pathFolders)
        {
            return Path.Combine(GetAbsoluteDirectoryPath(pathFolders), fileName);
        }

        public string GetAbsoluteDirectoryPath(params string[] pathFolders)
        {
            var pathList = new List<string>(new[] { _rootFolder });
            pathList.AddRange(pathFolders);
            return Path.Combine(pathList.ToArray());
        }

        public async Task UseFileAsync(Action<FileInfo> action, string fileName, params string[] pathFolders)
        {
            var filePath = GetAbsoluteFilePath(fileName, pathFolders);

            await _FileLockWrapperAsync(filePath, () =>
            {
                var file = GetFile(filePath);
                action(file);
            });
        }

        public async Task UseFileAsync(Func<FileInfo, Task> action, string fileName, params string[] pathFolders)
        {
            var filePath = GetAbsoluteFilePath(fileName, pathFolders);

            await _FileLockWrapperAsync(filePath, async () =>
            {
                var file = GetFile(filePath);
                await action(file);
            });
        }

        public async Task UseFileAsync(Func<FileInfo, Task> action, string filePath)
        {
            await _FileLockWrapperAsync(filePath, async () =>
            {
                var file = GetFile(filePath);
                await action(file);
            });
        }

        public async Task UseFileAsync(Func<FileInfo, Task> action, FileInfo file)
        {
            await _FileLockWrapperAsync(file.FullName, async () =>
            {
                await action(file);
            });
        }

        public async Task<FileInfo> CreateFileAsync(Stream fileStream, string fileName, bool editIfAlreadyExists = false, bool ignoreLock = false, params string[] pathFolders)
        {
            FileInfo fileInfo = null;
            var filePath = GetAbsoluteFilePath(fileName, pathFolders);

            Func<Task> func = async () =>
            {
                var file = new FileInfo(filePath);
                if (file.Exists && !editIfAlreadyExists)
                {
                    throw new Exception($"File {filePath} already exists.");
                }
                var shouldEdit = file.Exists && editIfAlreadyExists;

                using (var fs = shouldEdit ? file.OpenWrite() : file.Create())
                {
                    await fileStream.CopyToAsync(fs);
                    fileStream.Dispose();
                }

                fileInfo = file;
            };
            if (ignoreLock)
            {
                await func();
            }
            else
            {
                await _FileLockWrapperAsync(filePath, func);
            }

            return fileInfo;
        }

        public DirectoryInfo CreateDirectory(params string[] pathFolders)
        {
            return Directory.CreateDirectory(_GetDirectory(pathFolders).FullName);
        }

        private FileInfo GetFile(string fullFileName)
        {
            var file = new FileInfo(fullFileName);
            if (file.Exists)
            {
                return file;
            }
            else
            {
                return null;
            }
        }


        private string _CombinePathWithRoot(params string[] pathFolders)
        {
            var pathList = new List<string>(new[] { _rootFolder });
            pathList.AddRange(pathFolders);
            return Path.Combine(pathList.ToArray());
        }

        private DirectoryInfo _GetDirectory(params string[] pathFolders)
        {
            return new DirectoryInfo(_CombinePathWithRoot(pathFolders));
        }

        private async Task _LockFileAsync(string file)
        {
            await _ClearObsoleteLocksAsync();
            var exists = _fileLocks.TryGetValue(file, out var fileLock);
            if (!exists)
            {
                fileLock = new SemaphoreSlim(1, 1);
                _fileLocks.TryAdd(file, fileLock);
            }
            await fileLock.WaitAsync();
        }

        private async Task _ClearObsoleteLocksAsync()
        {
            if (_fileLocks.Count > MAX_LOCKS)
            {
                var locks = _fileLocks.Values.ToList();
                foreach (var l in locks)
                {
                    await l.WaitAsync();
                }

                _fileLocks.Clear();

                foreach (var l in locks)
                {
                    l.Release();
                }
            }
        }

        private void _ReleaseFile(string file)
        {
            if (_fileLocks.TryGetValue(file, out var fileLock) && fileLock.CurrentCount == 0)
            {
                fileLock.Release();
            }
        }

        private async Task _FileLockWrapperAsync(string filePath, Func<Task> func)
        {
            await _LockFileAsync(filePath);
            try
            {
                await func();
            }
            finally
            {
                _ReleaseFile(filePath);
            }
        }

        private async Task _FileLockWrapperAsync(string filePath, Action func)
        {
            await _LockFileAsync(filePath);
            try
            {
                func();
            }
            finally
            {
                _ReleaseFile(filePath);
            }
        }

        public async Task RemoveOldestFilesAsync(string directoryPath, int leaveNewest)
        {
            var files = new DirectoryInfo(directoryPath).GetFiles()
                .OrderBy(f => f.LastAccessTime)
                .SkipLast(leaveNewest);

            foreach (var f in files)
            {
                await _LockFileAsync(f.FullName);
                f.Delete();
                _ReleaseFile(f.FullName);
            }
        }
    }
}
