﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;
using System.Collections.Generic;
using System.Linq;

namespace PblCovid19Api.Common.Utilities
{
    public static class NotNull
    {
        public static void ThrowIfNull(this object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj), "Provided null argument!");
            }
        }

        public static void ThrowIfNullOrEmpty(this string str)
        {
            str.ThrowIfNull();

            if (string.IsNullOrWhiteSpace(str))
            {
                throw new ArgumentNullException(nameof(str), "Provided string is empty");
            }
        }

        public static void ThrowIfNullOrEmpty<T>(this IEnumerable<T> col)
        {
            col.ThrowIfNull();

            if (!col.Any())
            {
                throw new ArgumentNullException(nameof(col), "Provided collection is empty!");
            }
        }
    }
}
