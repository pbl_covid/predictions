﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;
using System.IO;
using System.Threading.Tasks;

namespace PblCovid19Api.Common.Utilities
{
    public interface IFileService
    {
        Task UseFileAsync(Action<FileInfo> action, string fileName, params string[] pathFolders);
        Task UseFileAsync(Func<FileInfo, Task> action, string fileName, params string[] pathFolders);
        Task UseFileAsync(Func<FileInfo, Task> action, string filePath);
        Task UseFileAsync(Func<FileInfo, Task> action, FileInfo file);
        Task<FileInfo> CreateFileAsync(Stream fileStream, string fileName, bool editIfAlreadyExists = false, bool ignoreLock = false, params string[] pathFolders);
        DirectoryInfo CreateDirectory(params string[] pathFolders);
        string GetAbsoluteDirectoryPath(params string[] pathFolders);
        string GetAbsoluteFilePath(string fileName, params string[] pathFolders);
        Task RemoveOldestFilesAsync(string directoryPath, int leaveNewest);
    }
}
