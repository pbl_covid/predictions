﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.Collections.Generic;

namespace PblCovid19Api.Common.Models.Charts
{
    public class CountryChart
    {
        public IEnumerable<DailyCase> Real { get; set; }
        public IEnumerable<DailyCase> Simulation { get; set; }
        public ErrorsModel Errors { get; set; }
    }
}
