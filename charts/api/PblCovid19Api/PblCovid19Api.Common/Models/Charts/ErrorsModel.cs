﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

namespace PblCovid19Api.Common.Models.Charts
{
    public sealed class ErrorsModel
    {
        public double MeanSquareError { get; set; }
        public double MeanError { get; set; }
        public double MeanAbsoluteError { get; set; }
    }
}
