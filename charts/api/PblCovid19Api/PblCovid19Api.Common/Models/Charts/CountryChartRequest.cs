﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;
using System.Collections.Generic;

namespace PblCovid19Api.Common.Models.Charts
{
    public class CountryChartRequest
    {
        public DateTime ChartDateFrom { get; set; }
        public DateTime SimulationDateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string Model { get; set; }
        public string CountryCode { get; set; }
        public IEnumerable<RestrictionModel> Restrictions { get; set; }
    }
}
