﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.Net;

namespace PblCovid19Api.Common.Models.Exceptions
{
    public sealed class NotFoundException : BaseException
    {
        public NotFoundException(string? message) : base(HttpStatusCode.NotFound, message)
        {
        }
    }
}
