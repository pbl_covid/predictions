﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.Net;

namespace PblCovid19Api.Common.Models.Exceptions
{
    public sealed class BadRequestException : BaseException
    {
        public BadRequestException(string? message) : base(HttpStatusCode.BadRequest, message)
        {
        }
    }
}
