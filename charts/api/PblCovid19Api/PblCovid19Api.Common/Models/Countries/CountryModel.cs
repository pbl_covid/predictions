﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

namespace PblCovid19Api.Common.Models.Countries
{
    public sealed class CountryModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Alpha3 { get; set; }
    }
}
