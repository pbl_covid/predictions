﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;

namespace PblCovid19Api.Common.Models
{
    public class TimeRange
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
