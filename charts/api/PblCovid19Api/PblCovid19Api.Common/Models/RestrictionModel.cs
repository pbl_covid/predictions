﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.Collections.Generic;

namespace PblCovid19Api.Common.Models
{
    public class RestrictionModel
    {
        public string Name { get; set; }
        public int Value { get; set; }
        public IEnumerable<TimeRange> TimeRanges { get; set; }
    }
}
