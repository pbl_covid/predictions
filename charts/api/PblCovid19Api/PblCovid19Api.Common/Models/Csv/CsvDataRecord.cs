﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using CsvHelper.Configuration.Attributes;
using PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment;
using PblCovid19Api.Common.Models.Restrictions.EconomicResponse;
using PblCovid19Api.Common.Models.Restrictions.HealthSystem;
using PblCovid19Api.Common.Models.Restrictions.Miscellaneous;
using System;
using System.Globalization;

namespace PblCovid19Api.Common.Models.Csv
{
    public class CsvDataRecord
    {
        public string Province { get; set; }
        public string City { get; set; }
        public string CityCode { get; set; }
        public int Confirmed { get; set; }
        public int Deaths { get; set; }
        public int Recovered { get; set; }
        public int Active { get; set; }
        public DateTime LastUpdate { get; set; }

        [Name("population")]
        public long Population { get; set; }

        #region restrictions

        #region ClosuresAndContainment

        /// <summary>
        /// C1
        /// </summary>
        [Name("Restriction_C1")]
        public SchoolClosing? Restriction_C1 { get; set; }

        /// <summary>
        /// C2
        /// </summary>
        [Name("Restriction_C2")]
        public WorkplaceClosing? Restriction_C2 { get; set; }

        /// <summary>
        /// C3
        /// </summary>
        [Name("Restriction_C3")]
        public CancelPublicEvents? Restriction_C3 { get; set; }

        /// <summary>
        /// C4
        /// </summary>
        [Name("Restriction_C4")]
        public RestrictionsOnGatherings? Restriction_C4 { get; set; }

        /// <summary>
        /// C5
        /// </summary>
        [Name("Restriction_C5")]
        public ClosePublicTransport? Restriction_C5 { get; set; }

        /// <summary>
        /// C6
        /// </summary>
        [Name("Restriction_C6")]
        public StayAtHome? Restriction_C6 { get; set; }

        /// <summary>
        /// C7
        /// </summary>
        [Name("Restriction_C7")]
        public InternalMovement? Restriction_C7 { get; set; }

        /// <summary>
        /// C8
        /// </summary>
        [Name("Restriction_C8")]
        public InternationalTravelControls? Restriction_C8 { get; set; }

        #endregion ClosuresAndContainment

        #region HealthSystem

        /// <summary>
        /// H1
        /// </summary>
        [Name("Restriction_H1")]
        public PublicInfoCampaings? Restriction_H1 { get; set; }

        /// <summary>
        /// H2
        /// </summary>
        [Name("Restriction_H2")]
        public TestingPolicy? Restriction_H2 { get; set; }

        /// <summary>
        /// H3
        /// </summary>
        [Name("Restriction_H3")]
        public ContactTracing? Restriction_H3 { get; set; }

        /// <summary>
        /// H4
        /// </summary>
        [Name("Restriction_H4")]
        [NumberStyles(NumberStyles.Number)]
        public long? Restriction_H4 { get; set; }

        /// <summary>
        /// H5
        /// </summary>
        [Name("Restriction_H5")]
        [NumberStyles(NumberStyles.Number)]
        public long? Restriction_H5 { get; set; }

        #endregion HealthSystem

        #region EconomicResponse

        /// <summary>
        /// E1
        /// </summary>
        [Name("Restriction_E1")]
        public E1? Restriction_E1 { get; set; }

        /// <summary>
        /// E2
        /// </summary>
        [Name("Restriction_E2")]
        public E2? Restriction_E2 { get; set; }

        /// <summary>
        /// E3
        /// </summary>
        [Name("Restriction_E3")]
        [NumberStyles(NumberStyles.Number)]
        public long? Restriction_E3 { get; set; }

        /// <summary>
        /// E4
        /// </summary>
        [Name("Restriction_E4")]
        [NumberStyles(NumberStyles.Number)]
        public long? Restriction_E4 { get; set; }

        #endregion EconomicResponse

        #region Miscellaneous

        /// <summary>
        /// M1
        /// </summary>
        [Name("Restriction_M1")]
        public M1? Restriction_M1 { get; set; }

        #endregion

        #endregion restrictions
    }
}
