﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

#nullable disable

using System.ComponentModel.DataAnnotations.Schema;

namespace PblCovid19Api.Common.Models.Database
{
    /// <summary>
    /// ukView
    /// </summary>
    public class UkView : CountryCase
    {
        [NotMapped]
        public const string Code = "ukView";
    }
}
