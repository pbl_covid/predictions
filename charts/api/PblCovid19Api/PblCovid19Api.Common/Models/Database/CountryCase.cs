﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment;
using PblCovid19Api.Common.Models.Restrictions.EconomicResponse;
using PblCovid19Api.Common.Models.Restrictions.HealthSystem;
using PblCovid19Api.Common.Models.Restrictions.Miscellaneous;
using System;
using System.ComponentModel.DataAnnotations;

namespace PblCovid19Api.Common.Models.Database
{
    public abstract class CountryCase
    {
        [Key]
        public int Id { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string CityCode { get; set; }
        public int Confirmed { get; set; }
        public int Deaths { get; set; }
        public int Recovered { get; set; }
        public int Active { get; set; }
        public DateTime LastUpdate { get; set; }

        #region restrictions

        #region ClosuresAndContainment

        /// <summary>
        /// C1
        /// </summary>
        public SchoolClosing? SchoolClosing { get; set; }

        /// <summary>
        /// C2
        /// </summary>
        public WorkplaceClosing? WorkplaceClosing { get; set; }

        /// <summary>
        /// C3
        /// </summary>
        public CancelPublicEvents? CancelPublicEvents { get; set; }

        /// <summary>
        /// C4
        /// </summary>
        public RestrictionsOnGatherings? RestrictionsOnGatherings { get; set; }

        /// <summary>
        /// C5
        /// </summary>
        public ClosePublicTransport? ClosePublicTransport { get; set; }

        /// <summary>
        /// C6
        /// </summary>
        public StayAtHome? StayAtHome { get; set; }

        /// <summary>
        /// C7
        /// </summary>
        public InternalMovement? InternalMovement { get; set; }

        /// <summary>
        /// C8
        /// </summary>
        public InternationalTravelControls? InternationalTravelControls { get; set; }

        #endregion ClosuresAndContainment

        #region HealthSystem

        /// <summary>
        /// H1
        /// </summary>
        public PublicInfoCampaings? PublicInfoCampaings { get; set; }

        /// <summary>
        /// H2
        /// </summary>
        public TestingPolicy? TestingPolicy { get; set; }

        /// <summary>
        /// H3
        /// </summary>
        public ContactTracing? ContactTracing { get; set; }

        /// <summary>
        /// H4
        /// </summary>
        public long EmergencyInvestmentInHealthCare { get; set; }

        /// <summary>
        /// H5
        /// </summary>
        public long InvestmentInVaccines { get; set; }

        #endregion HealthSystem

        #region EconomicResponse

        /// <summary>
        /// E1
        /// </summary>
        public E1? E1 { get; set; }

        /// <summary>
        /// E2
        /// </summary>
        public E2? E2 { get; set; }

        /// <summary>
        /// E3
        /// </summary>
        public long FiscalMeasures { get; set; }

        /// <summary>
        /// E4
        /// </summary>
        public long E4 { get; set; }

        #endregion EconomicResponse

        #region Miscellaneous

        public M1? M1 { get; set; }

        #endregion

        #endregion restrictions
    }
}
