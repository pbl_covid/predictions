﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

#nullable disable

using System.ComponentModel.DataAnnotations.Schema;

namespace PblCovid19Api.Common.Models.Database
{
    /// <summary>
    /// usaView
    /// </summary>
    public class UsaView : CountryCase
    {
        [NotMapped]
        public const string Code = "usaView";
    }
}
