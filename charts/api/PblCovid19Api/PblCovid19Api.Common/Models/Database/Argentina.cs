﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel.DataAnnotations.Schema;

namespace PblCovid19Api.Common.Models.Database
{
    public class Argentina : CountryCase
    {
        [NotMapped]
        public const string Code = "argentina";
    }
}
