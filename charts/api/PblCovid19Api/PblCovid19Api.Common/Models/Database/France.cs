﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

#nullable disable

using System.ComponentModel.DataAnnotations.Schema;

namespace PblCovid19Api.Common.Models.Database
{
    public class France : CountryCase
    {
        [NotMapped]
        public const string Code = "france";
    }
}
