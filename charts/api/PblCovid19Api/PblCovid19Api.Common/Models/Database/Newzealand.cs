﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace PblCovid19Api.Common.Models.Database
{
    public class NewZealand : CountryCase
    {
        [NotMapped]
        public const string Code = "newzealand";
    }
}
