﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace PblCovid19Api.Common.Models.Database
{
    public class Country
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Alpha3 { get; set; }
        public string ViewName { get; set; }
        public DateTime? RestrictionsLastUpdate { get; set; }
    }
}
