﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

namespace PblCovid19Api.Common.Models
{
    public static class ConfigurationKeys
    {

        public const string DataFetcherUrl = "Urls:DataFetcher";
        public const string InputCsvPath = "Paths:InputCsv";
        public const string OutputCsvPath = "Paths:OutputCsv";

        public const string PythonModelUrl = "Models:Python";

        public const string MatlabWindowPath = "Models:Matlab:Windows:Path";

        public const string MatlabLinuxFile = "Models:Matlab:Linux:File";
        public const string MatlabLinuxPath = "Models:Matlab:Linux:Path";
        public const string MatlabLinuxScript = "Models:Matlab:Linux:Script";
    }
}
