﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace PblCovid19Api.Common.Models.Restrictions
{
    public sealed class AvailableRestriction<TEnum> where TEnum : struct, Enum
    {
        public string Code { get; set; }
        public IEnumerable<TEnum> Values { get; set; }

        public AvailableRestriction()
        {
            var description = typeof(TEnum).GetCustomAttributes(typeof(DescriptionAttribute), false).First();
            var code = ((DescriptionAttribute)description).Description;
            Code = code;
            Values = EnumValuesHelper.GetAllEnumValues<TEnum>();
        }
    }
}
