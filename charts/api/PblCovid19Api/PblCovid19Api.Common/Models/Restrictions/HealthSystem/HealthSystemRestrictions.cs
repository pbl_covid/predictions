﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Database;

namespace PblCovid19Api.Common.Models.Restrictions.HealthSystem
{
    public sealed class HealthSystemRestrictions
    {
        public HealthSystemRestrictions(CountryCase countryCase)
        {
            PublicInfoCampaings = countryCase.PublicInfoCampaings;
            TestingPolicy = countryCase.TestingPolicy;
            ContactTracing = countryCase.ContactTracing;
            EmergencyInvestmentInHealthCare = countryCase.EmergencyInvestmentInHealthCare;
            InvestmentInVaccines = countryCase.InvestmentInVaccines;
        }

        public HealthSystemRestrictions()
        {

        }

        public PublicInfoCampaings? PublicInfoCampaings { get; set; }
        public TestingPolicy? TestingPolicy { get; set; }
        public ContactTracing? ContactTracing { get; set; }
        public long EmergencyInvestmentInHealthCare { get; set; }
        public long InvestmentInVaccines { get; set; }
    }
}
