﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.HealthSystem
{
    /// <summary>
    /// H1
    /// </summary>
    [Description("H1")]
    public enum PublicInfoCampaings
    {
        NoPublicInformationCampaign = 0,

        PublicOfficialsUrgingCaution,
        /// <summary>
        /// (e.g. across traditional and social media)
        /// </summary>
        CoordinatedPublicInformationCampaign
    }
}
