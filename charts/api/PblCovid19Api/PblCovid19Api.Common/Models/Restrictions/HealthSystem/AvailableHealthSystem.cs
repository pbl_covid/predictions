﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

namespace PblCovid19Api.Common.Models.Restrictions.HealthSystem
{
    public sealed class AvailableHealthSystem
    {
        public AvailableRestriction<PublicInfoCampaings> PublicInfoCampaings { get; } = new AvailableRestriction<PublicInfoCampaings>();
        public AvailableRestriction<TestingPolicy> TestingPolicy { get; } = new AvailableRestriction<TestingPolicy>();
        public AvailableRestriction<ContactTracing> ContactTracing { get; } = new AvailableRestriction<ContactTracing>();
        //public AvailableRestriction<EmergencyInvestmentInHealthCare> EmergencyInvestmentInHealthCare { get; } = new AvailableRestriction<EmergencyInvestmentInHealthCare>();
    }
}
