﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.HealthSystem
{
    /// <summary>
    /// H3
    /// </summary>
    [Description("H3")]
    public enum ContactTracing
    {
        Val1 = 0,
        Val2,
        Val3,
        Val4,
        Val5,
    }
}
