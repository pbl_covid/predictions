﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Database;
using PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment;
using PblCovid19Api.Common.Models.Restrictions.EconomicResponse;
using PblCovid19Api.Common.Models.Restrictions.HealthSystem;
using PblCovid19Api.Common.Models.Restrictions.Miscellaneous;

namespace PblCovid19Api.Common.Models.Restrictions
{
    public sealed class RestrictionsModel
    {
        public RestrictionsModel(CountryCase countryCase)
        {
            ClosuresAndContainment = new ClosuresAndContainmentRestrictions(countryCase);
            HealthSystem = new HealthSystemRestrictions(countryCase);
            EconomicResponse = new EconomicResponseRestrictions(countryCase);
            MiscellaneousRestrictions = new MiscellaneousRestrictions(countryCase);
        }

        public RestrictionsModel()
        {

        }

        public ClosuresAndContainmentRestrictions ClosuresAndContainment { get; set; }
        public HealthSystemRestrictions HealthSystem { get; set; }
        public EconomicResponseRestrictions EconomicResponse { get; set; }
        public MiscellaneousRestrictions MiscellaneousRestrictions { get; set; }
    }
}
