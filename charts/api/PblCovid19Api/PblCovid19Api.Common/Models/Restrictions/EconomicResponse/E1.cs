﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.EconomicResponse
{
    /// <summary>
    /// E1
    /// </summary>
    [Description("E1")]
    public enum E1
    {
        Val1 = 0,
        Val2,
        Val3,
        Val4,
        Val5,
    }
}
