﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

namespace PblCovid19Api.Common.Models.Restrictions.EconomicResponse
{
    public sealed class AvailableEconomicResponse
    {
        public AvailableRestriction<E1> E1 { get; } = new AvailableRestriction<E1>();
        public AvailableRestriction<E2> E2 { get; } = new AvailableRestriction<E2>();
        //public AvailableRestriction<FiscalMeasures> FiscalMeasures { get; } = new AvailableRestriction<FiscalMeasures>();
        //public AvailableRestriction<E4> E4 { get; } = new AvailableRestriction<E4>();
    }
}
