﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Database;

namespace PblCovid19Api.Common.Models.Restrictions.EconomicResponse
{
    public sealed class EconomicResponseRestrictions
    {
        public EconomicResponseRestrictions(CountryCase countryCase)
        {
            E1 = countryCase.E1;
            E2 = countryCase.E2;
            FiscalMeasures = countryCase.FiscalMeasures;
            E4 = countryCase.E4;
        }

        public EconomicResponseRestrictions()
        {

        }

        public E1? E1 { get; set; }
        public E2? E2 { get; set; }
        public long FiscalMeasures { get; set; }
        public long E4 { get; set; }
    }
}
