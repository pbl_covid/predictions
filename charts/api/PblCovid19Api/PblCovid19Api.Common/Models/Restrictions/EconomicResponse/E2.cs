﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.EconomicResponse
{
    /// <summary>
    /// E2
    /// </summary>
    [Description("E2")]
    public enum E2
    {
        Val1 = 0,
        Val2,
        Val3,
        Val4,
        Val5,
    }
}
