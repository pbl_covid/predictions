﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Exceptions;
using PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment;
using PblCovid19Api.Common.Models.Restrictions.EconomicResponse;
using PblCovid19Api.Common.Models.Restrictions.HealthSystem;
using PblCovid19Api.Common.Models.Restrictions.Miscellaneous;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PblCovid19Api.Common.Models.Restrictions.Converters
{
    public static class RestrictionModelConverter
    {
        /// <summary>
        /// its really hacked
        /// </summary>
        /// <param name="restrictions"></param>
        /// <returns></returns>
        public static int[] RestrictionsToVector(IEnumerable<RestrictionModel> restrictions)
        {
            var dict = new Dictionary<string, int>
            {
                {"C1" , 0 },
                {"C2" , 0 },
                {"C3" , 0 },
                {"C4" , 0 },
                {"C5" , 0 },
                {"C6" , 0 },
                {"C7" , 0 },
                {"C8" , 0 },
            };

            foreach (var restriction in restrictions)
            {
                //this two lines check if the value is correct
                var type = GetEnumFromString(restriction.Name);
                var typed = Enum.ToObject(type, restriction.Value);

                if (dict.ContainsKey(restriction.Name))
                {
                    dict[restriction.Name] = restriction.Value;
                }
            }

            return dict.Values.ToArray();
        }

        private static Type GetEnumFromString(string name)
        {
            return name switch
            {
                "C1" => typeof(SchoolClosing),
                "C2" => typeof(WorkplaceClosing),
                "C3" => typeof(CancelPublicEvents),
                "C4" => typeof(RestrictionsOnGatherings),
                "C5" => typeof(ClosePublicTransport),
                "C6" => typeof(StayAtHome),
                "C7" => typeof(InternalMovement),
                "C8" => typeof(InternationalTravelControls),

                "H1" => typeof(PublicInfoCampaings),
                "H2" => typeof(TestingPolicy),
                "H3" => typeof(ContactTracing),
                "H4" => throw new BadRequestException($"{name} is not enum typ"),
                "H5" => throw new BadRequestException($"{name} is not enum typ"),

                "E1" => typeof(E1),
                "E2" => typeof(E2),
                "E3" => throw new BadRequestException($"{name} is not enum typ"),
                "E4" => throw new BadRequestException($"{name} is not enum typ"),

                "M1" => typeof(M1),

                _ => throw new BadRequestException($"There is no registered exception as {name}")
            };
        }
    }
}
