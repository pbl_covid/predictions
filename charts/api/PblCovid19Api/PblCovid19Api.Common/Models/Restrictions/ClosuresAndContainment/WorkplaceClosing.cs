﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment
{
    /// <summary>
    /// C2
    /// </summary>
    [Description("C2")]
    public enum WorkplaceClosing
    {
        NoMeasures = 0,
        /// <summary>
        /// (or work from home)
        /// </summary>
        RecommendClosing,
        /// <summary>
        /// (or work from home) for some sectors or categories of workers
        /// </summary>
        LimitedClosing,
        /// <summary>
        /// (or work from home) all-butessential workplaces (eg grocery stores, doctors)
        /// </summary>
        RequireClosing
    }
}
