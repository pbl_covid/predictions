﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment
{
    /// <summary>
    /// C8
    /// </summary>
    [Description("C8")]
    public enum InternationalTravelControls
    {
        NoMeasures = 0,
        Screenings,
        QuarantineArrivals,
        BanHighRiskRegions,
        TotalBorderClosure
    }
}
