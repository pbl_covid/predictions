﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment
{
    /// <summary>
    /// C4 Restrictions on gatherings
    /// </summary>
    [Description("C4")]
    public enum RestrictionsOnGatherings
    {
        NoRestrictions = 0,
        /// <summary>
        /// Restrictions on very large gatherings (the limit is above 1000 people)
        /// </summary>
        Above1000,
        /// <summary>
        /// Restrictions on gatherings between 100-1000 people
        /// </summary>
        Limit1000,
        /// <summary>
        /// Restrictions on gatherings between 10-100 people
        /// </summary>
        Limit100,
        /// <summary>
        /// Restrictions on gatherings of less than 10 people
        /// </summary>
        Limit10
    }
}
