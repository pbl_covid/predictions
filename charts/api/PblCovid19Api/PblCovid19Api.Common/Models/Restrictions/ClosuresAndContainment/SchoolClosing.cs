﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment
{
    /// <summary>
    /// C1
    /// </summary>
    [Description("C1")]
    public enum SchoolClosing
    {
        NoMeasures = 0,
        RecommendClosing,
        /// <summary>
        /// only some levels or categories, eg just high school, or just public schools
        /// </summary>
        LimitedClosing,
        /// <summary>
        /// all levels
        /// </summary>
        RequireClosing
    }
}
