﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

namespace PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment
{
    public sealed class AvailableClosuresAndContainment
    {
        public AvailableRestriction<SchoolClosing> SchoolClosing { get; } = new AvailableRestriction<SchoolClosing>();
        public AvailableRestriction<WorkplaceClosing> WorkplaceClosing { get; } = new AvailableRestriction<WorkplaceClosing>();
        public AvailableRestriction<CancelPublicEvents> CancelPublicEvents { get; } = new AvailableRestriction<CancelPublicEvents>();
        public AvailableRestriction<RestrictionsOnGatherings> RestrictionsOnGatherings { get; } = new AvailableRestriction<RestrictionsOnGatherings>();
        public AvailableRestriction<ClosePublicTransport> ClosePublicTransport { get; } = new AvailableRestriction<ClosePublicTransport>();
        public AvailableRestriction<StayAtHome> StayAtHome { get; } = new AvailableRestriction<StayAtHome>();
        public AvailableRestriction<InternalMovement> InternalMovement { get; } = new AvailableRestriction<InternalMovement>();
        public AvailableRestriction<InternationalTravelControls> InternationalTravelControls { get; } = new AvailableRestriction<InternationalTravelControls>();
    }
}
