﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment
{
    /// <summary>
    /// C5
    /// </summary>
    [Description("C5")]
    public enum ClosePublicTransport
    {
        NoMeasures = 0,
        /// <summary>
        /// (or significantly reduce volume/route/means of transport available)
        /// </summary>
        RecommendClosing,
        /// <summary>
        /// (or prohibit most citizens from using it)
        /// </summary>
        RequireClosing
    }
}
