﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Database;

namespace PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment
{
    public sealed class ClosuresAndContainmentRestrictions
    {
        public ClosuresAndContainmentRestrictions()
        {

        }

        public ClosuresAndContainmentRestrictions(CountryCase countryCase)
        {
            SchoolClosing = countryCase.SchoolClosing;
            WorkplaceClosing = countryCase.WorkplaceClosing;
            CancelPublicEvents = countryCase.CancelPublicEvents;
            RestrictionsOnGatherings = countryCase.RestrictionsOnGatherings;
            ClosePublicTransport = countryCase.ClosePublicTransport;
            StayAtHome = countryCase.StayAtHome;
            InternalMovement = countryCase.InternalMovement;
            InternationalTravelControls = countryCase.InternationalTravelControls;
        }

        public SchoolClosing? SchoolClosing { get; set; }
        public WorkplaceClosing? WorkplaceClosing { get; set; }
        public CancelPublicEvents? CancelPublicEvents { get; set; }
        public RestrictionsOnGatherings? RestrictionsOnGatherings { get; set; }
        public ClosePublicTransport? ClosePublicTransport { get; set; }
        public StayAtHome? StayAtHome { get; set; }
        public InternalMovement? InternalMovement { get; set; }
        public InternationalTravelControls? InternationalTravelControls { get; set; }
    }
}
