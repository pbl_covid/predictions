﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment
{
    /// <summary>
    /// C3
    /// </summary>
    [Description("C3")]
    public enum CancelPublicEvents
    {
        NoMeasures = 0,
        RecommendCancelling,
        RequireCancelling
    }
}
