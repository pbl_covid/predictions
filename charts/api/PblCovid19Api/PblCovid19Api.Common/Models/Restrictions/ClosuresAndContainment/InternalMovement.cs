﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment
{
    /// <summary>
    /// C7
    /// </summary>
    [Description("C7")]
    public enum InternalMovement
    {
        NoMeasures = 0,
        /// <summary>
        /// (or significantly reduce volume/route/means of transport)
        /// </summary>
        RecommendClosing,
        /// <summary>
        /// (or prohibit most people from using it)
        /// </summary>
        RequireClosing
    }
}
