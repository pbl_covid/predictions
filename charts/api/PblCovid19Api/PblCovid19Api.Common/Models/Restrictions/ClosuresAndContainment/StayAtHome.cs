﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment
{
    /// <summary>
    /// C6
    /// </summary>
    [Description("C6")]
    public enum StayAtHome
    {
        NoMeasures = 0,
        RecommendNotLeavingHouse,
        /// <summary>
        /// with exceptions for daily exercise, grocery shopping, and ‘essential’ trips
        /// </summary>
        RequireNotLeavingHouseWithExceptions,
        /// <summary>
        /// with minimal exceptions (e.g. allowed to leave only once every few days, or only one person can leave at a time, etc.)
        /// </summary>
        RequireNotLeavingHouse
    }
}
