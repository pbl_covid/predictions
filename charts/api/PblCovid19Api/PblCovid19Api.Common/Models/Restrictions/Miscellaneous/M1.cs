﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System.ComponentModel;

namespace PblCovid19Api.Common.Models.Restrictions.Miscellaneous
{
    /// <summary>
    /// M1
    /// </summary>
    [Description("M1")]
    public enum M1
    {
        Val1 = 0,
        Val2,
        Val3,
    }
}
