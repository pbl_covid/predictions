﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Database;

namespace PblCovid19Api.Common.Models.Restrictions.Miscellaneous
{
    public sealed class MiscellaneousRestrictions
    {
        public MiscellaneousRestrictions(CountryCase countryCase)
        {
            M1 = countryCase.M1;
        }

        public MiscellaneousRestrictions()
        {

        }

        public M1? M1 { get; set; }
    }
}
