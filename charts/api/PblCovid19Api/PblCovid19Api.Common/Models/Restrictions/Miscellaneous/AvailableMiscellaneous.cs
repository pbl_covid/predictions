﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

namespace PblCovid19Api.Common.Models.Restrictions.Miscellaneous
{
    public sealed class AvailableMiscellaneous
    {
        public AvailableRestriction<M1> PublicInfoCampaings { get; } = new AvailableRestriction<M1>();
    }
}
