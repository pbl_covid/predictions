﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment;
using PblCovid19Api.Common.Models.Restrictions.EconomicResponse;
using PblCovid19Api.Common.Models.Restrictions.HealthSystem;
using PblCovid19Api.Common.Models.Restrictions.Miscellaneous;

namespace PblCovid19Api.Common.Models.Restrictions
{
    public sealed class AvailableRestrictions
    {
        public AvailableClosuresAndContainment ClosuresAndContainment { get; set; } = new AvailableClosuresAndContainment();
        public AvailableEconomicResponse EconomicResponse { get; set; } = new AvailableEconomicResponse();
        public AvailableHealthSystem AvailableHealthSystem { get; set; } = new AvailableHealthSystem();
        public AvailableMiscellaneous AvailableMiscellaneous { get; set; } = new AvailableMiscellaneous();
    }
}
