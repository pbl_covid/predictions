﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;

namespace PblCovid19Api.Common.Models
{
    public class DailyCase
    {
        public DateTime Date { get; set; }
        public int Deaths { get; set; }
        public int Recovered { get; set; }
        public int Confirmed { get; set; }
        public int Active { get; set; }
    }
}
