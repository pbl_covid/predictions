﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.DependencyInjection;
using PblCovid19Api.Logic.Charts;
using PblCovid19Api.Logic.Charts.Errors;
using PblCovid19Api.Logic.Countries;
using PblCovid19Api.Logic.Real;
using PblCovid19Api.Logic.Restrictions;

namespace PblCovid19Api.Logic
{
    public static class Services
    {
        /// <summary>
        /// Register all services from PblCovid19Api.Logic.
        /// </summary>
        public static IServiceCollection AddLogicServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICountryChartProvider, CountryChartProvider>();
            serviceCollection.AddTransient<IRealDailyCaseDataProvider, RealDailyCaseDataProvider>();
            serviceCollection.AddTransient<ICountriesProvider, CountriesProvider>();
            serviceCollection.AddTransient<IRestrictionsProvider, RestrictionsProvider>();
            serviceCollection.AddTransient<IErrorsCalculator, ErrorsCalculator>();

            return serviceCollection;
        }
    }
}
