﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models;
using PblCovid19Api.Common.Models.Charts;
using PblCovid19Api.Infrastructure.Database.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PblCovid19Api.Logic.Real
{
    internal class RealDailyCaseDataProvider : IRealDailyCaseDataProvider
    {
        private readonly ICountryCaseRepositoryFactory repositoryFactory;

        public RealDailyCaseDataProvider(ICountryCaseRepositoryFactory repositoryFactory)
        {
            this.repositoryFactory = repositoryFactory;
        }

        public async Task<IEnumerable<DailyCase>> GetDailyCasesAsync(CountryChartRequest request)
        {
            var repository = repositoryFactory.GetCountryCaseRepository(request.CountryCode);
            var cases = await repository.GetCountryCasesAsync(request.ChartDateFrom, request.DateTo).ConfigureAwait(false);
            var list = new List<DailyCase>();
            foreach (var dailyCase in cases)
            {
                list.Add(new DailyCase
                {
                    Active = dailyCase.Active,
                    Confirmed = dailyCase.Confirmed,
                    Date = dailyCase.LastUpdate,
                    Deaths = dailyCase.Deaths,
                    Recovered = dailyCase.Recovered
                });
            }

            return list.OrderByDescending(l => l.Date);
        }
    }
}
