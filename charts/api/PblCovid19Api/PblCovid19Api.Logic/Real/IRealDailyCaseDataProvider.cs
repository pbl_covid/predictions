﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models;
using PblCovid19Api.Common.Models.Charts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PblCovid19Api.Logic.Real
{
    public interface IRealDailyCaseDataProvider
    {
        Task<IEnumerable<DailyCase>> GetDailyCasesAsync(CountryChartRequest request);
    }
}
