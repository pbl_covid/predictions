﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Restrictions;
using System.Threading.Tasks;

namespace PblCovid19Api.Logic.Restrictions
{
    public interface IRestrictionsProvider
    {
        AvailableRestrictions GetAvailableRestrictions();
        Task<RestrictionsModel> GetCountryCurrentRestrictionsAsync(string countryCode);
    }
}