﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Restrictions;
using PblCovid19Api.Infrastructure.Database.Repository;
using System.Threading.Tasks;

namespace PblCovid19Api.Logic.Restrictions
{
    internal class RestrictionsProvider : IRestrictionsProvider
    {
        private readonly ICountryCaseRepositoryFactory _repositoryFactory;
        private readonly ICountryRepository _countries;

        public RestrictionsProvider(ICountryCaseRepositoryFactory repositoryFactory, ICountryRepository countries)
        {
            _repositoryFactory = repositoryFactory;
            _countries = countries;
        }

        public AvailableRestrictions GetAvailableRestrictions()
        {
            return new AvailableRestrictions();
        }

        public async Task<RestrictionsModel> GetCountryCurrentRestrictionsAsync(string countryCode)
        {
            var country = await _countries.GetCountryForCountryCodeAsync(countryCode).ConfigureAwait(false);
            var lastUpdateTime = country.RestrictionsLastUpdate;

            var repository = _repositoryFactory.GetCountryCaseRepository(countryCode);
            var countryCase = await repository.GetCountryCaseAsync(lastUpdateTime.Value).ConfigureAwait(false);
            var currentRestrictions = new RestrictionsModel(countryCase);
            return currentRestrictions;
        }
    }
}
