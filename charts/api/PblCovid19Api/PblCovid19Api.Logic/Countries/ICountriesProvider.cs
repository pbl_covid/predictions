﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Countries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PblCovid19Api.Logic.Countries
{
    public interface ICountriesProvider
    {
        Task<IEnumerable<CountryModel>> GetAvailableCountriesAsync();
    }
}