﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Countries;
using PblCovid19Api.Infrastructure.Database.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PblCovid19Api.Logic.Countries
{
    internal sealed class CountriesProvider : ICountriesProvider
    {
        private readonly ICountryRepository countryRepository;
        private readonly string[] DisabledCountryCodes = new[] { "china" };


        public CountriesProvider(ICountryRepository countryRepository)
        {
            this.countryRepository = countryRepository;
        }
        public async Task<IEnumerable<CountryModel>> GetAvailableCountriesAsync()
        {
            var countries = await countryRepository.GetCountriesAsync().ConfigureAwait(false);
            return countries
                .Where(c => !DisabledCountryCodes.Contains(c.Code.ToLower()))
                .Select(c => new CountryModel
                {
                    Alpha3 = c.Alpha3,
                    Code = c.Code,
                    Name = c.Name,
                })
                .OrderBy(c => c.Name);
        }
    }
}
