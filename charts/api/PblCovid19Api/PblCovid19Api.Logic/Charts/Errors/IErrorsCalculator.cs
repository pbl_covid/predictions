﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Charts;
using System.Collections.Generic;

namespace PblCovid19Api.Logic.Charts.Errors
{
    public interface IErrorsCalculator
    {
        ErrorsModel CalculateAllErrors(IEnumerable<int> real, IEnumerable<int> simulation);
        double CalculateMAE(IList<int> real, IList<int> simulation);
        double CalculateME(IList<int> real, IList<int> simulation);
        double CalculateMSE(IList<int> real, IList<int> simulation);
    }
}