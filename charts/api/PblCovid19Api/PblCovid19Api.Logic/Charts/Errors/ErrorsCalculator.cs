﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Charts;
using PblCovid19Api.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PblCovid19Api.Logic.Charts.Errors
{
    internal class ErrorsCalculator : IErrorsCalculator
    {
        public ErrorsModel CalculateAllErrors(IEnumerable<int> real, IEnumerable<int> simulation)
        {
            real.ThrowIfNullOrEmpty();
            simulation.ThrowIfNullOrEmpty();
            if (real.Count() != simulation.Count())
            {
                throw new ArgumentException("Length of provided int collections are not equal.");
            }

            var realList = real.ToList();
            var simList = simulation.ToList();

            return new ErrorsModel
            {
                MeanAbsoluteError = CalculateMAE(realList, simList),
                MeanError = CalculateME(realList, simList),
                MeanSquareError = CalculateMSE(realList, simList)
            };
        }

        public double CalculateMSE(IList<int> real, IList<int> simulation)
        {
            CheckArguments(real, simulation);

            var error = 0.0;

            for (var i = 0; i < real.Count(); i++)
            {
                var difference = real[i] - simulation[i];
                error += difference * difference;
            }

            return error / real.Count();
        }

        public double CalculateME(IList<int> real, IList<int> simulation)
        {
            CheckArguments(real, simulation);

            var error = 0.0;

            for (var i = 0; i < real.Count(); i++)
            {
                error += real[i] - simulation[i];
            }

            return error / real.Count();
        }

        public double CalculateMAE(IList<int> real, IList<int> simulation)
        {
            CheckArguments(real, simulation);

            var error = 0.0;

            for (var i = 0; i < real.Count(); i++)
            {
                error += Math.Abs(real[i] - simulation[i]);
            }

            return error / real.Count();
        }

        private void CheckArguments(IList<int> real, IList<int> simulation)
        {
            real.ThrowIfNullOrEmpty();
            simulation.ThrowIfNullOrEmpty();
            if (real.Count() != simulation.Count())
            {
                throw new ArgumentException("Length of provided int collections are not equal.");
            }
        }
    }
}
