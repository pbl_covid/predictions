﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models;
using PblCovid19Api.Common.Models.Charts;
using PblCovid19Api.Common.Utilities;
using PblCovid19Api.Logic.Charts.Errors;
using PblCovid19Api.Logic.Real;
using PblCovid19Api.ModelProxy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PblCovid19Api.Logic.Charts
{
    internal class CountryChartProvider : ICountryChartProvider
    {
        private readonly IEstimationModelProxy modelProxy;
        private readonly IRealDailyCaseDataProvider dailyCaseDataProvider;
        private readonly IErrorsCalculator errorsCalculator;

        public CountryChartProvider(IEstimationModelProxy modelsProxy, IRealDailyCaseDataProvider dailyCaseDataProvider, IErrorsCalculator errorsCalculator)
        {
            modelProxy = modelsProxy;
            this.dailyCaseDataProvider = dailyCaseDataProvider;
            this.errorsCalculator = errorsCalculator;
        }
        public async Task<CountryChart> GetCountryChartAsync(CountryChartRequest request)
        {
            request.ThrowIfNull();

            var real = await GetRealDataAsync(request).ConfigureAwait(false);
            if (request.SimulationDateFrom > real.First().Date)
            {
                throw new ArgumentException("SimulationDateFrom is older than the last real value");
            }

            var simulation = await GetSimulationDataAsync(request).ConfigureAwait(false);

            return new CountryChart
            {
                Simulation = simulation,
                Real = real,
                Errors = CalculateErrors(real, simulation, request)
            };
        }

        private Task<IEnumerable<DailyCase>> GetSimulationDataAsync(CountryChartRequest request)
        {
            return modelProxy.GetDataAsync(request);
        }

        private Task<IEnumerable<DailyCase>> GetRealDataAsync(CountryChartRequest request)
        {
            return dailyCaseDataProvider.GetDailyCasesAsync(request);
        }

        private ErrorsModel CalculateErrors(IEnumerable<DailyCase> real, IEnumerable<DailyCase> simulation, CountryChartRequest request)
        {
            var realDataJoin = real.Where(r => r.Date >= request.SimulationDateFrom);
            var simDataJoin = simulation.Where(s => s.Date <= real.First().Date);

            if (realDataJoin.Any() && simDataJoin.Any())
            {
                return errorsCalculator.CalculateAllErrors(realDataJoin.Select(r => r.Confirmed), simDataJoin.Select(s => s.Confirmed));
            }

            throw new ArgumentException("Real and simulation set have not common part.");
        }
    }
}
