﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Charts;
using System.Threading.Tasks;

namespace PblCovid19Api.Logic.Charts
{
    public interface ICountryChartProvider
    {
        Task<CountryChart> GetCountryChartAsync(CountryChartRequest request);
    }
}