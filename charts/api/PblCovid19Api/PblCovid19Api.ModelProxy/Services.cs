﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.DependencyInjection;
using PblCovid19Api.Common.Helpers;
using PblCovid19Api.ModelProxy.Models;
using PblCovid19Api.ModelProxy.Models.Matlab;
using PblCovid19Api.ModelProxy.Models.Mock;
using PblCovid19Api.ModelProxy.Models.Python;

namespace PblCovid19Api.ModelProxy
{
    public static class Services
    {
        /// <summary>
        /// Register all services from PblCovid19Api.ModelProxy.
        /// </summary>
        public static IServiceCollection AddModelServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IEstimationModelProxy, EstimationModelProxy>();
            serviceCollection.AddTransient<IMatlabPredictionsProvider, MatlabPredictionsProvider>();

            serviceCollection.AddTransient<IModelProxy, SirModelProxy>();
            // serviceCollection.AddTransient<IModelProxy, MockModelProxy>();
            serviceCollection.AddTransient<IModelProxy, PythonArimaModelProxy>();
            serviceCollection.AddTransient<IModelProxy, PythonArimaxModelProxy>();
            serviceCollection.AddTransient<IModelProxy, PythonVarModelProxy>();
            serviceCollection.AddTransient<IPythonPredictionsProvider, PythonPredictionsProvider>();

            if (OperatingSystem.IsWindows())
            {
                serviceCollection.AddTransient<IMatlabExecutor, MatlabWindowsExecutor>();
            }
            else
            {
                serviceCollection.AddTransient<IMatlabExecutor, MatlabLinuxExecutor>();
            }

            return serviceCollection;
        }
    }
}
