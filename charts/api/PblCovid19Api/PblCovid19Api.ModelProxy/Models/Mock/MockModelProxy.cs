﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Helpers;
using PblCovid19Api.Common.Models;
using PblCovid19Api.Common.Models.Database;
using PblCovid19Api.Infrastructure.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PblCovid19Api.ModelProxy.Models.Mock
{
    internal class MockModelProxy : IModelProxy
    {
        private readonly ICountryCaseRepositoryFactory _repositoryFactory;
        private readonly ICountryRepository _countryRepository;
        private readonly Random _random;

        public MockModelProxy(ICountryCaseRepositoryFactory repositoryFactory, ICountryRepository countryRepository)
        {
            _repositoryFactory = repositoryFactory;
            _countryRepository = countryRepository;
            _random = new Random();
        }

        public string Name => "mock";

        public async Task<IEnumerable<DailyCase>> GetDataAsync(string alpha3, int[] restrictionsVector, DateTime dateTimeFrom, DateTime dateTimeTo)
        {
            var lastUpdates = await GetLast14UpdatesAsync(alpha3, dateTimeFrom).ConfigureAwait(false);
            var sd = MathHelper.StandardDeviation(lastUpdates.Select(l => (double)l.Confirmed));
            var last = lastUpdates.First().Confirmed;
            var date = dateTimeFrom;
            var list = new List<DailyCase>();

            while (date <= dateTimeTo)
            {
                list.Add(new DailyCase
                {
                    Date = date,
                    Confirmed = last
                });

                last = Estimate(last, sd);
                date += TimeSpan.FromDays(1);
            }

            return list;
        }

        private async Task<IEnumerable<CountryCase>> GetLast14UpdatesAsync(string alpha3, DateTime dateTimeFrom)
        {
            var country = await _countryRepository.GetCountryForCountryAlpha3Async(alpha3).ConfigureAwait(false);
            var repository = _repositoryFactory.GetCountryCaseRepository(country.Code);
            var cases = await repository.GetLastFromDayCasesAsync(14, dateTimeFrom).ConfigureAwait(false);
            return cases;
        }

        private int Estimate(int last, double standardDeviation)
        {
            var rand = _random.NextDouble();
            var estimate = (int)(last + rand * standardDeviation);

            return estimate;
        }
    }
}
