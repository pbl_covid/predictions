﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PblCovid19Api.ModelProxy.Models
{
    /// <summary>
    /// Interface for prediction model proxy.
    /// </summary>
    public interface IModelProxy
    {
        /// <summary>
        /// Name of the model by which it is used. Should be unique;
        /// </summary>
        string Name { get; }
        /// <summary>
        /// Get prediction from provided model.
        /// </summary>
        /// <param name="country"></param>
        /// <param name="restrictionsVector"></param>
        /// <param name="dateTimeFrom"></param>
        /// <param name="dateTimeTo"></param>
        /// <returns></returns>
        Task<IEnumerable<DailyCase>> GetDataAsync(string country, int[] restrictionsVector, DateTime dateTimeFrom, DateTime dateTimeTo);
    }
}
