﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models;
using PblCovid19Api.Common.Models.Charts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PblCovid19Api.ModelProxy.Models
{
    /// <summary>
    /// Use to interface to call specific prediction model.
    /// </summary>
    public interface IEstimationModelProxy
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns>All available prediction models.</returns>
        IEnumerable<string> GetAvailableModels();

        /// <summary>
        /// Get predictions from provided model in a request.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Calculated predictions</returns>
        Task<IEnumerable<DailyCase>> GetDataAsync(CountryChartRequest request);
    }
}