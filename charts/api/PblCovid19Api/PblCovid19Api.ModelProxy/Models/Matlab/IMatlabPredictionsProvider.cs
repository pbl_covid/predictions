﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PblCovid19Api.ModelProxy.Models.Matlab
{
    public interface IMatlabPredictionsProvider
    {
        Task<IDictionary<DateTime, int>> GetPredictionsAsync(string country, int[] restrictionsVector, DateTime dateTimeFrom, DateTime dateTimeTo);
    }
}