// Gliwice, Politechnika Slaska 2020/2021
// Paczu�a Piotr, �abia�owicz Marek

using Newtonsoft.Json;
using System;
using System.Text;

namespace PblCovid19Api.ModelProxy.Models.Matlab.Models
{
    public sealed class MatlabRequest
    {
        public MatlabRequest(string country, int[] restrictions, DateTime dateTimeFrom, DateTime dateTimeTo)
        {
            DateFrom = dateTimeFrom;
            DateTo = dateTimeTo;
            Country = country;
            Restrictions = restrictions;
        }

        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string Country { get; set; }
        public int[] Restrictions { get; set; }

        [JsonIgnore]
        public string Hash => $"{Country}_{DateFrom:yyyyMMdd}_{DateTo:yyyyMMdd}_{GetRestrictionsString()}";

        public string GetRestrictionsString()
        {
            var sb = new StringBuilder("[");
            foreach (var item in Restrictions)
            {
                sb.Append(item.ToString());
                sb.Append(",");
            }

            sb.Remove(sb.Length - 1, 1);
            sb.Append("]");

            return sb.ToString();
        }
    }
}