﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;

namespace PblCovid19Api.ModelProxy.Models.Matlab.Models
{
    public sealed class MatlabDailyCase
    {
        public DateTime Date { get; set; }
        public int Confirmed { get; set; }
        public int Infected { get; set; }
    }
}
