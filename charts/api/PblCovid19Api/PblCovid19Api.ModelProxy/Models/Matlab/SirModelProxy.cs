﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PblCovid19Api.ModelProxy.Models.Matlab
{
    internal class SirModelProxy : IModelProxy
    {
        private readonly IMatlabPredictionsProvider _predictionsProvider;

        public SirModelProxy(IMatlabPredictionsProvider predictionsProvider)
        {
            _predictionsProvider = predictionsProvider;
        }
        public string Name => "sir";

        public async Task<IEnumerable<DailyCase>> GetDataAsync(string country, int[] restrictionsVector, DateTime dateTimeFrom, DateTime dateTimeTo)
        {
            var predictions = await _predictionsProvider.GetPredictionsAsync(country, restrictionsVector, dateTimeFrom, dateTimeTo).ConfigureAwait(false);

            return predictions
                .Select(p => new DailyCase
                {
                    Date = p.Key,
                    Confirmed = p.Value
                });
        }
    }
}
