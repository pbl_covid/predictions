﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PblCovid19Api.Common.DataFetcher;
using PblCovid19Api.Common.Extensions;
using PblCovid19Api.Common.Models;
using PblCovid19Api.Common.Utilities;
using PblCovid19Api.ModelProxy.Models.Matlab.Models;
using System.Diagnostics;

namespace PblCovid19Api.ModelProxy.Models.Matlab
{
    internal class MatlabLinuxExecutor : MatlabExecutorBase
    {
        private readonly string _file;
        private readonly string _path;
        private readonly string _script;
        private readonly string _outputCsvPath;
        private readonly IFileService _fileService;

        public MatlabLinuxExecutor(
            IDataFetcherService dataFetcherService,
            IFileService fileService,
            IConfiguration configuration,
            ILoggerFactory loggerFactory)
            : base(dataFetcherService, loggerFactory)
        {
            _fileService = fileService;
            _file = configuration[ConfigurationKeys.MatlabLinuxFile];
            _path = configuration[ConfigurationKeys.MatlabLinuxPath];
            _script = _fileService.GetAbsoluteDirectoryPath(configuration[ConfigurationKeys.MatlabLinuxScript].PathToFolderArray());
            _outputCsvPath = _fileService.GetAbsoluteDirectoryPath(configuration[ConfigurationKeys.OutputCsvPath].PathToFolderArray()); ;
            _fileService.CreateDirectory(_outputCsvPath);
        }

        public override ProcessStartInfo GetProcessStartInfo(MatlabRequest request)
        {
            return new ProcessStartInfo
            {
                FileName = _file,
                Arguments = GetArguments(request),
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                WorkingDirectory = _outputCsvPath
            };
        }

        private string GetArguments(MatlabRequest request)
        {
            var format = "MM-dd-yyyy";
            var dateFrom = request.DateFrom.ToString(format);
            var dateTo = request.DateTo.ToString(format);
            var countryCsv = GetCsvSourceFilePathAsync(request.Country).Result;
            return $"{_script} {_path} {dateFrom} {dateTo} {countryCsv} {request.Hash} {request.GetRestrictionsString()}";
        }
    }
}
