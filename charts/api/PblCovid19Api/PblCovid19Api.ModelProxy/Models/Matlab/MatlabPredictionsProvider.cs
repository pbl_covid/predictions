﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PblCovid19Api.Common.Extensions;
using PblCovid19Api.Common.Helpers.Files;
using PblCovid19Api.Common.Models;
using PblCovid19Api.Common.Utilities;
using PblCovid19Api.ModelProxy.Models.Matlab.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PblCovid19Api.ModelProxy.Models.Matlab
{
    internal class MatlabPredictionsProvider : IMatlabPredictionsProvider
    {
        private const int MAX_RESULT_FILES = 50;
        private const int FILES_AFTER_CLEANUP = 25;

        private readonly IFileService _fileService;
        private readonly IMatlabExecutor _matlabExecutor;
        private readonly ICsvReaderHelper _csvReader;
        private readonly string _path;
        private readonly ILogger<MatlabPredictionsProvider> _logger;

        public MatlabPredictionsProvider(
            IFileService fileService,
            IMatlabExecutor matlabExecutor,
            ICsvReaderHelper csvReader,
            IConfiguration configuration,
            ILoggerFactory loggerFactory)
        {
            _fileService = fileService;
            _matlabExecutor = matlabExecutor;
            _csvReader = csvReader;
            var outputCsvPath = configuration[ConfigurationKeys.OutputCsvPath].PathToFolderArray(); ;
            EnsureOutputFolderCreated(outputCsvPath);
            _path = fileService.GetAbsoluteDirectoryPath(outputCsvPath);
            _logger = loggerFactory.CreateLogger<MatlabPredictionsProvider>();
        }

        private void EnsureOutputFolderCreated(params string[] relativePath)
        {
            _fileService.CreateDirectory(relativePath);
        }

        private async Task RemoveOldestResultsAsync()
        {
            var outputDir = new DirectoryInfo(_path);
            if (outputDir.GetFiles().Count() > MAX_RESULT_FILES)
            {
                await _fileService.RemoveOldestFilesAsync(outputDir.FullName, FILES_AFTER_CLEANUP);
            }
        }

        public async Task<IDictionary<DateTime, int>> GetPredictionsAsync(string country, int[] restrictions, DateTime dateTimeFrom, DateTime dateTimeTo)
        {
            await RemoveOldestResultsAsync(); // Clear oldest cached files
            var matlabRequest = new MatlabRequest(country, restrictions, dateTimeFrom, dateTimeTo);
            var hash = matlabRequest.Hash;
            var filePath = Path.Combine(_path, $"{hash}.csv");

            IEnumerable<MatlabDailyCase> csvData = null;
            await _fileService.UseFileAsync(async file =>
            {
                if (file == null)
                {
                    _logger.LogDebug($"There is no file {filePath}");
                    return;
                }
                using (var stream = file.OpenText())
                {
                    csvData = await ReadMatlabCsvAsync(stream);
                }
            }, filePath);

            if (csvData == null)
            {
                _matlabExecutor.Execute(matlabRequest);
            }

            await _fileService.UseFileAsync(async file =>
            {
                using (var stream = file.OpenText())
                {
                    csvData = await ReadMatlabCsvAsync(stream);
                }
            }, filePath);

            return csvData.ToDictionary(c => c.Date, c => c.Confirmed);
        }

        private Task<IEnumerable<MatlabDailyCase>> ReadMatlabCsvAsync(StreamReader stream)
        {
            return _csvReader.ReadCsvAsync<MatlabDailyCase>(stream);
        }
    }
}
