﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.Logging;
using PblCovid19Api.Common.DataFetcher;
using PblCovid19Api.ModelProxy.Models.Matlab.Models;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PblCovid19Api.ModelProxy.Models.Matlab
{
    internal abstract class MatlabExecutorBase : IMatlabExecutor
    {
        private readonly IDataFetcherService _dataFetcherService;
        protected readonly ILogger _logger;

        public MatlabExecutorBase(IDataFetcherService dataFetcherService, ILoggerFactory loggerFactory)
        {
            _dataFetcherService = dataFetcherService;
            _logger = loggerFactory.CreateLogger(GetType());
        }

        protected async Task<string> GetCsvSourceFilePathAsync(string countryAlpha)
        {
            var filePath = "";
            await _dataFetcherService.UseCsvDataFileAsync(countryAlpha, file =>
            {
                filePath = file.FullName;
            });

            return filePath;
        }

        public abstract ProcessStartInfo GetProcessStartInfo(MatlabRequest request);

        public void Execute(MatlabRequest request)
        {
            _logger.LogDebug($"Executing matlab for request: {request.Hash}");
            var proc = new Process
            {
                StartInfo = GetProcessStartInfo(request),
            };


            _logger.LogDebug("Matlab filename: " + proc.StartInfo.FileName);
            _logger.LogDebug("Matlab arguments: " + proc.StartInfo.Arguments);
            _logger.LogDebug("Matlab working directory: " + proc.StartInfo.WorkingDirectory);
            proc.Start();

            using (var readerSt = proc.StandardOutput)
            using (var readerErr = proc.StandardError)
            {
                var stout = readerSt.ReadToEnd();
                var errout = readerErr.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(errout))
                {
                    _logger.LogError("Standard error from matlab: \n" + errout);
                }
                if (!string.IsNullOrWhiteSpace(stout))
                {
                    _logger.LogDebug("Standard output from matlab: \n" + stout);
                }
            }

            proc.WaitForExit();
        }
    }
}
