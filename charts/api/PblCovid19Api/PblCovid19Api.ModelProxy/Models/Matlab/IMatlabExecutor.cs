﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.ModelProxy.Models.Matlab.Models;

namespace PblCovid19Api.ModelProxy.Models.Matlab
{
    public interface IMatlabExecutor
    {
        void Execute(MatlabRequest request);
    }
}