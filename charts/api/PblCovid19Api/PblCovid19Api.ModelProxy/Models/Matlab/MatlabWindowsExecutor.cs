﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PblCovid19Api.Common.DataFetcher;
using PblCovid19Api.Common.Extensions;
using PblCovid19Api.Common.Models;
using PblCovid19Api.Common.Utilities;
using PblCovid19Api.ModelProxy.Models.Matlab.Models;
using System.Diagnostics;

namespace PblCovid19Api.ModelProxy.Models.Matlab
{
    internal class MatlabWindowsExecutor : MatlabExecutorBase
    {
        private readonly string _exePath;
        private readonly string[] _outputCsvPath;
        private readonly IFileService _fileService;

        public MatlabWindowsExecutor(
            IDataFetcherService dataFetcherService,
            IFileService fileService,
            IConfiguration configuration,
            ILoggerFactory loggerFactory)
            : base(dataFetcherService, loggerFactory)
        {
            _exePath = fileService.GetAbsoluteDirectoryPath(configuration[ConfigurationKeys.MatlabWindowPath].PathToFolderArray());
            _fileService = fileService;
            _outputCsvPath = configuration[ConfigurationKeys.OutputCsvPath].PathToFolderArray(); ;
            _fileService.CreateDirectory(_outputCsvPath);
        }

        public override ProcessStartInfo GetProcessStartInfo(MatlabRequest request)
        {
            return new ProcessStartInfo
            {
                FileName = _exePath,
                Arguments = GetArguments(request),
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                WorkingDirectory = _fileService.GetAbsoluteDirectoryPath(_outputCsvPath)
            };
        }

        private string GetArguments(MatlabRequest request)
        {
            var format = "MM-dd-yyyy";
            var dateFrom = request.DateFrom.ToString(format);
            var dateTo = request.DateTo.ToString(format);
            var countryCsv = GetCsvSourceFilePathAsync(request.Country).Result;

            return $"{dateFrom} {dateTo} {countryCsv} {request.Hash} {request.GetRestrictionsString()}";
        }
    }
}
