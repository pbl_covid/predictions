﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models;
using PblCovid19Api.Common.Models.Charts;
using PblCovid19Api.Common.Models.Restrictions.Converters;
using PblCovid19Api.Common.Utilities;
using PblCovid19Api.Infrastructure.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PblCovid19Api.ModelProxy.Models
{
    internal class EstimationModelProxy : IEstimationModelProxy
    {
        private readonly IEnumerable<IModelProxy> models;
        private readonly ICountryRepository countryRepository;

        public EstimationModelProxy(IEnumerable<IModelProxy> models, ICountryRepository countryRepository)
        {
            this.models = models;
            this.countryRepository = countryRepository;
        }

        public IEnumerable<string> GetAvailableModels()
        {
            return models.Select(m => m.Name).OrderBy(n => n);
        }

        public async Task<IEnumerable<DailyCase>> GetDataAsync(CountryChartRequest request)
        {
            var model = GetModel(request.Model);
            var country = await GetCountryAlpha3CodeAsync(request.CountryCode).ConfigureAwait(false);
            var restrictions = RestrictionModelConverter.RestrictionsToVector(request.Restrictions);
            var data = await model.GetDataAsync(country, restrictions, request.SimulationDateFrom, request.DateTo).ConfigureAwait(false);
            return data
                .Where(d => d.Date >= request.SimulationDateFrom && d.Date <= request.DateTo)
                .OrderByDescending(d => d.Date);
        }

        private async Task<string> GetCountryAlpha3CodeAsync(string countryCode)
        {
            var country = await countryRepository.GetCountryForCountryCodeAsync(countryCode).ConfigureAwait(false);
            return country.Alpha3;
        }

        private IModelProxy GetModel(string name)
        {
            name.ThrowIfNullOrEmpty();

            var model = models.FirstOrDefault(m => m.Name == name);
            if (model != null)
            {
                return model;
            }
            else
            {
                throw new ArgumentException("There is no registered {name} model", name);
            }
        }
    }
}
