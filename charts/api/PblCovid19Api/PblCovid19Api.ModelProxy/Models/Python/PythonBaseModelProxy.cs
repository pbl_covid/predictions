﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.Logging;
using PblCovid19Api.Common.DataFetcher;
using PblCovid19Api.Common.Helpers;
using PblCovid19Api.Common.Models;
using PblCovid19Api.ModelProxy.Models.Python.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PblCovid19Api.ModelProxy.Models.Python
{
    internal abstract class PythonBaseModelProxy : IModelProxy
    {
        private readonly ICsvSourceDataProvider _dataProvider;
        private readonly IPythonPredictionsProvider _pythonPredictions;
        private readonly ILogger _logger;

        public virtual string Name => "arima";

        public PythonBaseModelProxy(
            ICsvSourceDataProvider dataProvider,
            ILoggerFactory loggerFactory,
            IPythonPredictionsProvider pythonPredictions)
        {
            _dataProvider = dataProvider;
            _pythonPredictions = pythonPredictions;
            _logger = loggerFactory.CreateLogger<PythonBaseModelProxy>();
        }

        public async Task<IEnumerable<DailyCase>> GetDataAsync(string countryAlpha, int[] restrictionsVector, DateTime dateTimeFrom, DateTime dateTimeTo)
        {
            var data = await _dataProvider.GetCsvSourceDataAsync(countryAlpha).ConfigureAwait(false);

            var model = new PythonRequestModel
            {
                Data = data,
                DateFrom = dateTimeFrom,
                DateTo = dateTimeTo,
                Model = Name,
                Restrictions = restrictionsVector
            };

            var predictions = await _pythonPredictions.GetPredictionsAsync(model).ConfigureAwait(false);

            return predictions.Predictions.Select(p => new DailyCase
            {
                Date = DateTimeHelper.GetDateTimeFromUnixMiliseconds(p.Key),
                Confirmed = (int)p.Value
            });
        }
    }
}
