﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Csv;
using System;
using System.Collections.Generic;

namespace PblCovid19Api.ModelProxy.Models.Python.Models
{
    public sealed class PythonRequestModel
    {
        public IEnumerable<CsvDataRecord> Data { get; set; }
        public int[] Restrictions { get; set; }
        public string Model { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
