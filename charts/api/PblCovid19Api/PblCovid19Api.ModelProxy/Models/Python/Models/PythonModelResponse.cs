﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Newtonsoft.Json;
using System.Collections.Generic;

namespace PblCovid19Api.ModelProxy.Models.Python.Models
{
    public class PythonModelResponse
    {
        [JsonProperty("Predictions")]
        public IDictionary<long, decimal> Predictions { get; set; }
    }
}
