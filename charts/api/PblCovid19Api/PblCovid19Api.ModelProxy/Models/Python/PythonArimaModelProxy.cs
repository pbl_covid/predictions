﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.Logging;
using PblCovid19Api.Common.DataFetcher;

namespace PblCovid19Api.ModelProxy.Models.Python
{
    internal class PythonArimaModelProxy : PythonBaseModelProxy
    {
        public override string Name => "arima";

        public PythonArimaModelProxy(
            IPythonPredictionsProvider pythonPredictions,
            ICsvSourceDataProvider dataProvider,
            ILoggerFactory loggerFactory) : base(dataProvider, loggerFactory, pythonPredictions)
        {
        }
    }
}
