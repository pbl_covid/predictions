﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PblCovid19Api.Common.Http;
using PblCovid19Api.Common.Models;
using PblCovid19Api.ModelProxy.Models.Python.Models;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PblCovid19Api.ModelProxy.Models.Python
{
    internal sealed class PythonPredictionsProvider : IPythonPredictionsProvider
    {
        private readonly string _apiUrl;
        private readonly ILogger _logger;
        private readonly IHttpClientWrapper _httpClient;

        public PythonPredictionsProvider(ILoggerFactory loggerFactory, IHttpClientWrapper httpClient, IConfiguration configuration)
        {
            _apiUrl = Environment.GetEnvironmentVariable("PYTHON_URL") ?? configuration[ConfigurationKeys.PythonModelUrl];
            _logger = loggerFactory.CreateLogger<PythonPredictionsProvider>();
            _httpClient = httpClient;
        }

        public async Task<PythonModelResponse> GetPredictionsAsync(PythonRequestModel requestModel)
        {
            var url = PrepareUrl(requestModel.Model, requestModel.DateFrom, requestModel.DateTo);

            var stopwatch = Stopwatch.StartNew();
            _logger.LogDebug($"Sending estimation request to {url}");
            var predictions = await _httpClient.PostAsync<PythonModelResponse>(url, requestModel).ConfigureAwait(false);
            stopwatch.Stop();
            _logger.LogDebug($"Estimation request {url} took {stopwatch.Elapsed}");

            return predictions;
        }

        private string PrepareUrl(string model, DateTime from, DateTime to)
        {
            var format = "yyyy-MM-dd";
            var dateTimeFrom = from.ToString(format);
            var dateTimeTo = to.ToString(format);

            return $"{_apiUrl}/{model}/{dateTimeFrom}/{dateTimeTo}";
        }
    }
}