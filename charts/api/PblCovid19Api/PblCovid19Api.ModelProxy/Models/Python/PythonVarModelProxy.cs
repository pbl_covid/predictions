﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.Logging;
using PblCovid19Api.Common.DataFetcher;

namespace PblCovid19Api.ModelProxy.Models.Python
{
    internal class PythonVarModelProxy : PythonBaseModelProxy
    {
        public override string Name => "var";

        public PythonVarModelProxy(
            IPythonPredictionsProvider pythonPredictions,
            ICsvSourceDataProvider dataProvider,
            ILoggerFactory loggerFactory) : base(dataProvider, loggerFactory, pythonPredictions)
        {
        }
    }
}
