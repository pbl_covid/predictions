﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.ModelProxy.Models.Python.Models;
using System.Threading.Tasks;

namespace PblCovid19Api.ModelProxy.Models.Python
{
    public interface IPythonPredictionsProvider
    {
        Task<PythonModelResponse> GetPredictionsAsync(PythonRequestModel requestModel);
    }
}