﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.Logging;
using PblCovid19Api.Common.DataFetcher;

namespace PblCovid19Api.ModelProxy.Models.Python
{
    internal class PythonArimaxModelProxy : PythonBaseModelProxy
    {
        public override string Name => "arimax";

        public PythonArimaxModelProxy(
            IPythonPredictionsProvider pythonPredictions,
            ICsvSourceDataProvider dataProvider,
            ILoggerFactory loggerFactory) : base(dataProvider, loggerFactory, pythonPredictions)
        {
        }
    }
}
