﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PblCovid19Api.Common.Models.Database;
using PblCovid19Api.Common.Models.Restrictions.ClosuresAndContainment;
using PblCovid19Api.Common.Models.Restrictions.EconomicResponse;
using PblCovid19Api.Common.Models.Restrictions.HealthSystem;
using PblCovid19Api.Common.Models.Restrictions.Miscellaneous;
using PblCovid19Api.Infrastructure.Logic;
using System.Globalization;

namespace PblCovid19Api.Infrastructure.Database.Extensions
{
    public static class EntityBuilderExtensions
    {
        public static void AddRestrictions<T>(this EntityTypeBuilder<T> entity) where T : CountryCase
        {
            entity.Property(e => e.SchoolClosing).HasColumnName("Restriction_C1").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<SchoolClosing>(s));

            entity.Property(e => e.WorkplaceClosing).HasColumnName("Restriction_C2").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<WorkplaceClosing>(s));
            entity.Property(e => e.CancelPublicEvents).HasColumnName("Restriction_C3").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<CancelPublicEvents>(s));
            entity.Property(e => e.RestrictionsOnGatherings).HasColumnName("Restriction_C4").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<RestrictionsOnGatherings>(s));
            entity.Property(e => e.ClosePublicTransport).HasColumnName("Restriction_C5").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<ClosePublicTransport>(s));
            entity.Property(e => e.StayAtHome).HasColumnName("Restriction_C6").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<StayAtHome>(s));
            entity.Property(e => e.InternalMovement).HasColumnName("Restriction_C7").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<InternalMovement>(s));
            entity.Property(e => e.InternationalTravelControls).HasColumnName("Restriction_C8").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<InternationalTravelControls>(s));

            entity.Property(e => e.E1).HasColumnName("Restriction_E1").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<E1>(s));
            entity.Property(e => e.E2).HasColumnName("Restriction_E2").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<E2>(s));
            entity.Property(e => e.FiscalMeasures).HasColumnName("Restriction_E3").HasConversion(v => v.ToString(), s => long.Parse(s, NumberStyles.Number));
            entity.Property(e => e.E4).HasColumnName("Restriction_E4").HasConversion(v => v.ToString(), s => long.Parse(s, NumberStyles.Number));

            entity.Property(e => e.PublicInfoCampaings).HasColumnName("Restriction_H1").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<PublicInfoCampaings>(s));
            entity.Property(e => e.TestingPolicy).HasColumnName("Restriction_H2").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<TestingPolicy>(s));
            entity.Property(e => e.ContactTracing).HasColumnName("Restriction_H3").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<ContactTracing>(s));
            entity.Property(e => e.EmergencyInvestmentInHealthCare).HasColumnName("Restriction_H4").HasConversion(v => v.ToString(), s => long.Parse(s, NumberStyles.Number));
            entity.Property(e => e.InvestmentInVaccines).HasColumnName("Restriction_H5").HasConversion(v => v.ToString(), s => long.Parse(s, NumberStyles.Number));

            entity.Property(e => e.M1).HasColumnName("Restriction_M1").HasConversion(v => v.ToString(), s => RestrictionsStringReader.Parse<M1>(s));
        }

        public static void RegisterAsCountryCase<T>(this EntityTypeBuilder<T> entity, string countryCode) where T : CountryCase
        {
            entity.ToTable(countryCode);

            entity.Property(e => e.City)
                .HasMaxLength(255)
                .IsUnicode(false);

            entity.Property(e => e.CityCode)
                .HasMaxLength(255)
                .IsUnicode(false);

            entity.Property(e => e.LastUpdate).HasColumnType("date");

            entity.Property(e => e.Province)
                .HasMaxLength(255)
                .IsUnicode(false);

            entity.AddRestrictions();
        }

        public static void RegisterViewAsCountryCase<T>(this EntityTypeBuilder<T> entity, string viewCode) where T : CountryCase
        {
            entity.HasNoKey();

            entity.ToView(viewCode);

            entity.Property(e => e.City)
                .HasMaxLength(255)
                .IsUnicode(false);

            entity.Property(e => e.CityCode)
                .HasMaxLength(255)
                .IsUnicode(false);

            entity.Property(e => e.Id).ValueGeneratedOnAdd();

            entity.Property(e => e.LastUpdate).HasColumnType("date");

            entity.Property(e => e.Province)
                .HasMaxLength(255)
                .IsUnicode(false);

            entity.AddRestrictions();
        }
    }
}
