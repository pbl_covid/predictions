﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Database;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PblCovid19Api.Infrastructure.Database.Repository
{
    public interface ICountryCaseRepository
    {
        Task<IEnumerable<CountryCase>> GetCountryCasesAsync(DateTime dateFrom, DateTime dateTo);
        Task<CountryCase> GetCountryCaseAsync(DateTime datetime);
        Task<IEnumerable<CountryCase>> GetLastFromDayCasesAsync(int count, DateTime day);
    }
}
