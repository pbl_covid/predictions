﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Database;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PblCovid19Api.Infrastructure.Database.Repository
{
    public interface ICountryRepository
    {
        Task<IEnumerable<Country>> GetCountriesAsync();
        Task<Country> GetCountryForCountryCodeAsync(string countryCode);
        Task<Country> GetCountryForCountryAlpha3Async(string alpha3);
    }
}