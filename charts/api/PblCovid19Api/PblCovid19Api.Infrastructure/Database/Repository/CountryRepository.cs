﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.EntityFrameworkCore;
using PblCovid19Api.Common.Models.Database;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PblCovid19Api.Infrastructure.Database.Repository
{
    internal sealed class CountryRepository : ICountryRepository
    {
        private readonly PblCovidDbContext _dbContext;

        public CountryRepository(PblCovidDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Country>> GetCountriesAsync()
        {
            var countries = await _dbContext.Countries.AsNoTracking().ToListAsync().ConfigureAwait(false);
            return countries;
        }

        public async Task<Country> GetCountryForCountryAlpha3Async(string alpha3)
        {
            var country = await _dbContext.Countries.FirstOrDefaultAsync(c => c.Alpha3 == alpha3).ConfigureAwait(false);
            return country;
        }

        public async Task<Country> GetCountryForCountryCodeAsync(string countryCode)
        {
            var country = await _dbContext.Countries.FirstOrDefaultAsync(c => c.Code == countryCode).ConfigureAwait(false);
            return country;
        }
    }
}
