﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using PblCovid19Api.Common.Models.Database;
using System;

namespace PblCovid19Api.Infrastructure.Database.Repository
{
    internal class CountryCaseRepositoryFactory : ICountryCaseRepositoryFactory
    {
        private readonly PblCovidDbContext _dbContext;

        public CountryCaseRepositoryFactory(PblCovidDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ICountryCaseRepository GetCountryCaseRepository(string countryCode)
        {
            return countryCode switch
            {
                Argentina.Code => new CountryCaseRepository<Argentina>(_dbContext.ArgentinaSet),
                Austria.Code => new CountryCaseRepository<Austria>(_dbContext.AustriaSet),
                Brazil.Code => new CountryCaseRepository<Brazil>(_dbContext.BrazilSet),
                China.Code => new CountryCaseRepository<China>(_dbContext.ChinaSet),
                CzechRepublic.Code => new CountryCaseRepository<CzechRepublic>(_dbContext.CzechRepublicSet),
                Germany.Code => new CountryCaseRepository<Germany>(_dbContext.GermanySet),
                Ireland.Code => new CountryCaseRepository<Ireland>(_dbContext.IrelandSet),
                Italy.Code => new CountryCaseRepository<Italy>(_dbContext.ItalySet),
                Japan.Code => new CountryCaseRepository<Japan>(_dbContext.JapanSet),
                Mexico.Code => new CountryCaseRepository<Mexico>(_dbContext.MexicoSet),
                NewZealand.Code => new CountryCaseRepository<NewZealand>(_dbContext.NewZealandSet),
                Poland.Code => new CountryCaseRepository<Poland>(_dbContext.PolandSet),
                Slovakia.Code => new CountryCaseRepository<Slovakia>(_dbContext.SlovakiaSet),
                SouthAfrica.Code => new CountryCaseRepository<SouthAfrica>(_dbContext.SouthAfricaSet),
                Spain.Code => new CountryCaseRepository<Spain>(_dbContext.SpainSet),
                Ukraine.Code => new CountryCaseRepository<Ukraine>(_dbContext.UkraineSet),
                Belgium.Code => new CountryCaseRepository<Belgium>(_dbContext.BelgiumSet),
                Iceland.Code => new CountryCaseRepository<Iceland>(_dbContext.IcelandSet),
                Israel.Code => new CountryCaseRepository<Israel>(_dbContext.IsraelSet),
                Jamaica.Code => new CountryCaseRepository<Jamaica>(_dbContext.JamaicaSet),

                UnitedKingdom.Code => new CountryCaseRepository<UkView>(_dbContext.UkView),
                UnitedStates.Code => new CountryCaseRepository<UsaView>(_dbContext.UsaView),
                Canada.Code => new CountryCaseRepository<CanadaView>(_dbContext.CanadaView),
                France.Code => new CountryCaseRepository<FranceView>(_dbContext.FranceView),

                _ => throw new ArgumentException($"Not found table for provided country code : {countryCode}")
            };
        }
    }
}
