﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.EntityFrameworkCore;
using PblCovid19Api.Common.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PblCovid19Api.Infrastructure.Database.Repository
{
    internal sealed class CountryCaseRepository<T> : ICountryCaseRepository where T : CountryCase
    {
        private readonly DbSet<T> _dbSet;

        public CountryCaseRepository(DbSet<T> dbSet)
        {
            _dbSet = dbSet;
        }

        public async Task<IEnumerable<CountryCase>> GetCountryCasesAsync(DateTime dateFrom, DateTime dateTo)
        {
            var cases = await _dbSet
                .AsQueryable()
                .Where(c => c.LastUpdate >= dateFrom && c.LastUpdate <= dateTo)
                .OrderByDescending(c => c.LastUpdate)
                .AsNoTracking()
                .ToListAsync();

            return cases;
        }

        public async Task<CountryCase> GetCountryCaseAsync(DateTime datetime)
        {
            var countryCase = await _dbSet
                .AsQueryable()
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.LastUpdate.Date == datetime.Date);

            return countryCase;
        }

        public async Task<IEnumerable<CountryCase>> GetLastFromDayCasesAsync(int count, DateTime day)
        {
            var cases = await _dbSet
                .AsQueryable()
                .OrderByDescending(c => c.LastUpdate)
                .Where(c => c.LastUpdate <= day)
                .Take(count)
                .AsNoTracking()
                .ToListAsync();

            return cases;
        }
    }
}
