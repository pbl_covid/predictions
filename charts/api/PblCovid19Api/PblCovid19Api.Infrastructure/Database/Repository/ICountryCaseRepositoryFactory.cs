﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

namespace PblCovid19Api.Infrastructure.Database.Repository
{
    public interface ICountryCaseRepositoryFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns>Provided country's repository</returns>
        ICountryCaseRepository GetCountryCaseRepository(string countryCode);
    }
}