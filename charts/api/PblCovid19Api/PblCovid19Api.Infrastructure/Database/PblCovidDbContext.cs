﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.EntityFrameworkCore;
using PblCovid19Api.Common.Models.Database;
using PblCovid19Api.Infrastructure.Database.Extensions;

#nullable disable

namespace PblCovid19Api.Infrastructure.Database
{
    public partial class PblCovidDbContext : DbContext
    {
        public PblCovidDbContext()
        {
        }

        public PblCovidDbContext(DbContextOptions<PblCovidDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Argentina> ArgentinaSet { get; set; }
        public virtual DbSet<Austria> AustriaSet { get; set; }
        public virtual DbSet<Brazil> BrazilSet { get; set; }
        public virtual DbSet<China> ChinaSet { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<CzechRepublic> CzechRepublicSet { get; set; }
        public virtual DbSet<Germany> GermanySet { get; set; }
        public virtual DbSet<Ireland> IrelandSet { get; set; }
        public virtual DbSet<Italy> ItalySet { get; set; }
        public virtual DbSet<Japan> JapanSet { get; set; }
        public virtual DbSet<Mexico> MexicoSet { get; set; }
        public virtual DbSet<NewZealand> NewZealandSet { get; set; }
        public virtual DbSet<Poland> PolandSet { get; set; }
        public virtual DbSet<Slovakia> SlovakiaSet { get; set; }
        public virtual DbSet<SouthAfrica> SouthAfricaSet { get; set; }
        public virtual DbSet<Spain> SpainSet { get; set; }
        public virtual DbSet<Ukraine> UkraineSet { get; set; }
        public virtual DbSet<Jamaica> JamaicaSet { get; set; }
        public virtual DbSet<Iceland> IcelandSet { get; set; }
        public virtual DbSet<Israel> IsraelSet { get; set; }
        public virtual DbSet<Belgium> BelgiumSet { get; set; }
        public virtual DbSet<UsaView> UsaView { get; set; }
        public virtual DbSet<CanadaView> CanadaView { get; set; }
        public virtual DbSet<UkView> UkView { get; set; }
        public virtual DbSet<FranceView> FranceView { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("countries");

                entity.Property(e => e.Code)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Alpha3)
                .HasMaxLength(255)
                .IsUnicode(false);

                entity.Property(e => e.RestrictionsLastUpdate).HasColumnType("date");
            });

            modelBuilder.Entity<Argentina>(entity =>
            {
                entity.RegisterAsCountryCase(Argentina.Code);
            });

            modelBuilder.Entity<Austria>(entity =>
            {
                entity.RegisterAsCountryCase(Austria.Code);
            });

            modelBuilder.Entity<Brazil>(entity =>
            {
                entity.RegisterAsCountryCase(Brazil.Code);
            });

            modelBuilder.Entity<Canada>(entity =>
            {
                entity.RegisterAsCountryCase(Canada.Code);
            });

            modelBuilder.Entity<China>(entity =>
            {
                entity.RegisterAsCountryCase(China.Code);
            });

            modelBuilder.Entity<CzechRepublic>(entity =>
            {
                entity.RegisterAsCountryCase(CzechRepublic.Code);
            });

            modelBuilder.Entity<France>(entity =>
            {
                entity.RegisterAsCountryCase(France.Code);
            });

            modelBuilder.Entity<Germany>(entity =>
            {
                entity.RegisterAsCountryCase(Germany.Code);
            });

            modelBuilder.Entity<Ireland>(entity =>
            {
                entity.RegisterAsCountryCase(Ireland.Code);
            });

            modelBuilder.Entity<Italy>(entity =>
            {
                entity.RegisterAsCountryCase(Italy.Code);
            });

            modelBuilder.Entity<Japan>(entity =>
            {
                entity.RegisterAsCountryCase(Japan.Code);
            });

            modelBuilder.Entity<Mexico>(entity =>
            {
                entity.RegisterAsCountryCase(Mexico.Code);
            });

            modelBuilder.Entity<NewZealand>(entity =>
            {
                entity.RegisterAsCountryCase(NewZealand.Code);
            });

            modelBuilder.Entity<Poland>(entity =>
            {
                entity.RegisterAsCountryCase(Poland.Code);
            });

            modelBuilder.Entity<Slovakia>(entity =>
            {
                entity.RegisterAsCountryCase(Slovakia.Code);
            });

            modelBuilder.Entity<SouthAfrica>(entity =>
            {
                entity.RegisterAsCountryCase(SouthAfrica.Code);
            });

            modelBuilder.Entity<Spain>(entity =>
            {
                entity.RegisterAsCountryCase(Spain.Code);
            });

            modelBuilder.Entity<Ukraine>(entity =>
            {
                entity.RegisterAsCountryCase(Ukraine.Code);
            });

            modelBuilder.Entity<UnitedKingdom>(entity =>
            {
                entity.RegisterAsCountryCase(UnitedKingdom.Code);
            });

            modelBuilder.Entity<UnitedStates>(entity =>
            {
                entity.RegisterAsCountryCase(UnitedStates.Code);
            });

            modelBuilder.Entity<Jamaica>(entity =>
            {
                entity.RegisterAsCountryCase(Jamaica.Code);
            });

            modelBuilder.Entity<Iceland>(entity =>
            {
                entity.RegisterAsCountryCase(Iceland.Code);
            });

            modelBuilder.Entity<Israel>(entity =>
            {
                entity.RegisterAsCountryCase(Israel.Code);
            });

            modelBuilder.Entity<Belgium>(entity =>
            {
                entity.RegisterAsCountryCase(Belgium.Code);
            });

            modelBuilder.Entity<UsaView>(entity =>
            {
                entity.RegisterViewAsCountryCase(Common.Models.Database.UsaView.Code);
            });

            modelBuilder.Entity<CanadaView>(entity =>
            {
                entity.RegisterViewAsCountryCase(Common.Models.Database.CanadaView.Code);
            });

            modelBuilder.Entity<UkView>(entity =>
            {
                entity.RegisterViewAsCountryCase(Common.Models.Database.UkView.Code);
            });

            modelBuilder.Entity<FranceView>(entity =>
            {
                entity.RegisterViewAsCountryCase(Common.Models.Database.FranceView.Code);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
