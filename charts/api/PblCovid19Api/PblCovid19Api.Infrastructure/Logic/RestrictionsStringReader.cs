﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using System;

namespace PblCovid19Api.Infrastructure.Logic
{
    public static class RestrictionsStringReader
    {
        public static TEnum? Parse<TEnum>(string s) where TEnum : struct, Enum
        {
            if (int.TryParse(s, out var _))
            {
                return (TEnum)Enum.Parse(typeof(TEnum), s);
            }
            return null;
        }
    }
}
