﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using Microsoft.Extensions.DependencyInjection;
using PblCovid19Api.Infrastructure.Database.Repository;

namespace PblCovid19Api.Infrastructure
{
    public static class Services
    {
        /// <summary>
        /// Register all services from PblCovid19Api.Infrastructure.
        /// </summary>
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICountryCaseRepositoryFactory, CountryCaseRepositoryFactory>();
            serviceCollection.AddTransient<ICountryRepository, CountryRepository>();

            return serviceCollection;
        }
    }
}
