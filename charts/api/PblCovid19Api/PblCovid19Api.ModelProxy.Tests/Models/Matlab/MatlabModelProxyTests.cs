﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using AutoFixture;
using AutoFixture.AutoMoq;
using NUnit.Framework;
using PblCovid19Api.ModelProxy.Models;
using PblCovid19Api.ModelProxy.Models.Matlab;

namespace PblCovid19Api.ModelProxy.Tests.Models.Matlab
{
    [TestFixture]
    internal class MatlabModelProxyTests
    {
        private IFixture _fixture;

        [SetUp]
        public void SetUp()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
        }

        private IModelProxy CreateSut()
        {
            return _fixture.Create<SirModelProxy>();
        }

        //[Test]
        //public async Task GetDataAsync_WhenRequestProvided_ThenItShouldReturn10Days()
        //{
        //    var request = new CountryChartRequest
        //    {
        //        SimulationDateFrom = new DateTime(2018, 10, 1),
        //        DateTo = new DateTime(2018,10,10)
        //    };

        //    var sut = CreateSut();
        //    var result = await sut.GetDataAsync(request).ConfigureAwait(false);
        //    result.Should().HaveCount(10);
        //}
    }
}
