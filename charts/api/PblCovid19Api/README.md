# ABOUT

Main API code.

# REQUIREMENTS

- ASP.NET Core 3.1 sdk
- Matlab both on Windows and Linux
- Visual Studio

# HOW TO DEBUG

Easiest way to debug the code is to run it in Visual Studio. Remember to start exteral services if you want to use them and provide correct URL in `appsettings.Development.json`

## Updating SIR model
After updating SIR model you will have to compile it to executable file (.exe for windows and .sh + binary file for linux)

1. Windows locally
Replace existing executable file in `PblCovid19Api/Resources/Windows`, keep file name the same. 

2. Linux locally / Docker
Replace existing executable files in `PblCovid19Api/Resources/Linux`, keep file names the same. For docker you should also replace .zip file with new code (also keep name the same) 

# HOW TO ADD A NEW PREDICTION MODEL
All models' proxys are stored in project `PblCovid19Api.ModelProxy`. There are 2 predictions providers (and one extra mock for testing).

If you want to add a new model you have to add a new class which impelemnts `IModelProxy` and register it in `PblCovid19Api.ModelProxy/Services.cs`. Matlab provider is set to execute this specific function, but it can be easly fixed. If you want to add a python model, just provide a correct URL and prepare the python api.

# COMMON PROBLEMS
__! BE CAREFUL - SIR bash script has often its whitespace edited due to end of line environment differences in Windows and Linux. It may cause script to stop working.__