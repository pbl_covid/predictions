# HOW TO

1. Build docker image from `utility/aspnetcore3.1_matlab-runtime.DEBUG.dockerfile` with tag `aspnetcore-matlab-debug`
2. Build docker image ./dockerfile.debug with tag `pbl-chart-api-debug`
3. Run `docker run -it --rm -p 5000:5000 --name pbl-chart-api-debug pbl-chart-api-debug`
5. Start debugging with vs code using `.NET Core Docker Attach` to process with executed .dll path 