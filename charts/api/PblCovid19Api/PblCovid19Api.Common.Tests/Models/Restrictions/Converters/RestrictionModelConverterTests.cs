﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using FluentAssertions;
using NUnit.Framework;
using PblCovid19Api.Common.Models;
using PblCovid19Api.Common.Models.Restrictions.Converters;

namespace PblCovid19Api.Common.Tests.Models.Restrictions.Converters
{
    [TestFixture]
    internal class RestrictionModelConverterTests
    {
        [Test]
        public void RestrictionsToVector_WhenSomeRestrictionsProvided_ItShouldConvertItTo8ElementArray()
        {
            var restrictions = new[]
            {
                new RestrictionModel
                {
                    Name = "C1",
                    Value = 2
                },new RestrictionModel
                {
                    Name = "C3",
                    Value = 2
                },new RestrictionModel
                {
                    Name = "C8",
                    Value = 2
                },
            };

            var result = RestrictionModelConverter.RestrictionsToVector(restrictions);
            result.Should().HaveCount(8);
            result[0].Should().Be(2);
            result[2].Should().Be(2);
            result[7].Should().Be(2);
        }
    }
}
