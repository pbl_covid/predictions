﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using FluentAssertions;
using NUnit.Framework;
using PblCovid19Api.Common.Helpers;
using System;

namespace PblCovid19Api.Common.Tests
{
    [TestFixture]
    internal class DateTimeHelperTests
    {
        [Test]
        public void Test()
        {
            var ticks = 1608422400000;

            var result = DateTimeHelper.GetDateTimeFromUnixMiliseconds(ticks);

            result.Date.Should().Be(new DateTime(2020, 12, 20));
        }
    }
}
