﻿// Gliwice, Politechnika Slaska 2020/2021
// Paczuła Piotr, Żabiałowicz Marek

using FluentAssertions;
using NUnit.Framework;
using PblCovid19Api.Common.Helpers;

namespace PblCovid19Api.Common.Tests
{
    [TestFixture]
    internal class MathHelperTests
    {
        [Test]
        public void StandardDeviation()
        {
            var numbers = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var result = MathHelper.StandardDeviation(numbers);
            result.Should().BeApproximately(2.58, 0.1);
        }
    }
}
