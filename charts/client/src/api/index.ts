import Axios from "axios";

const origin = `${process.env.REACT_APP_API_URL ||
  "http://localhost:5000"}/api`;

export const urls = {
  chartData: `${origin}/charts/country`,
  countries: `${origin}/countries`,
  models: `${origin}/models`,
  restrictions: (countryCode: string) => `${origin}/restrictions/${countryCode}`
};
const TIMEOUT = process.env.NODE_ENV === "development" ? 120_000 : 240_000;

export const getAsync = async <T>(url: string): Promise<ResponseModel<T>> => {
  try {
    const { data } = await Axios.get<ResponseModel<T>>(url, {
      timeout: TIMEOUT
    });
    return data;
  } catch (err) {
    return {
      errors: [{ message: "Unknown error during request", stackTrace: "" }]
    };
  }
};

export const postAsync = async <T>(
  url: string,
  body: any
): Promise<ResponseModel<T>> => {
  try {
    const { data } = await Axios.post<ResponseModel<T>>(url, body, {
      timeout: TIMEOUT
    });
    return data;
  } catch (err) {
    return {
      errors: [{ message: "Unknown error during request", stackTrace: "" }]
    };
  }
};
