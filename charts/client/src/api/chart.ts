// eslint-disable-next-line no-unused-vars
import { Moment } from "moment";
import { getAsync, postAsync, urls } from ".";
import { formatDate, formatMoment } from "../utilities";

interface CountryApiDto {
  code: string;
  name: string;
  alpha3: string;
}

interface ChartDataEndpointRestriction {
  name: string;
  value: number;
  timeRanges: { dateFrom: string; dateTo: string }[];
}

interface ChartDataEndpointBody {
  chartDateFrom: string;
  simulationDateFrom: string;
  dateTo: string;
  model: string;
  countryCode: string;
  restrictions: ChartDataEndpointRestriction[];
}
export const prepareRestrictions = (
  restrictions: RestrictionType[]
): ChartDataEndpointRestriction[] => {
  const prepared: ChartDataEndpointRestriction[] = [];
  restrictions.forEach(r => {
    r.values.forEach(v => {
      prepared.push({
        name: v.code,
        value: Number(v.value),
        timeRanges: v.timeRanges.map(tr => ({
          dateFrom: formatDate(tr.dateFrom),
          dateTo: formatDate(tr.dateTo)
        }))
      });
    });
  });

  return prepared;
};

export const getChartDataAsync = async (
  countryCode: string,
  modelCode: string,
  startDate: Moment,
  endDate: Moment,
  simulationStartDate: Moment,
  restrictions: RestrictionType[]
  //   model: string,
): Promise<ResponseModel<ChartsDataset>> => {
  const parsedRestrictions = prepareRestrictions(restrictions);

  const body: ChartDataEndpointBody = {
    model: modelCode,
    chartDateFrom: formatMoment(startDate),
    dateTo: formatMoment(endDate),
    simulationDateFrom: formatMoment(simulationStartDate),
    countryCode,
    restrictions: parsedRestrictions
  };

  return postAsync<ChartsDataset>(urls.chartData, body);
};

export const getCountriesAsync = (): Promise<ResponseModel<
  CountryApiDto[]
>> => {
  return getAsync<CountryApiDto[]>(urls.countries);
};

export const getModelsAync = (): Promise<ResponseModel<string[]>> => {
  return getAsync<string[]>(urls.models);
};

export const getRestrictionsAsync = (
  countryCode: string
): Promise<ResponseModel<RestrictionsApiResponse>> => {
  return getAsync<RestrictionsApiResponse>(urls.restrictions(countryCode));
};
