import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("renders learn react link", () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/PolSl PBL Covid Design ©2020 using Ant/i);
  expect(linkElement).toBeInTheDocument();
});
