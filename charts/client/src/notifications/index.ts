import { notification } from "antd";

const useInfoCallback = (api: any) => (message: string) => {
  api.info({ message: "Info", description: message, placement: "bottomRight" });
};

const useSuccessCallback = (api: any) => (message: string) => {
  api.success({
    message: "Success",
    description: message,
    placement: "bottomRight"
  });
};

const useErrorCallback = (api: any) => (message: string) => {
  api.error({
    message: "Error",
    description: message,
    placement: "bottomRight"
  });
};

export const useNotifications = () => {
  const [api, notificationContext] = notification.useNotification();

  return {
    notificationContext,
    info: useInfoCallback(api),
    success: useSuccessCallback(api),
    error: useErrorCallback(api)
  };
};
