import React from "react";
import { BrowserRouter } from "react-router-dom";
import MainLayout from "./components/mainLayout";

import "./App.scss";
import "antd/dist/antd.min.css";

function App() {
  return (
    <BrowserRouter>
      <MainLayout />
    </BrowserRouter>
  );
}

export default App;
