export const enumValidator = (
  value: any,
  startValue: number,
  endValue: number
): ValidationResult<number> => {
  const errors: string[] = [];
  let result: number | undefined;
  const toFloat = Number.parseFloat(value);

  if (Number.isNaN(toFloat)) {
    errors.push("Given value is not a number");
  } else if (Number.isInteger(toFloat)) {
    if (toFloat >= startValue && toFloat <= endValue) {
      result = toFloat;
    } else {
      errors.push(
        `Given value is not valid. Allowed enums: ${startValue} - ${endValue}`
      );
    }
  } else {
    errors.push("Value must be an integer");
  }

  return {
    success: errors.length === 0,
    errors,
    value: result
  };
};

export const integerValidator = (
  value: any,
  min: number,
  max: number
): ValidationResult<number> => {
  const errors: string[] = [];
  let result: number | undefined;
  const toFloat = Number.parseFloat(value);

  if (Number.isNaN(toFloat)) {
    errors.push("Given value is not a number");
  } else if (Number.isInteger(toFloat)) {
    if (toFloat >= min && toFloat <= max) {
      result = toFloat;
    } else {
      errors.push(
        `Given value is not valid. Allowed integer between ${min} and ${max}`
      );
    }
  } else {
    errors.push("Value must be an integer");
  }

  return {
    success: errors.length === 0,
    errors,
    value: result
  };
};

export const floatValidator = (
  value: any,
  min: number,
  max: number
): ValidationResult<number> => {
  const errors: string[] = [];
  let result: number | undefined;
  const toFloat = Number.parseFloat(value);
  if (Number.isNaN(toFloat)) {
    errors.push("Given value is not a number");
  } else if (toFloat >= min && toFloat <= max) {
    result = toFloat;
  } else {
    errors.push(
      `Given value is not valid. Allowed floats between ${min} and ${max}`
    );
  }

  return {
    success: errors.length === 0,
    errors,
    value: result
  };
};
