interface ChartsDataset {
  real: DailyData[];
  simulation: DailyData[];
  errors?: ErrorStats;
}

interface ErrorStats {
  meanSquareError: number;
  meanError: number;
  meanAbsoluteError: number;
}

interface DailyData {
  date: string;
  deaths?: number;
  recovered?: number;
  confirmed?: number;
  active?: number;
}

interface CRestrictionsReponse {
  schoolClosing: number; // C1
  workplaceClosing: number; // C2
  cancelPublicEvents: number; // C3
  restrictionsOnGatherings: number; // C4
  closePublicTransport: number; // C5
  stayAtHome: number; // C6
  internalMovement: number; // C7
  internationalTravelControls: number; // C8
}

interface HRestrictionsResponse {
  testingPolicy: number; // H2
}

interface ERestrictionsResponse {
  e1: number;
  e2: number;
}
//

interface RestrictionsApiResponse {
  closuresAndContainment: CRestrictionsReponse;
  healthSystem: HRestrictionsResponse;
  economicResponse: ERestrictionsResponse;
  miscellaneousRestrictions: any;
}
