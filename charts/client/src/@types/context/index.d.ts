interface RestrictionStore {
  validRestrictionTypes: string[];
  selectedRestrictions: RestrictionsTree;
}
