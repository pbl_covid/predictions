interface ValidationResult<T> {
  success: boolean;
  errors: string[];
  value?: T;
}
