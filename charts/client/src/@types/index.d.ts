interface RestrictionType {
  code: string;
  values: Restriction[];
}

interface Restriction {
  id: string;
  code: string;
  value: string;
  timeRanges: RestrictionTimeRage[];
}

interface RestrictionTimeRage {
  id: string;
  dateFrom: Moment;
  dateTo: Moment;
}

type RestrictionValueType = "enum" | "dollars";

interface RestrictionValueUIModel {
  code: string;
  displayName: string;
  validator: (value: string) => ValidationResult<number>;
  type: RestrictionValueType;
}

// Api models

interface ErrorModel {
  message: string;
  data?: any;
  stackTrace: string;
}

interface ResponseModel<T> {
  data?: T;
  errors: ErrorModel[];
}
