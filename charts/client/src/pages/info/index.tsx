import { Typography } from "antd";
import React from "react";
import { Container } from "./style";

const { Title, Paragraph, Text } = Typography;

const InfoPage = () => {
  return (
    <Container>
      <Typography>
        <Paragraph>
          Application was created as a part of Project Base Learning on Silesian
          University of Technology with purpose of{" "}
          <Text strong>
            Modeling and predicting the development of a viral epidemic on the
            example of the SARS-CoV-2 pandemic
          </Text>
        </Paragraph>
        <Paragraph>
          Many different approaches have been used to model coronavirus spread.
          One of them was to use compartmental models. One of the most popular
          of them is the SIR (Susceptible-Infected-Recovered) model that has 3
          compartments: S(t) as suspected individuals that are not infected, but
          could become infected, I(t) as infected individuals that have already
          been infected by the virus and can transmit it to those individuals
          who are susceptible, R(t) as recovered individuals that are those
          individuals who have recovered from the virus and are assumed to be
          immune or have died (Rm(t) and D(t)). The model works with assumption
          which says we can neglect births and deaths other than deaths caused
          by the virus since the time scale of the SIR model is short enough[1].
          The goal of our project is to check different approaches for
          SARS-CoV-2 modelling containing various sets of predictors in order to
          evaluate their performances in predicting number of coronavirus cases
          for different countries in specified forecast period and create
          model-driven application enabling users to easily make predictions for
          set by them simulation parameters and visualize them.
        </Paragraph>
      </Typography>
    </Container>
  );
};

export default InfoPage;
