import React from "react";
import { Layout, Menu, Breadcrumb } from "antd";
import { Redirect, Route, Switch, Link } from "react-router-dom";
import "./style.scss";
import InfoPage from "../../pages/info";
import ChartWithRestrictions from "../chartWithRestrictions";

const { Header, Content, Footer } = Layout;

const MainLayout = () => {
  return (
    <Layout>
      <Header style={{ width: "100%" }}>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1">
            <Link to="/home">Predictions</Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link to="/info">Info</Link>
          </Menu.Item>
        </Menu>
      </Header>
      <Content
        className="site-layout"
        style={{ padding: "0 50px", marginTop: 64 }}
      >
        <div
          className="site-layout-background"
          style={{
            padding: 24,
            minHeight: "min-content",
            minWidth: "min-content"
          }}
        >
          <Switch>
            <Route exact path="/predictions">
              <ChartWithRestrictions />
            </Route>
            <Route exact path="/info">
              <InfoPage />
            </Route>
            <Redirect to="/predictions" />
          </Switch>
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>
        PolSl PBL Covid Design ©2020 using Ant
      </Footer>
    </Layout>
  );
};

export default MainLayout;
