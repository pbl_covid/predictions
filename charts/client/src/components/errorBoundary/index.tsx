import React, { Component } from "react";
import { Empty } from "antd";
import Style from "./style";

interface State {
  hasError: boolean;
  error?: any;
}
interface Props {
  children: JSX.Element | JSX.Element[];
}

export default class ErrorBoundary extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error: any) {
    // Zaktualizuj stan, aby następny render pokazał zastępcze UI.
    return { hasError: true, error };
  }

  componentDidCatch(error: any, errorInfo: any) {
    // Możesz także zalogować błąd do zewnętrznego serwisu raportowania błędów
    console.log(error, errorInfo);
  }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;
    if (hasError) {
      // Możesz wyrenderować dowolny interfejs zastępczy.
      return (
        <Style.Container>
          <Empty
            style={{ flex: "1", display: "flex", flexDirection: "column" }}
            description="Something went wrong."
          />
        </Style.Container>
      );
    }

    return children;
  }
}
