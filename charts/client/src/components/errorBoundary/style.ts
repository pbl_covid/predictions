import styled from "styled-components";

const Container = styled.div`
  flex: 1;
  width: 100%;
  height: 100%;
  position: relative;
  .ant-empty {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
  }
`;

export default { Container };
