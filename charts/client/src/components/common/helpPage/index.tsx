/* eslint-disable react/no-danger */
/* eslint-disable react/jsx-wrap-multilines */
import { QuestionCircleOutlined } from "@ant-design/icons";
import { Button, Tooltip } from "antd";
import Modal from "antd/lib/modal/Modal";
import React, { useState } from "react";

interface Props {
  modalTitle: string;
  modalDescription: string;
  tooltip: string;
}
const HelpPageButton = ({ modalTitle, modalDescription, tooltip }: Props) => {
  const [isVisible, setIsVisible] = useState(false);
  return (
    <>
      <Tooltip placement="topLeft" title={tooltip}>
        <Button
          onClick={() => setIsVisible(true)}
          type="link"
          icon={<QuestionCircleOutlined />}
        />
      </Tooltip>
      <Modal
        title={modalTitle}
        visible={isVisible}
        okText=""
        onCancel={() => setIsVisible(false)}
        footer={
          <Button type="primary" onClick={() => setIsVisible(false)}>
            Close
          </Button>
        }
      >
        <div dangerouslySetInnerHTML={{ __html: modalDescription }} />
      </Modal>
    </>
  );
};

export default HelpPageButton;
