/* eslint-disable no-unused-vars */
import { Button } from "antd";
import { ButtonType } from "antd/lib/button";
import { SizeType } from "antd/lib/config-provider/SizeContext";
import Modal from "antd/lib/modal/Modal";
import React, { useState } from "react";

interface Props {
  modalTitle: string;
  onOk: () => void;
  onCancel: () => void;
  children: JSX.Element;
  button: { type: ButtonType; size: SizeType; text: string; danger?: boolean };
}

const ModalButton = ({
  modalTitle,
  onOk,
  onCancel,
  children,
  button
}: Props) => {
  const { danger, size, text, type } = button;

  const [visible, setVisible] = useState(false);

  const handleOk = () => {
    onOk();
    setVisible(false);
  };

  const handleCancel = () => {
    onCancel();
    setVisible(false);
  };

  return (
    <>
      <Button
        onClick={() => setVisible(true)}
        type={type}
        size={size}
        danger={!!danger}
      >
        {text}
      </Button>
      <Modal
        title={modalTitle}
        visible={visible}
        onCancel={handleCancel}
        onOk={handleOk}
      >
        {children}
      </Modal>
    </>
  );
};

export default ModalButton;
