import React, { useEffect, useState } from "react";
import { Select } from "antd";
import Style from "./style";

interface Props {
  minWidth: string;
  options: { value: string | number; displayName: string }[];
  onChange: (value: string | number) => void;
}

const SelectInput = ({ minWidth, options, onChange }: Props) => {
  const defaultValue = options.length > 0 ? options[0].value : undefined;

  const [value, setValue] = useState(defaultValue);
  useEffect(() => {
    if (defaultValue) {
      onChange(defaultValue);
    }
  }, []);

  useEffect(() => {
    setValue(defaultValue);
  }, [options]);

  const onValueChange = (v: string | number) => {
    setValue(v);
    onChange(v);
  };

  return (
    <Style.SelectContainer minWidth={minWidth}>
      <Select
        value={value}
        defaultValue={defaultValue}
        onChange={onValueChange}
      >
        {options.map(o => (
          <Select.Option key={o.value} value={o.value}>
            {o.displayName}
          </Select.Option>
        ))}
      </Select>
    </Style.SelectContainer>
  );
};

export default React.memo(SelectInput);
