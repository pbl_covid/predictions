import styled from "styled-components";

const SelectContainer = styled.div<{ minWidth: string }>`
  .ant-select {
    width: ${props => props.minWidth};
  }
`;

const DollarContainer = styled.div<{ minWidth: string }>`
  .ant-input-number {
    width: ${props => props.minWidth};
  }
`;

export default { SelectContainer, DollarContainer };
