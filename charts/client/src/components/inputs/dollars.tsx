import { InputNumber } from "antd";
import React, { useEffect, useState } from "react";
import Style from "./style";
import { MAX_DOLLARS } from "../../context/restrictions";

interface Props {
  onChange: (dollars: number) => void;
  minWidth: string;
}

const DollarsInput = ({ onChange, minWidth }: Props) => {
  const [value, setValue] = useState<number>();
  const onValueChange = (v: number | string | undefined) => {
    setValue(Number(v || 0));
  };

  useEffect(() => {
    onChange(value || 0);
  }, [value]);

  return (
    <Style.DollarContainer minWidth={minWidth}>
      <InputNumber
        min={0}
        max={MAX_DOLLARS}
        step={1}
        formatter={v => `${v}$`}
        onChange={onValueChange as any}
        value={value}
      />
    </Style.DollarContainer>
  );
};

export default DollarsInput;
