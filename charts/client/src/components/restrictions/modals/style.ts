import styled from "styled-components";

const SelectInputGroup = styled.div`
  .ant-input-group {
    display: flex;
    justify-content: center;

    .ant-input,
    .ant-select {
      min-width: 200px;
    }
  }
`;

const TimeRangeGroup = styled.div``;

export default {
  SelectInputGroup,
  TimeRangeGroup
};
