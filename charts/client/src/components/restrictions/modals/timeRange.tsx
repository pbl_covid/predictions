import { Modal, DatePicker, Form } from "antd";
// eslint-disable-next-line no-unused-vars
import moment, { Moment } from "moment";
import React, { useEffect, useState } from "react";
import Style from "./style";

const { RangePicker } = DatePicker;
interface Props {
  dateValidator: (moment: Moment) => boolean;
  visible: boolean;
  setVisible: (visible: boolean) => void;
  onAdd: (from: Moment, to: Moment) => void;
  modalTitle: string;
  okText: string;
  contentText: string;
  defaultValue?: [Moment, Moment];
}

const TimeRangeModal = ({
  modalTitle,
  okText,
  contentText,
  dateValidator,
  visible,
  setVisible,
  onAdd,
  defaultValue
}: Props) => {
  const [value, setValue] = useState<[Moment, Moment] | null>();

  useEffect(() => {
    if (defaultValue) {
      setValue([moment(defaultValue[0]), moment(defaultValue[1])]);
    }
  }, [defaultValue]);

  const resetValues = () => {
    setValue(null);
  };

  const onOk = () => {
    if (value) {
      onAdd(value[0], value[1]);
    } else {
      // todo: error notification
    }
    resetValues();
  };

  const onCancel = () => {
    setVisible(false);
    resetValues();
  };

  const onValueChange = (any: any) => {
    setValue(any);
  };

  return (
    <Modal
      title={modalTitle}
      okText={okText}
      visible={visible}
      onOk={onOk}
      onCancel={onCancel}
    >
      <Style.TimeRangeGroup>
        <Form layout="vertical" size="small">
          <Form.Item label={contentText} name="dataRange">
            <RangePicker
              disabledDate={dateValidator}
              value={value}
              defaultValue={defaultValue}
              onChange={onValueChange}
            />
          </Form.Item>
        </Form>
      </Style.TimeRangeGroup>
    </Modal>
  );
};

TimeRangeModal.defaultProps = {
  defaultValue: null
};

export default TimeRangeModal;
