import { Input, Modal, Select } from "antd";
import React, { useState } from "react";
import { useNotifications } from "../../../notifications";
import Style from "./style";
import DollarsInput from "../../inputs/dollars";
import SelectInput from "../../inputs/select";
import { getValue, getEnumsForValue } from "../../../context/restrictions";

interface RestrictionValidatorDict {
  [key: string]: (value: string) => boolean;
}

interface Props {
  availableRestrictions: RestrictionValueUIModel[];
  visible: boolean;
  setVisible: (visible: boolean) => void;
  onAdd: (restriction: string, value: string) => void;
}

const AddValueModal = ({
  availableRestrictions,
  visible,
  setVisible,
  onAdd
}: Props) => {
  const [restriction, setRestriction] = useState("");
  const [value, setValue] = useState("");
  const { error } = useNotifications();
  const resetValues = () => {
    setRestriction("");
    setValue("");
  };

  const onOk = () => {
    const foundRestr = availableRestrictions.find(r => r.code === restriction);
    if (foundRestr) {
      const { errors, success } = foundRestr.validator(value);
      if (success) {
        onAdd(restriction, value);
      } else {
        error(errors.join(" "));
      }
    } else {
      error("Restriction value type is invalid");
    }
    resetValues();
  };

  const onCancel = () => {
    setVisible(false);
    resetValues();
  };
  const onValueChange = (v: string | number) => {
    setValue(v.toString());
  };
  const selectOptions = availableRestrictions.map(r => ({
    code: r.code,
    displayName: r.displayName
  }));

  const RenderInput = () => {
    const type = getValue(restriction)?.type;

    switch (type) {
      case "dollars":
        return <DollarsInput minWidth="150px" onChange={onValueChange} />;
      case "enum":
        const opts = getEnumsForValue(restriction);
        return (
          <SelectInput
            onChange={onValueChange}
            options={opts!}
            minWidth="300px"
          />
        );
      default:
        return null;
    }
  };

  return (
    <Modal
      visible={visible}
      title="Add new restriction value"
      okText="Add"
      onOk={onOk}
      onCancel={onCancel}
    >
      <Style.SelectInputGroup>
        <Input.Group compact>
          <Select value={restriction} onChange={setRestriction}>
            {selectOptions.map(v => (
              <Select.Option key={v.code} value={v.code}>
                {v.displayName}
              </Select.Option>
            ))}
          </Select>
          {RenderInput()}
        </Input.Group>
      </Style.SelectInputGroup>
    </Modal>
  );
};

export default AddValueModal;
