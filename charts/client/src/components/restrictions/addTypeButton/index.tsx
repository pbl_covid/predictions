/* eslint-disable no-restricted-syntax */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable no-unused-vars */
import { Modal, Button, TreeSelect } from "antd";
import React, { useState } from "react";

const { TreeNode } = TreeSelect;

interface Props {
  onRestrictionsChanged?: (addedRestrictions: any) => void;
  restrictionsToAdd?: any;
}

const AddNewRestrictionTypeButton = ({
  onRestrictionsChanged,
  restrictionsToAdd
}: Props) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [restrictionTree, setRestrictionTree] = useState();
  const openModal = () => setModalVisible(true);
  const onCancel = () => {
    setModalVisible(false);
  };
  const onAdd = () => {
    setModalVisible(false);
  };

  const onTreeValueChange = (values: string[]) => {
    const added: any = { C: [], E: [], H: [] };
    for (const v of values) {
      switch (v) {
        case "C":
          added.C.push(v);
          break;
        case "H":
          added.H.push(v);
          break;
        case "E":
          added.E.push(v);
          break;
        default:
          break;
      }
    }
    onRestrictionsChanged!(added);
  };

  const renderRestrictionType = (parentName: string, restrs: string[]) => {
    return (
      restrs.length !== 0 && (
        <TreeNode selectable={false} value={parentName} title={parentName}>
          {restrs.map(r => (
            <TreeNode value={r} title={r} />
          ))}
        </TreeNode>
      )
    );
  };
  return (
    <>
      <Button type="primary" size="small" onClick={openModal}>
        Add restriction
      </Button>
      <Modal
        okText="Add"
        title="Add new restriction"
        visible={modalVisible}
        onOk={onAdd}
        onCancel={onCancel}
      >
        <TreeSelect
          showSearch
          style={{ width: "100%" }}
          value={restrictionTree}
          dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
          placeholder="Please select"
          allowClear
          multiple
          treeDefaultExpandAll
          onChange={onTreeValueChange}
        >
          {renderRestrictionType("C Restrictions", restrictionsToAdd!.C)}
          {renderRestrictionType("E Restrictions", restrictionsToAdd!.E)}
          {renderRestrictionType("H Restrictions", restrictionsToAdd!.H)}
        </TreeSelect>
      </Modal>
    </>
  );
};

AddNewRestrictionTypeButton.defaultProps = {
  onRestrictionsChanged: () => {},
  restrictionsToAdd: {}
};

export default AddNewRestrictionTypeButton;
