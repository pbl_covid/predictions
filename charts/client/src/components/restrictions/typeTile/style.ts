import styled from "styled-components";

const Options = styled.div`
  align-items: center;
  display: flex;
  button {
    margin: 2px;
  }
`;
const Title = styled.div`
  display: flex;
`;
const Header = styled.header`
  display: flex;
  justify-content: space-between;
`;

const Box = styled.div`
  box-shadow: 2px 2px 5px #00000080;
  padding: 10px;
  margin: 10px 5px;
`;

export default {
  Options,
  Title,
  Header,
  Box
};
