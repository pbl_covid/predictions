/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-wrap-multilines */
import React, { useState } from "react";
import { Button, Modal, Typography } from "antd";
import { observer } from "mobx-react";
import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import {
  getTypeDisplayName,
  SupportedRestrictions
} from "../../../context/restrictions";
import { useStores } from "../../../context/provider";
import HelpPageButton from "../../common/helpPage";
import ValueTile from "../valueTile";
import AddValueModal from "../modals/addValue";
import Style from "./style";
import { RestrictionTypesHelp } from "../../../utilities/helpPages";

const { Text } = Typography;
interface Props {
  restriction: RestrictionType;
}

const RestrictionTypeTile = ({ restriction }: Props) => {
  const { code, values: restrChildren } = restriction;
  const { restrictionStore } = useStores();
  const [addModalVisible, setAddModalVisible] = useState(false);
  const [deleteModalVisible, setDeleteModalVisible] = useState(false);

  const helpDetails = RestrictionTypesHelp.find(h => h.code === code)?.help;

  const onAddHandle = (restr: string, value: string) => {
    restrictionStore.addRestrictionValue(code, restr, value);
    setAddModalVisible(false);
  };

  const onDeleteHandle = () => {
    setDeleteModalVisible(false);
    restrictionStore.removeRestrictionType(code);
  };

  const onDeleteCancel = () => {
    setDeleteModalVisible(false);
  };

  const restrictionValuesToAdd = () => {
    const addedValueCodes = restrChildren.map(v => v.code);

    return (
      SupportedRestrictions.find(x => x.code === code)?.values || []
    ).filter(v => !addedValueCodes.includes(v.code));
  };

  const displayName = getTypeDisplayName(code);

  const addRestritionValueModal = (
    <AddValueModal
      visible={addModalVisible}
      setVisible={setAddModalVisible}
      availableRestrictions={restrictionValuesToAdd()}
      onAdd={onAddHandle}
    />
  );
  const deleteRestrictionTypeModal = (
    <Modal
      title="Delete restriction type"
      okText="Delete"
      visible={deleteModalVisible}
      onOk={onDeleteHandle}
      onCancel={onDeleteCancel}
    >
      <Text>Do you want to delete </Text>
      <Text code>{displayName}</Text>
      <Text> ?</Text>
    </Modal>
  );

  return (
    <>
      <Style.Box>
        <Style.Header>
          <Style.Title>
            <h1>{displayName}</h1>
          </Style.Title>
          <Style.Options>
            <HelpPageButton
              modalTitle={helpDetails?.title || ""}
              modalDescription={helpDetails?.description || ""}
              tooltip={helpDetails?.title || ""}
            />
            <Button
              icon={<PlusOutlined />}
              size="small"
              type="primary"
              onClick={() => setAddModalVisible(true)}
            >
              Add
            </Button>
            <Button
              icon={<DeleteOutlined />}
              size="small"
              type="primary"
              danger
              onClick={() => setDeleteModalVisible(true)}
            >
              Delete
            </Button>
          </Style.Options>
        </Style.Header>
        {restrChildren.map(r => (
          <ValueTile key={r.id} {...r} />
        ))}
      </Style.Box>
      {/* Modals */}
      {addRestritionValueModal}
      {deleteRestrictionTypeModal}
    </>
  );
};

export default observer(RestrictionTypeTile);
