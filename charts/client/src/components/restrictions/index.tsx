/* eslint-disable react/jsx-wrap-multilines */
import React, { useState } from "react";
import { Button, Divider, Select, Switch, Typography } from "antd";
import { observer } from "mobx-react";
import { PlusOutlined } from "@ant-design/icons";
import { useStores } from "../../context/provider";
import HelpPageButton from "../common/helpPage";
import RestrictionTypeTile from "./typeTile";
import Style from "./style";
import { AddNewRestrictionTypeHelp } from "../../utilities/helpPages";

const { Text } = Typography;

const RestrictionsTile = () => {
  const rootStore = useStores();
  const { restrictionStore } = rootStore;
  const {
    ignoreCountryDefaulRestrictions,
    setIgnoreRestrictions,
    emptyRestrictionTypes,
    restrictions,
    addRestrictionType
  } = restrictionStore;

  const [formType, setFormType] = useState<string | undefined>();
  const handleAddType = () => {
    if (formType != null) {
      addRestrictionType(formType);
      setFormType(undefined);
    }
  };

  const canAddRestr = emptyRestrictionTypes.length > 0;

  return (
    <Style.Container>
      <Style.IgnoreRestrictionsContainer>
        <Text strong>Do not update restrictions when country is changed</Text>
        <Switch
          checked={ignoreCountryDefaulRestrictions}
          onChange={() => {
            setIgnoreRestrictions(!ignoreCountryDefaulRestrictions);
          }}
        />
      </Style.IgnoreRestrictionsContainer>
      <Divider />
      {canAddRestr && (
        <Style.ControlPanel>
          <Text strong>Add new restriction type</Text>
          <HelpPageButton
            modalTitle={AddNewRestrictionTypeHelp.title}
            modalDescription={AddNewRestrictionTypeHelp.description}
            tooltip={AddNewRestrictionTypeHelp.title}
          />
          <Select value={formType} onChange={setFormType}>
            {emptyRestrictionTypes.map(r => (
              <Select.Option key={r.code} value={r.code}>
                {r.displayName}
              </Select.Option>
            ))}
          </Select>
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={handleAddType}
          >
            Add
          </Button>
        </Style.ControlPanel>
      )}
      {restrictions.map(r => (
        <RestrictionTypeTile key={r.code} restriction={r} />
      ))}
    </Style.Container>
  );
};

export default observer(RestrictionsTile);
