/* eslint-disable react/jsx-wrap-multilines */
import React, { useState } from "react";
import { DeleteOutlined, FormOutlined, MoreOutlined } from "@ant-design/icons";
import { Button, Popover, Modal, Typography } from "antd";
// eslint-disable-next-line no-unused-vars
import { Moment } from "moment";

import { formatMoment } from "../../../utilities";
import TimeRangeModal from "../modals/timeRange";
import Style from "./style";

const { Text } = Typography;
interface Props {
  dateFrom: Moment;
  dateTo: Moment;
  validateAvailableDate: (date: Moment) => boolean;
  editOccurence: (dateFrom: Moment, dateTo: Moment) => void;
  deleteOccurence: () => void;
}

const DateRangeTile = ({
  dateFrom,
  dateTo,
  validateAvailableDate,
  editOccurence,
  deleteOccurence
}: Props) => {
  const [visibleOptionsPopover, setVisibleOptionsPopover] = useState(false);
  const [editModalVisible, setEditModalVisible] = useState(false);
  const [deleteModalVisible, setDeleteModalVisible] = useState(false);
  const deleteHandle = () => {
    setVisibleOptionsPopover(false);
    setDeleteModalVisible(true);
  };
  const editHandle = () => {
    setVisibleOptionsPopover(false);
    setEditModalVisible(true);
  };

  const confirmDelete = () => {
    deleteOccurence();
    setDeleteModalVisible(false);
  };
  const confirmEdit = (from: Moment, to: Moment) => {
    editOccurence(from, to);
    setEditModalVisible(false);
  };
  const dateFormat = `${formatMoment(dateFrom)} - ${formatMoment(dateTo)}`;

  const editModal = (
    <TimeRangeModal
      defaultValue={[dateFrom, dateTo]}
      visible={editModalVisible}
      setVisible={setEditModalVisible}
      okText="Edit"
      contentText="Edit date range:"
      modalTitle="Edit restriction date range"
      dateValidator={validateAvailableDate}
      onAdd={confirmEdit}
    />
  );
  const deleteModal = (
    <Modal
      visible={deleteModalVisible}
      title="Remove restriction occurence"
      onOk={confirmDelete}
      okText="Confirm"
      onCancel={() => setDeleteModalVisible(false)}
    >
      <Text>Do you want to delete this occurence?</Text>
      <Text code>{dateFormat}</Text>
    </Modal>
  );
  return (
    <>
      <Style.DateTile>
        <span>{dateFormat}</span>
        <Popover
          content={
            <Style.PopoverOptions>
              <Button
                icon={<FormOutlined />}
                size="small"
                type="primary"
                onClick={editHandle}
              >
                Edit
              </Button>
              <Button
                icon={<DeleteOutlined />}
                size="small"
                danger
                type="primary"
                onClick={deleteHandle}
              >
                Delete
              </Button>
            </Style.PopoverOptions>
          }
          trigger="click"
          visible={visibleOptionsPopover}
          onVisibleChange={setVisibleOptionsPopover}
        >
          <Button size="small" type="text" icon={<MoreOutlined />} />
        </Popover>
      </Style.DateTile>
      {/* Modals */}
      {editModal}
      {deleteModal}
    </>
  );
};

export default DateRangeTile;
