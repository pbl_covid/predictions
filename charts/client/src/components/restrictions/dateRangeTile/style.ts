import styled from "styled-components";

const DateTile = styled.div`
  display: inline-block;
  margin: 10px;
  border: 2px solid #00000080;
  border-radius: 1px;
  & > span {
    padding: 10px;
  }
`;

const PopoverOptions = styled.div`
  display: flex;
  min-width: 150px;
  & > button {
    margin: 0 3px;
    flex: 1;
  }
`;

export default {
  PopoverOptions,
  DateTile
};
