import styled from "styled-components";

const Options = styled.div`
  align-items: center;
  display: flex;
  button {
    margin: 2px;
  }
`;

const Container = styled.div`
  box-shadow: 2px 2px 5px #00000080;
  margin: 10px;

  .ant-card-head-title {
    text-align: left;
  }

  .ant-card-body {
    padding: 0;
  }
`;

export default {
  Options,
  Container
};
