/* eslint-disable react/jsx-wrap-multilines */
import { Button, Card, Typography } from "antd";
import Modal from "antd/lib/modal/Modal";
import { observer } from "mobx-react";
import React, { useState } from "react";
// eslint-disable-next-line no-unused-vars
import { Moment } from "moment";
import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import { getEnumDisplayName, getValue } from "../../../context/restrictions";
import { useStores } from "../../../context/provider";
import HelpPageButton from "../../common/helpPage";
import DateRangeTile from "../dateRangeTile";
import TimeRangeModal from "../modals/timeRange";
import Style from "./style";
import { RestrictionValuesHelp } from "../../../utilities/helpPages";

const { Text } = Typography;

type Props = Restriction;

const RestrictionTile = ({ code, value, timeRanges, id }: Props) => {
  const [deleteModalVisible, setDeleteModalVisible] = useState(false);
  const { restrictionStore } = useStores();
  const onDeleteHandle = () => {
    setDeleteModalVisible(false);
    restrictionStore.removeRestrictionValue(id);
  };
  const { displayName, type } = getValue(code)!;

  const helpDetails = RestrictionValuesHelp.find(h => h.code === code)?.help;

  const onDeleteCancel = () => {
    setDeleteModalVisible(false);
  };

  const [addModalVisible, setAddModalVisible] = useState(false);

  const onAddHandle = (from: Moment, to: Moment) => {
    restrictionStore.addTimeRangeToRestriciton(id, from, to);
    setAddModalVisible(false);
  };

  const editOccurence = (timeRangeId: string) => (from: Moment, to: Moment) => {
    restrictionStore.editTimeRangeOnRestriciton(timeRangeId, from, to);
  };

  const deleteOccurence = (timeRangeId: string) => () => {
    restrictionStore.removeTimeRangeFromRestriciton(timeRangeId);
  };

  const restrictionFormat = (t: RestrictionValueType) => {
    switch (t) {
      case "dollars":
        return `${displayName}: $${value}`;
      case "enum":
        return `${displayName}: ${getEnumDisplayName(code, value)}`;
      default:
        return "N/A";
    }
  };

  const deleteRestrictionValueModal = (
    <Modal
      title="Delete restriction value"
      okText="Delete"
      visible={deleteModalVisible}
      onOk={onDeleteHandle}
      onCancel={onDeleteCancel}
    >
      <Text>Do you want to delete </Text>
      <Text code>{restrictionFormat(type)}</Text>
      <Text> ?</Text>
    </Modal>
  );

  const disabledDates = (current: Moment) => {
    // eslint-disable-next-line no-restricted-syntax
    for (const tr of timeRanges) {
      const curr = current.startOf("day");
      const from = tr.dateFrom.startOf("day");
      const to = tr.dateTo.startOf("day");
      if (curr >= from && curr <= to) {
        return true;
      }
    }
    return false;
  };

  const editDisabledDatesFactory = (occurenceId: string) => {
    const occurence = restrictionStore.getTimeOccurence(occurenceId);

    return (current: Moment) => {
      if (occurence) {
        const curr = current.startOf("day");
        const from = occurence.dateFrom.startOf("day");
        const to = occurence.dateTo.startOf("day");
        if (curr >= from && curr <= to) {
          return false;
        }
      }

      return disabledDates(current);
    };
  };

  const addRestrictionValueModal = addModalVisible && (
    <TimeRangeModal
      modalTitle="Add restriction value"
      okText="Add"
      contentText="Choose new date range:"
      visible={addModalVisible}
      setVisible={setAddModalVisible}
      onAdd={onAddHandle}
      dateValidator={disabledDates}
    />
  );

  return (
    <>
      <Style.Container>
        <Card
          title={restrictionFormat(type)}
          extra={
            <Style.Options>
              <HelpPageButton
                modalTitle={helpDetails?.title || ""}
                modalDescription={helpDetails?.description || ""}
                tooltip={helpDetails?.title || ""}
              />
              <Button
                icon={<PlusOutlined />}
                size="small"
                type="primary"
                onClick={() => setAddModalVisible(true)}
              >
                Add
              </Button>
              <Button
                icon={<DeleteOutlined />}
                size="small"
                type="primary"
                danger
                onClick={() => setDeleteModalVisible(true)}
              >
                Delete
              </Button>
            </Style.Options>
          }
        >
          {timeRanges.map(t => (
            <DateRangeTile
              key={t.id}
              editOccurence={editOccurence(t.id)}
              deleteOccurence={deleteOccurence(t.id)}
              validateAvailableDate={editDisabledDatesFactory(t.id)}
              dateFrom={t.dateFrom}
              dateTo={t.dateTo}
            />
          ))}
        </Card>
      </Style.Container>
      {/* Modals */}
      {deleteRestrictionValueModal}
      {addRestrictionValueModal}
    </>
  );
};

export default observer(RestrictionTile);
