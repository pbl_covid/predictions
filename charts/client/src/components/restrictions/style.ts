import styled from "styled-components";

const Container = styled.div`
  box-shadow: 2px 2px 5px #00000080;
  padding: 10px;
`;

const ControlPanel = styled.div`
  display: flex;
  justify-content: flex-end;
  & > * {
    margin: 5px;
  }

  .ant-typography {
    margin: auto 0;
  }
  .ant-select {
    min-width: 230px;
  }
`;

const IgnoreRestrictionsContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 10px;
  * {
    padding: 0 5px;
  }
`;

export default {
  Container,
  ControlPanel,
  IgnoreRestrictionsContainer
};
