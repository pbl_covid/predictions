import styled from "styled-components";

interface Props {}

export const Container = styled.div<Props>`
  padding: 20px;
  width: 100%;
  height: calc(100% - 60px);
  .ant-card-bordered {
    display: flex;
    flex-direction: column;
    height: 100%;

    .ant-card-body {
      width: 100%;
      height: 100%;
      padding: 10px;
      flex: 1;
      justify-content: center;
      align-items: center;
      display: flex;
    }
  }
`;

export const Empty = styled.div`
  width: 100%;
  height: 100%;
  position: relative;

  .ant-empty {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

export default { Container, Empty };
