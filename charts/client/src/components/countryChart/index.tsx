import React, { useEffect, useRef, useState } from "react";
// eslint-disable-next-line no-unused-vars
import Chart, { ChartConfiguration } from "chart.js";
// eslint-disable-next-line no-unused-vars
import { Moment } from "moment";
import { Empty } from "antd";
import { observer } from "mobx-react";
import Style from "./styles";
import LoadingSpinner from "../loadingSpinner";
import { toDataset } from "../../utilities/charts";
// eslint-disable-next-line no-unused-vars

interface ChartConfigurationExtended extends ChartConfiguration {
  verticalLines: { index: number; text: string; color: string }[];
}
const colors = {
  red: "rgb(255, 99, 132)",
  orange: "rgb(255, 159, 64)",
  yellow: "rgb(255, 205, 86)",
  green: "rgb(75, 192, 192)",
  violet: "rgb(135, 0, 139)",
  blue: "rgb(56, 1, 255)",
  black: "rgb(0, 0, 0)",
  brown: "rgb(80, 25, 0)"
};
const labels = {
  axes: {
    X: "Date",
    Y: "Amount"
  },
  datasets: {
    real: {
      recovered: "Recovered",
      active: "Active",
      deathts: "Deaths",
      confirmed: "Confirmed"
    },
    simulation: {
      recovered: "Simulated Recovered",
      active: "Simulated Active",
      deathts: "Simulated Deaths",
      confirmed: "Simulated Confirmed"
    }
  }
};

interface Props {
  data: ChartsDataset;
  guid: string;
  loading: boolean;
  noData: boolean;
}

const CountryChart = ({ guid, data, loading, noData }: Props) => {
  const canvasRef = useRef<HTMLCanvasElement>();
  const [chart, setChart] = useState<Chart>();

  const destroyChart = () => {
    chart?.destroy();
  };
  /**
   * @param data
   * return [simulationIndex, data]
   */
  const createChartData = (
    dataset: ChartsDataset
  ): [number, Chart.ChartData] => {
    const { dates, real, simulation, simulationIndex } = toDataset(dataset);

    return [
      simulationIndex,
      {
        labels: dates,
        datasets: [
          {
            label: labels.datasets.real.confirmed,
            data: real.confirmed,
            borderColor: colors.yellow,
            backgroundColor: colors.yellow,
            fill: false
          },
          {
            label: labels.datasets.real.deathts,
            data: real.deaths,
            borderColor: colors.red,
            backgroundColor: colors.red,
            fill: false
          },
          {
            label: labels.datasets.real.active,
            data: real.active,
            borderColor: colors.orange,
            backgroundColor: colors.orange,
            fill: false
          },
          {
            label: labels.datasets.real.recovered,
            data: real.recovered,
            borderColor: colors.green,
            backgroundColor: colors.green,
            fill: false
          },
          {
            label: labels.datasets.simulation.confirmed,
            data: simulation.confirmed,
            borderColor: colors.violet,
            backgroundColor: colors.violet,
            borderDash: [2, 10],
            fill: false
          },
          {
            label: labels.datasets.simulation.deathts,
            data: simulation.deaths,
            borderColor: colors.black,
            backgroundColor: colors.black,
            borderDash: [2, 10],
            fill: false
          },
          {
            label: labels.datasets.simulation.active,
            data: simulation.active,
            borderColor: colors.blue,
            backgroundColor: colors.blue,
            borderDash: [2, 10],
            fill: false
          },
          {
            label: labels.datasets.simulation.recovered,
            data: simulation.recovered,
            borderColor: colors.brown,
            backgroundColor: colors.brown,
            borderDash: [2, 10],
            fill: false
          }
        ]
      }
    ];
  };
  const loadChart = () => {
    if (canvasRef.current != null) {
      if (data.simulation.length === 0) {
        return;
      }

      destroyChart();
      const [simulationIndex, dataSets] = createChartData(data);
      const ctx = canvasRef.current.getContext("2d");
      const options: ChartConfigurationExtended = {
        verticalLines: [
          {
            index: simulationIndex,
            text: " Simulation date",
            color: "#235fa5"
          }
        ],
        type: "line",
        options: {
          legend: {
            position: "right"
          },
          elements: {
            point: {
              radius: 3
            }
          },
          scales: {
            yAxes: [
              { scaleLabel: { display: true, labelString: labels.axes.Y } }
            ],
            xAxes: [
              {
                scaleLabel: { display: true, labelString: labels.axes.X },
                ticks: {
                  autoSkip: true,
                  maxTicksLimit: 20
                }
              }
            ]
          }
        },
        data: dataSets
      };
      setChart(new Chart(ctx!, options as ChartConfiguration));
    }
  };

  useEffect(() => {
    loadChart();
  }, [guid]);

  useEffect(() => () => destroyChart(), []);

  return (
    <Style.Container>
      <canvas hidden={loading || noData} ref={canvasRef as any} />
      {!loading && noData && (
        <Style.Empty>
          <Empty />
        </Style.Empty>
      )}
      {loading && <LoadingSpinner />}
    </Style.Container>
  );
};

export default observer(CountryChart);
