import styled from "styled-components";

const HiddenRestrictions = styled.div`
  padding: 1rem;
  position: relative;
  height: calc(100vw / 2);
  width: calc(100vw - 150px);
  max-width: 1650px;
  max-height: 900px;
  min-width: 650px;
  min-height: 400px;
  overflow: hidden;
  text-align: center;
  background: #fafafa;
  border: 1px solid #ebedf0;
  border-radius: 2px;
  margin: 1rem;
`;

const Controls = styled.div`
  display: flex;
  padding-bottom: 1rem;

  .ant-row.ant-form-item {
    display: flex;
    flex-direction: column;
    align-items: start;
  }

  .ant-form.ant-form-vertical {
    display: flex;
    align-items: flex-end;
    justify-content: space-between;
    width: 100%;
    .controls {
      position: relative;
      display: flex;
      width: 100%;
      .ant-select,
      .ant-picker {
        width: 240px;
      }

      .ant-collapse {
        position: absolute;
        width: 300px;
        z-index: 10;
      }

      &__left {
        margin-right: 2rem;
      }
      &__buttons {
        margin-left: auto;
        display: flex;
        button {
          margin-left: 5px;
        }
      }
    }
  }
`;

const ErrorStats = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export default { HiddenRestrictions, ErrorStats, Controls };
