import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from "react";
import { Button, Collapse, DatePicker, Drawer, Form, Typography } from "antd";
import moment, { Moment } from "moment";
import { AppstoreAddOutlined, CheckOutlined } from "@ant-design/icons";
import { observer } from "mobx-react";
import { useNotifications } from "../../notifications/index";
import CountryChart from "../countryChart";
import Restrictions from "../restrictions";
import {
  CreateStoreContext,
  StoreProvider,
  useStores
} from "../../context/provider";
import ErrorBoundary from "../errorBoundary";
import Style from "./style";
import SelectInput from "../inputs/select";
import {
  getCountriesAsync,
  getModelsAync,
  getRestrictionsAsync
} from "../../api/chart";

const { RangePicker } = DatePicker;
const { Text } = Typography;

interface CountryUIModel {
  code: string;
  displayName: string;
}
interface PredictionModelUIModel {
  code: string;
  displayName: string;
}

const ctx = CreateStoreContext();

const ChartWithRestrictions = () => {
  const Ctx = useRef(ctx);
  const [loading, setLoading] = useState(false);
  const [noData, setNoData] = useState(true);
  const { chartStore, restrictionStore } = useStores();
  const [visibleRestrictions, setVisibleRestrictions] = useState(false);
  const [dateRange, setDateRange] = useState<[Moment, Moment] | undefined>();
  const [simulationStartDate, setSimulationStartDate] = useState<
    Moment | undefined
  >(undefined);
  // Error stats
  const { meanAbsoluteError, meanError, meanSquareError } = chartStore;

  // available countries
  const [countries, setCountries] = useState<CountryUIModel[]>([]);

  // available models
  const [models, setModels] = useState<PredictionModelUIModel[]>([]);

  // current countries
  const [country, setCountry] = useState<string>();

  // current models
  const [model, setModel] = useState<string>();

  // notificatations
  const { error, success, notificationContext } = useNotifications();

  // rangeTime input constraints
  const startDisabledTime = useCallback((current: Moment) => {
    const firstCovidDate = moment.utc("2020-01-26");
    if (current < firstCovidDate) {
      return true;
    }
    return false;
  }, []);

  const simulationStartDisabledTime = useCallback(
    (current: Moment) => {
      if (dateRange && dateRange[0] > current) {
        return true;
      }
      if (dateRange && dateRange[1] < current) {
        return true;
      }
      if (!dateRange) {
        return true;
      }
      return false;
    },
    [dateRange]
  );

  useEffect(() => {
    getModelsAync().then(response => {
      const { errors, data } = response;
      if (data == null || (errors != null && errors.length > 0)) {
        error("Failed fetching models");
      } else {
        setModels(data!.map(d => ({ code: d, displayName: d })));
        setModel(data[0]);
      }
    });

    getCountriesAsync().then(response => {
      const { errors, data } = response;
      if (data != null && errors != null && errors.length > 0) {
        error("Failed fetching countries");
      } else {
        setCountries(data!.map(d => ({ code: d.code, displayName: d.name })));
        setCountry(data![0].code);
      }
    });
  }, []);

  useEffect(() => {
    if (!restrictionStore.ignoreCountryDefaulRestrictions && country) {
      getRestrictionsAsync(country).then(response => {
        const { errors, data } = response;
        if (data == null || (errors != null && errors.length > 0)) {
          error("Failed fetching restrictions");
        } else {
          restrictionStore.setInitialRestrictions(data!);
        }
      });
    }
  }, [country]);

  const onDateRangeChange = (range: Moment[]) => {
    setDateRange(range as any);
  };

  const onSimulationStartDateChange = (date: Moment) => {
    setSimulationStartDate(date);
  };

  const applyIsDisabled =
    model == null ||
    country == null ||
    dateRange == null ||
    simulationStartDate == null ||
    loading;

  let startDate: Moment | null = null;
  let endDate: Moment | null = null;

  if (dateRange?.length === 2) {
    [startDate, endDate] = dateRange;
  }

  const onDataFetched = () => {
    setLoading(false);
    success("Data fetched succesfully");
  };

  const onDataFetchError = () => {
    setLoading(false);
    setNoData(true);
    error("Error while fetching data");
  };

  const fetchData = () => {
    if (
      !simulationStartDate ||
      simulationStartDate < dateRange![0] ||
      simulationStartDate > dateRange![1]
    ) {
      error("Simulation start date must be between chart start and end dates.");
      return;
    }

    setNoData(false);
    setLoading(true);
    chartStore.loadData(
      country!,
      model!,
      startDate!,
      endDate!,
      simulationStartDate!,
      restrictionStore.restrictions,
      onDataFetched,
      onDataFetchError
    );
  };

  const countryOpts = useMemo(
    () =>
      countries.map(c => ({
        value: c.code,
        displayName: c.displayName
      })),
    [countries]
  );

  const modelOpts = useMemo(
    () =>
      models.map(c => ({
        value: c.code,
        displayName: c.displayName
      })),
    [models]
  );

  return (
    <StoreProvider storeCtx={Ctx.current}>
      <Style.HiddenRestrictions>
        {notificationContext}
        <ErrorBoundary>
          <Style.Controls>
            <Form layout="vertical" size="small">
              <div className="controls">
                <Collapse>
                  <Collapse.Panel header="Main options" key="1">
                    <Form.Item label="Country" name="country">
                      <SelectInput
                        minWidth="200px"
                        onChange={setCountry as any}
                        options={countryOpts}
                      />
                    </Form.Item>
                    <Form.Item
                      label="Predictions Model"
                      name="predictions-model"
                    >
                      <SelectInput
                        minWidth="200px"
                        onChange={setModel as any}
                        options={modelOpts}
                      />
                    </Form.Item>
                    <Form.Item label="Data range" name="data-range">
                      <RangePicker
                        value={dateRange}
                        onChange={onDateRangeChange as any}
                        disabledDate={startDisabledTime}
                      />
                    </Form.Item>
                    <Form.Item label="Simulation start" name="simulationRange">
                      <DatePicker
                        placeholder="Select simulation start date"
                        value={simulationStartDate}
                        onChange={onSimulationStartDateChange as any}
                        disabledDate={simulationStartDisabledTime}
                        disabled={!dateRange}
                        allowClear
                      />
                    </Form.Item>
                  </Collapse.Panel>
                </Collapse>
                <div className="controls__buttons">
                  <Button
                    disabled={applyIsDisabled}
                    icon={<CheckOutlined />}
                    type="primary"
                    size="middle"
                    onClick={fetchData}
                  >
                    Apply changes
                  </Button>
                  <Button
                    icon={<AppstoreAddOutlined />}
                    type="primary"
                    size="middle"
                    onClick={() => setVisibleRestrictions(true)}
                  >
                    Restrictions
                  </Button>
                </div>
              </div>
            </Form>
          </Style.Controls>
          <ErrorBoundary>
            <CountryChart
              loading={loading}
              noData={noData}
              data={chartStore.getData}
              guid={chartStore.dataGuid}
            />
            <Style.ErrorStats>
              <Text strong>{`Mean Error: ${meanError}`}</Text>
              <Text strong>{`Mean Square Error: ${meanSquareError}`}</Text>
              <Text strong>{`Mean Absolute Error: ${meanAbsoluteError}`}</Text>
            </Style.ErrorStats>
          </ErrorBoundary>
          <Drawer
            title="Edit restrictions"
            placement="right"
            closable={false}
            onClose={() => setVisibleRestrictions(false)}
            visible={visibleRestrictions}
            getContainer={false}
            style={{ position: "absolute" }}
            width="90%"
          >
            <ErrorBoundary>
              <Restrictions />
            </ErrorBoundary>
          </Drawer>
        </ErrorBoundary>
      </Style.HiddenRestrictions>
    </StoreProvider>
  );
};

export default observer(ChartWithRestrictions);
