import React from "react";
import { Spin } from "antd";
import Style from "./style";

const defaultMessage = "Loading...";
interface Props {
  message?: string;
}

const LoadingSpinner = ({ message }: Props) => {
  return (
    <Style.Container>
      <Spin size="large" tip={message ?? defaultMessage} />
    </Style.Container>
  );
};

LoadingSpinner.defaultProps = {
  message: defaultMessage
};

export default LoadingSpinner;
