import dateFormat from "dateformat";
import { Moment } from "moment";

export const formatDate = (date: Date) => dateFormat(date, "yyyy-mm-dd");
export const formatMoment = (date: Moment) =>
  dateFormat(date.toDate(), "yyyy-mm-dd");
