import {
  SupportedRestrictionEnumValues,
  SupportedRestrictions
} from "../../context/restrictions";

interface HelpDetails {
  title: string;
  description: string;
}

interface HelpRestrictionTypes {
  code: string;
  help: HelpDetails;
}

interface HelpRestrictionValues {
  code: string;
  help: HelpDetails;
}

export const AddNewRestrictionTypeHelp: HelpDetails = {
  title: `Add new restriction type`,
  description: `
  <p>Available restriction types:</p> 
  <ol>
    <li>Containment and closure</li>
    <li>Health systems</li>
    <li>Economic response</li>
   </ol> `
};

const createAvailableValuesList = (restrictionType: string) => {
  const type = SupportedRestrictions.find(t => t.code === restrictionType);

  const options: string[] = (type?.values || []).map(e => e.displayName);

  return `<p>Available values:</p>
  <ol>
  ${options.map(o => `<li>${o}</li>`).join("")}
  </ol>`;
};

export const RestrictionTypesHelp = [
  {
    code: `C`,
    help: {
      title: `Containment and closure`,
      description: createAvailableValuesList("C")
    }
  },
  {
    code: `H`,
    help: {
      title: `Health systems`,
      description: createAvailableValuesList("H")
    }
  },
  {
    code: `E`,
    help: {
      title: `Economic response`,
      description: createAvailableValuesList("E")
    }
  }
];

const createAvailableEnumList = (restrictionValue: string) => {
  const val = SupportedRestrictionEnumValues.find(
    v => v.valueCode === restrictionValue
  );

  const options: string[] = (val?.enums || []).map(e => e.displayName);

  return `<p>Available values:</p>
  <ol>
  ${options.map(o => `<li>${o}</li>`).join("")}
  </ol>`;
};

export const RestrictionValuesHelp = [
  {
    code: `C1`,
    help: {
      title: `School closing`,
      description: `
      <p>Record closings of schools and universities</p>
      ${createAvailableEnumList("C5")}
      `
    }
  },
  {
    code: `C2`,
    help: {
      title: `Workplace closing`,
      description: `
      <p>Record closings of workplaces</p>
      ${createAvailableEnumList("C5")}
      `
    }
  },
  {
    code: `C3`,
    help: {
      title: `Cancel public events`,
      description: `
      <p>Record cancelling public events</p>
      ${createAvailableEnumList("C5")}
      `
    }
  },
  {
    code: `C4`,
    help: {
      title: `Restrictions on gathering size`,
      description: `
      <p>Record the cut-off size for bans on private gatherings</p>
      ${createAvailableEnumList("C5")}
      `
    }
  },
  {
    code: `C5`,
    help: {
      title: `Close public transport`,
      description: `
      <p>Record closing of public transport</p>
      ${createAvailableEnumList("C5")}
      `
    }
  },
  {
    code: `C6`,
    help: {
      title: `Stay at home requirements`,
      description: `
      <p>Record orders to “shelter-in-place” and otherwise confine to home.</p>
      ${createAvailableEnumList("C5")}
      `
    }
  },
  {
    code: `C7`,
    help: {
      title: `Restrictions on internal movement`,
      description: `
      <p>Record restrictions on internal movement</p>
      ${createAvailableEnumList("C5")}
      `
    }
  },
  {
    code: `C8`,
    help: {
      title: `Restrictions on international travel`,
      description: `
      <p>Record restrictions on international travel</p>
      ${createAvailableEnumList("C5")}
      `
    }
  },
  {
    code: `E1`,
    help: {
      title: `Income support`,
      description: `<p>Record if the government is covering the salaries or providing direct cash payments, universal basic income, or similar, of people who lose their jobs or cannot work. (Includes payments to firms if explicitly linked to payroll/ salaries)</p>
      ${createAvailableEnumList("E1")}`
    }
  },
  {
    code: `E2`,
    help: {
      title: `Debt / contract relief for households`,
      description: `<p>Record if govt. is freezing financial obligations (e.g. stopping loan repayments, preventing services like water from stopping, or banning evictions)</p>
      ${createAvailableEnumList("E2")}`
    }
  },
  {
    code: `H2`,
    help: {
      title: `Testing policy`,
      description: `<p>Who can get tested?</p>
    ${createAvailableEnumList("H2")}`
    }
  }
];
