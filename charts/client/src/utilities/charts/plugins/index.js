export const verticalLinePlugin = {
  getLinePosition(chart, pointIndex) {
    const meta = chart.getDatasetMeta(0); // first dataset is used to discover X coordinate of a point
    const { data } = meta;
    // eslint-disable-next-line no-underscore-dangle
    return data[pointIndex]._model.x;
  },
  renderVerticalLine(chartInstance, pointIndex, text, color) {
    const lineLeftOffset = this.getLinePosition(chartInstance, pointIndex);
    const scale = chartInstance.scales["y-axis-0"];
    const context = chartInstance.chart.ctx;

    // render vertical line
    context.beginPath();
    context.strokeStyle = color || "#ff0000";
    context.moveTo(lineLeftOffset, scale.top);
    context.lineTo(lineLeftOffset, scale.bottom);
    context.stroke();

    // write label
    context.fillStyle = color || "#ff0000";
    context.textAlign = "start";
    context.fillText(
      text,
      lineLeftOffset,
      (scale.bottom - scale.top) / 2 + scale.top
    );
  },

  // eslint-disable-next-line no-unused-vars
  afterDatasetsDraw(chart, easing) {
    if (chart.config.verticalLines) {
      // eslint-disable-next-line no-restricted-syntax
      for (const lines of chart.config.verticalLines) {
        const { index, text, color } = lines;
        this.renderVerticalLine(chart, index, text, color);
      }
    }
  }
};
