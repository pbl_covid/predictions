/* eslint-disable no-param-reassign */
import moment from "moment";
import { formatDate } from "..";

const distinctPredicate = (value: any, index: number, self: any[]) =>
  self.indexOf(value) === index;
const orderByDate = (a: string, b: string) => +new Date(a) - +new Date(b);

export const toDataset = (data: ChartsDataset) => {
  data.real.forEach(d => {
    d.date = moment(d.date).format("YYYY-MM-DD");
  });

  data.simulation.forEach(d => {
    d.date = moment(d.date).format("YYYY-MM-DD");
  });

  const dateLabels = data.real
    .map(d => d.date)
    .concat(data.simulation.map(s => s.date))
    .filter(distinctPredicate)
    .sort(orderByDate);

  const realDataset = dateLabels.map<DailyData>(l => ({
    date: l,
    deaths: data.real.find(d => d.date === l)?.deaths,
    active: data.real.find(d => d.date === l)?.active,
    confirmed: data.real.find(d => d.date === l)?.confirmed,
    recovered: data.real.find(d => d.date === l)?.recovered
  }));

  const simulationDataset = dateLabels.map<DailyData>(l => ({
    date: l,
    deaths: data.simulation.find(d => d.date === l)?.deaths,
    active: data.simulation.find(d => d.date === l)?.active,
    confirmed: data.simulation.find(d => d.date === l)?.confirmed,
    recovered: data.simulation.find(d => d.date === l)?.recovered
  }));
  const simulationStartIndex = simulationDataset.findIndex(
    s => s.deaths != null
  );
  return {
    dates: dateLabels.map(d => formatDate(new Date(d))),
    real: {
      deaths: realDataset.map(r => r.deaths),
      active: realDataset.map(r => r.active),
      confirmed: realDataset.map(r => r.confirmed),
      recovered: realDataset.map(r => r.recovered)
    },
    simulation: {
      deaths: simulationDataset.map(r => r.deaths),
      active: simulationDataset.map(r => r.active),
      confirmed: simulationDataset.map(r => r.confirmed),
      recovered: simulationDataset.map(r => r.recovered)
    },
    simulationIndex: simulationStartIndex
  };
};
