const Routing: Route = {
  path: "/",
  displayName: "",
  subUrls: {
    HOME: {
      displayName: "HOME",
      path: "home",
      subUrls: {
        LIST: {
          displayName: "LIST",
          path: "list"
        },
        DETAILS: {
          displayName: "DETAILS",
          path: "details"
        }
      }
    },
    INFO: {
      displayName: "INFO",
      path: "info"
    }
  }
};

interface RouteBase {
  path: string;
  displayName: string;
}

interface Route extends RouteBase {
  subUrls?: RouteChildren;
}

interface RouteChildren {
  [key: string]: Route;
}

interface BreadcrumbRoute {
  displayName: string;
  fullUrl: string;
}

export const getBreadcrumbRoute = (routeKey: string): BreadcrumbRoute[] => {
  const keys = routeKey.toUpperCase().split(":");
  const routeArray: BreadcrumbRoute[] = [];
  let tempRoute: Route = Routing;
  const previousUrls: string[] = [];

  for (const k of keys) {
    previousUrls.push(tempRoute.path);
    routeArray.push({
      displayName: tempRoute.displayName,
      fullUrl: [...previousUrls, tempRoute.path].join("/")
    });
    tempRoute = (tempRoute.subUrls as any)[k];
  }

  return routeArray;
};

export const getFullRouting = () => {};
