/* eslint-disable no-param-reassign */
/* eslint-disable max-classes-per-file */
import { observable, makeAutoObservable, runInAction } from "mobx";
// eslint-disable-next-line no-unused-vars
import { Moment } from "moment";
import { v4 as uuidv4 } from "uuid";
import { getChartDataAsync } from "../../api/chart";

export class ChartStore {
  dataGuid: string;

  meanSquareError: string;

  meanError: string;

  meanAbsoluteError: string;

  private data: ChartsDataset = {
    real: observable.array(),
    simulation: observable.array()
  };

  get guid() {
    return this.dataGuid;
  }

  constructor() {
    this.meanSquareError = "N/A";
    this.meanError = "N/A";
    this.meanAbsoluteError = "N/A";
    this.dataGuid = uuidv4();
    makeAutoObservable(this);
  }

  get getData() {
    return this.data;
  }

  loadData = async (
    countryCode: string,
    modelCode: string,
    startDate: Moment,
    endDate: Moment,
    simulationDate: Moment,
    restrictions: RestrictionType[],
    onSuccess?: () => void,
    onError?: () => void
  ) => {
    const { errors, data } = await getChartDataAsync(
      countryCode,
      modelCode,
      startDate,
      endDate,
      simulationDate,
      restrictions
    );
    if (errors != null && errors.length > 0) {
      onError?.call(this);
    } else {
      onSuccess?.call(this);
      runInAction(() => {
        const { meanAbsoluteError, meanError, meanSquareError } = data!.errors!;

        this.meanError = meanError?.toPrecision(4) || "N/A";
        this.meanSquareError = meanSquareError?.toPrecision(4) || "N/A";
        this.meanAbsoluteError = meanAbsoluteError?.toPrecision(4) || "N/A";

        this.dataGuid = uuidv4();
        this.data.real.splice(0, this.data.real.length);
        this.data.simulation.splice(0, this.data.simulation.length);
        this.data.real.push(...(data?.real || []));
        this.data.simulation.push(...(data?.simulation || []));
      });
    }
  };
}
