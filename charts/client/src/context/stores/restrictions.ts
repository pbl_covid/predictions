/* eslint-disable no-param-reassign */
/* eslint-disable max-classes-per-file */
import { observable, makeAutoObservable } from "mobx";
// eslint-disable-next-line no-unused-vars
import moment, { Moment } from "moment";
import { v4 as uuidv4 } from "uuid";
import { SupportedRestrictions } from "../restrictions";
// eslint-disable-next-line no-unused-vars

export class RestrictionStore {
  restrictions: RestrictionType[] = observable.array();

  ignoreCountryDefaulRestrictions: boolean = false;

  setIgnoreRestrictions = (value: boolean) => {
    this.ignoreCountryDefaulRestrictions = value;
  };

  addRestrictionValue = (
    typeCode: string,
    valueCode: string,
    value: string
  ) => {
    const filtered = this.restrictions.filter(r => r.code === typeCode);
    if (filtered.length === 1) {
      const chosenRestr = filtered[0];

      chosenRestr.values.push({
        id: uuidv4(),
        code: valueCode,
        value,
        timeRanges: []
      });
    }
  };

  removeRestrictionValue = (id: string) => {
    const types = this.restrictions;
    types.forEach(t => {
      // eslint-disable-next-line no-param-reassign
      const index = t.values.findIndex(r => r.id === id);
      if (index >= 0) {
        t.values.splice(index, 1);
      }
    });
  };

  editRestrictionValue = (id: string, value: string) => {
    const types = this.restrictions;
    types.forEach(t => {
      t.values
        .filter(r => r.id === id)
        .forEach(v => {
          v.value = value;
        });
    });
  };

  addTimeRangeToRestriciton = (id: string, from: Moment, to: Moment) => {
    const types = this.restrictions;
    types.forEach(t => {
      t.values
        .filter(r => r.id === id)
        .forEach(v => {
          v.timeRanges.push({ dateFrom: from, dateTo: to, id: uuidv4() });
        });
    });
  };

  removeTimeRangeFromRestriciton = (id: string) => {
    const types = this.restrictions;
    types.forEach(t => {
      t.values.forEach(v => {
        const index = v.timeRanges.findIndex(tr => tr.id === id);
        if (index >= 0) {
          v.timeRanges.splice(index, 1);
        }
      });
    });
  };

  editTimeRangeOnRestriciton = (id: string, from: Moment, to: Moment) => {
    const types = this.restrictions;
    types.forEach(t => {
      t.values.forEach(v => {
        const timeRange = v.timeRanges.filter(tr => tr.id === id);
        if (timeRange.length === 1) {
          timeRange[0].dateFrom = from;
          timeRange[0].dateTo = to;
        }
      });
    });
  };

  addRestrictionType = (typeCode: string) => {
    const { restrictions } = this;
    const restr = restrictions.filter(r => r.code === typeCode);
    if (restr.length === 0) {
      restrictions.push({ code: typeCode, values: [] });
    }
  };

  removeRestrictionType = (typeCode: string) => {
    const index = this.restrictions.findIndex(r => r.code === typeCode);
    if (index >= 0) {
      this.restrictions.splice(index, 1);
    }
  };

  getTimeOccurence = (occurenceId: string) => {
    const { restrictions } = this;
    return restrictions
      .flatMap(r => r.values.map(v => v.timeRanges).flat())
      .find(o => o.id === occurenceId);
  };

  get emptyRestrictionTypes() {
    const types = this.restrictions;
    return SupportedRestrictions.filter(
      v => !types.map(t => t.code).includes(v.code)
    );
  }

  clearRestrictions = () => {
    this.restrictions.splice(0, this.restrictions.length);
  };

  createInitialRestrictionValue = (restrKey: string, restrValue: string) => {
    let v = "";

    const { success, value } = SupportedRestrictions.flatMap(r => r.values)
      .find(r => r.code === restrKey)!
      .validator(restrValue);

    v = success ? value?.toString() || "0" : "0";

    const today = moment.utc();
    const yearFromNow = moment.utc().add(1, "years");

    return {
      id: uuidv4(),
      code: restrKey,
      value: v,
      timeRanges: [{ id: uuidv4(), dateFrom: today, dateTo: yearFromNow }]
    };
  };

  setInitialRestrictions = (restrictions: RestrictionsApiResponse) => {
    const {
      closuresAndContainment: {
        schoolClosing: C1,
        workplaceClosing: C2,
        cancelPublicEvents: C3,
        restrictionsOnGatherings: C4,
        closePublicTransport: C5,
        stayAtHome: C6,
        internalMovement: C7,
        internationalTravelControls: C8
      }
    } = restrictions;

    this.clearRestrictions();

    this.restrictions.push({
      code: "C",
      values: [
        this.createInitialRestrictionValue("C1", C1.toString()),
        this.createInitialRestrictionValue("C2", C2.toString()),
        this.createInitialRestrictionValue("C3", C3.toString()),
        this.createInitialRestrictionValue("C4", C4.toString()),
        this.createInitialRestrictionValue("C5", C5.toString()),
        this.createInitialRestrictionValue("C6", C6.toString()),
        this.createInitialRestrictionValue("C7", C7.toString()),
        this.createInitialRestrictionValue("C8", C8.toString())
      ]
    });
  };

  constructor() {
    makeAutoObservable(this);
  }
}
