import React, { useContext } from "react";
// eslint-disable-next-line no-unused-vars
import { useLocalObservable } from "mobx-react";
import { RootStore } from ".";

const STORE = new RootStore();
const initialLocal = () => {
  return STORE;
};

const initialCtx = () => {
  return STORE;
};

const storeContext = React.createContext<RootStore>(initialCtx());
export const CreateStoreContext = () => {
  return storeContext;
};

interface Props {
  children: JSX.Element;
  storeCtx: React.Context<RootStore>;
}

export const StoreProvider = ({ children, storeCtx }: Props): JSX.Element => {
  const store = useLocalObservable(() => initialLocal());

  return <storeCtx.Provider value={store}>{children}</storeCtx.Provider>;
};

export const useStores = (): RootStore => {
  return useContext(storeContext);
};
