import { enumValidator, floatValidator } from "../validation/restrictions";

interface RestrictionTypeUIModel {
  code: string;
  displayName: string;
  values: RestrictionValueUIModel[];
}

interface RestrictionValueEnumUIModel {
  valueCode: string;
  enums: {
    value: string;
    displayName: string;
  }[];
}

export const MAX_DOLLARS = 1_000_000_000_000;

export const SupportedRestrictionEnumValues: RestrictionValueEnumUIModel[] = [
  {
    valueCode: "C1",
    enums: [
      { displayName: "No measures", value: "0" },
      { displayName: "Recommend closing", value: "1" },
      {
        displayName:
          "Require closing (only some levels or categories, eg just high school, or just public schools)",
        value: "2"
      },
      { displayName: "Require closing all levels", value: "3" }
    ]
  },
  {
    valueCode: "C2",
    enums: [
      { displayName: "No measures", value: "0" },
      { displayName: "Recommend closing (or work from home)", value: "1" },
      {
        displayName:
          "Require closing (or work from home) for some sectors or categories of workers",
        value: "2"
      },
      {
        displayName:
          "Require closing (or work from home) all-butessential workplaces (eg grocery stores, doctors)",
        value: "3"
      }
    ]
  },
  {
    valueCode: "C3",
    enums: [
      { displayName: "No measures", value: "0" },
      { displayName: "Recommend closing", value: "1" },
      { displayName: "Require cancelling", value: "2" }
    ]
  },
  {
    valueCode: "C4",
    enums: [
      { displayName: "No measures", value: "0" },
      {
        displayName:
          "Restrictions on very large gatherings (the limit is above 1000 people)",
        value: "1"
      },
      {
        displayName: "Restrictions on gatherings between 100-1000 people",
        value: "2"
      },
      {
        displayName: "Restrictions on gatherings between 10-100 people",
        value: "3"
      },
      {
        displayName: "Restrictions on gatherings of less than 10 people",
        value: "4"
      }
    ]
  },
  {
    valueCode: "C5",
    enums: [
      { displayName: "No measures", value: "0" },
      {
        displayName:
          "Recommend closing (or significantly reduce volume/route/means of transport available)",
        value: "1"
      },
      {
        displayName:
          "Require closing (or prohibit most citizens from using it)",
        value: "2"
      }
    ]
  },
  {
    valueCode: "C6",
    enums: [
      { displayName: "No measures", value: "0" },
      {
        displayName: "Recommend not leaving house",
        value: "1"
      },
      {
        displayName:
          "Require not leaving house with exceptions for daily exercise, grocery shopping, and ‘essential’ trips",
        value: "2"
      },
      {
        displayName:
          "Require not leaving house with minimal exceptions (e.g. allowed to leave only once every few days, or only one person can leave at a time, etc.)",
        value: "3"
      }
    ]
  },
  {
    valueCode: "C7",
    enums: [
      { displayName: "No measures", value: "0" },
      {
        displayName:
          "Recommend closing (or significantly reduce volume/route/means of transport)",
        value: "1"
      },
      {
        displayName: "Require closing (or prohibit most people from using it)",
        value: "2"
      }
    ]
  },
  {
    valueCode: "C8",
    enums: [
      { displayName: "No measures", value: "0" },
      { displayName: "Screening", value: "1" },
      { displayName: "Quarantine arrivals from high-risk regions", value: "2" },
      { displayName: "Ban on high-risk regions", value: "3" },
      { displayName: "Total border closure", value: "4" }
    ]
  },
  {
    valueCode: "E1",
    enums: [
      { displayName: "No income support", value: "0" },
      {
        displayName: "Government is replacing less than 50% of lost salary",
        value: "1"
      },
      {
        displayName: "Government is replacing 50% or more of lost salar",
        value: "2"
      }
    ]
  },
  {
    valueCode: "E2",
    enums: [
      { displayName: "No", value: "0" },
      {
        displayName: "Narrow relief, specific to one kind of contract",
        value: "1"
      },
      { displayName: "Broad debt/contract relief", value: "2" }
    ]
  },
  {
    valueCode: "H1",
    enums: [
      { displayName: "No COVID-19 public information campaign", value: "0" },
      {
        displayName: "public officials urging caution about COVID-19",
        value: "1"
      },
      {
        displayName:
          "coordinated public information campaign (e.g. across traditional and social media)",
        value: "2"
      }
    ]
  },
  {
    valueCode: "H2",
    enums: [
      { displayName: "No testing policy", value: "0" },
      {
        displayName:
          "Only those who both (a) have symptoms AND (b) meet specific criteria (eg key workers, admitted to hospital, came into contact with a known case, returned from overseas)",
        value: "1"
      },
      {
        displayName: "testing of anyone showing COVID-19 symptoms",
        value: "2"
      },
      {
        displayName:
          "open public testing (eg “drive through” testing available to asymptomatic people)",
        value: "3"
      }
    ]
  },
  {
    valueCode: "H3",
    enums: [
      { displayName: "No contact tracing", value: "0" },
      {
        displayName: "Limited contact tracing - not done for all cases",
        value: "1"
      },
      {
        displayName: "Comprehensive contact tracing - done for all cases",
        value: "2"
      }
    ]
  }
];

const allRestrictions: RestrictionTypeUIModel[] = [
  {
    code: "C",
    displayName: "Closures and Containment",
    values: [
      {
        code: "C1",
        displayName: "School closing",
        validator: v => enumValidator(v, 0, 3),
        type: "enum"
        // 0 - No measures
        // 1 - recommend closing
        // 2 - Require closing (only some levels or categories, eg just high school, or just public schools)
        // 3 - Require closing all levels
      },
      {
        code: "C2",
        displayName: "Workplace closing",
        validator: v => enumValidator(v, 0, 3),
        type: "enum"
        // 0 - No measures
        // 1 - Recommend closing (or work from home)
        // 2 - Require closing (or work from home) for some sectors or categories of workers
        // 3 - Require closing (or work from home) all-butessential workplaces (eg grocery stores, doctors)
        // No data - blank
      },
      {
        code: "C3",
        displayName: "Cancel public events",
        validator: v => enumValidator(v, 0, 5),
        type: "enum"
        // 0- No measures
        // 1 - Recommend cancelling
        // 2 - Require cancelling
        // No data - blank
      },
      {
        code: "C4",
        displayName: "Restrictions on gathering size",
        validator: v => enumValidator(v, 0, 4),
        type: "enum"
        // 0 - No restrictions
        // 1 - Restrictions on very large gatherings (the limit is above 1000 people)
        // 2 - Restrictions on gatherings between 100-1000 people
        // 3 - Restrictions on gatherings between 10-100 people
        // 4 - Restrictions on gatherings of less than 10 people
        // No data - blank
      },
      {
        code: "C5",
        displayName: "Close public transport",
        validator: v => enumValidator(v, 0, 2),
        type: "enum"
        // 0 - No measures
        // 1 - Recommend closing (or significantly reduce volume/route/means of transport available)
        // 2 - Require closing (or prohibit most citizens from using it)
      },
      {
        code: "C6",
        displayName: "Stay at home",
        validator: v => enumValidator(v, 0, 3),
        type: "enum"
        // 0 - No measures
        // 1 - recommend not leaving house
        // 2 - require not leaving house with exceptions for daily exercise, grocery shopping, and ‘essential’ trips
        // 3 - Require not leaving house with minimal exceptions (e.g. allowed to leave only once every few days, or only one person can leave at a time, etc.)
        // No data - blank
      },
      {
        code: "C7",
        displayName: "Restrictions on internal movement",
        validator: v => enumValidator(v, 0, 2),
        type: "enum"
        // 0 - No measures
        // 1 - Recommend closing (or significantly reduce volume/route/means of transport)
        // 2 - Require closing (or prohibit most people from using it)
      },
      {
        code: "C8",
        displayName: "International travel controls",
        validator: v => enumValidator(v, 0, 4),
        type: "enum"
        // 0 - No measures
        // 1 - Screening
        // 2 - Quarantine arrivals from high-risk regions
        // 3 - Ban on high-risk regions
        // 4 - Total border closure
        // No data - blank
      }
    ]
  },
  {
    code: "E",
    displayName: "Economic response",
    values: [
      {
        code: "E1",
        displayName: "Income support",
        validator: v => enumValidator(v, 0, 2),
        type: "enum"
      },
      {
        code: "E2",
        displayName: "Debt/contract relief for households",
        validator: v => enumValidator(v, 0, 2),
        type: "enum"
      },
      {
        code: "E3",
        displayName: "Fiscal measures",
        validator: v => floatValidator(v, 0, MAX_DOLLARS),
        type: "dollars"
      },
      {
        code: "E4",
        displayName: "Giving international support",
        validator: v => floatValidator(v, 0, MAX_DOLLARS),
        type: "dollars"
      }
    ]
  },
  {
    code: "H",
    displayName: "Public health / health system",
    values: [
      {
        code: "H1",
        displayName: "Public information campaign",
        validator: v => enumValidator(v, 0, 2),
        type: "enum"
        // 0 - No COVID-19 public information campaign
        // 1 - public officials urging caution about COVID-19
        // 2 - coordinated public information campaign (e.g. across traditional and social media)
        // No data - blank
      },
      {
        code: "H2",
        displayName: "Testing policy",
        validator: v => enumValidator(v, 0, 3),
        type: "enum"
        // 0 – No testing policy
        // 1 – Only those who both (a) have symptoms AND (b) meet specific criteria (eg key workers, admitted to hospital, came into contact with a known case, returned from overseas)
        // 2 – testing of anyone showing COVID-19 symptoms
        // 3 – open public testing (eg “drive through” testing available to asymptomatic people)
        // No data
      },
      {
        code: "H3",
        displayName: "Contact tracing",
        validator: v => enumValidator(v, 0, 2),
        type: "enum"
        // 0 - No contact tracing
        // 1 - Limited contact tracing - not done for all cases
        // 2 - Comprehensive contact tracing - done for all cases
        // No data
      },
      {
        code: "H4",
        displayName: "Emergency investment in healthcare",
        validator: v => floatValidator(v, 0, MAX_DOLLARS),
        type: "dollars"
      },
      {
        code: "H5",
        displayName: "Investment in Covid-19 vaccines",
        validator: v => floatValidator(v, 0, MAX_DOLLARS),
        type: "dollars"
      }
    ]
  }
];

const supportedTypeKeys = ["C"];
const supportedValueKeys = ["C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8"];

export const SupportedRestrictions: RestrictionTypeUIModel[] = allRestrictions
  .filter(r => supportedTypeKeys.includes(r.code))
  .map(r => ({
    ...r,
    values: r.values.filter(v => supportedValueKeys.includes(v.code))
  }));

export const getValue = (valueCode: string) =>
  SupportedRestrictions.flatMap(t => t.values).find(v => v.code === valueCode);

export const getValueDisplayName = (valueCode: string) =>
  getValue(valueCode)?.displayName || "N/A";

export const getType = (typeCode: string) =>
  SupportedRestrictions.find(t => t.code === typeCode);

export const getTypeDisplayName = (typeCode: string) =>
  getType(typeCode)?.displayName || "N/A";

export const getEnumsForValue = (valueCode: string) => {
  return SupportedRestrictionEnumValues.find(v => v.valueCode === valueCode)
    ?.enums;
};

export const getEnumDisplayName = (
  valueCode: string,
  enumValue: string | number
) => {
  return getEnumsForValue(valueCode)?.find(
    e => e.value.toString() === enumValue.toString()
  )?.displayName;
};
