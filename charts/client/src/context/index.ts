import { makeAutoObservable } from "mobx";
import { ChartStore } from "./stores/chart";
import { RestrictionStore } from "./stores/restrictions";

export class RootStore {
  restrictionStore: RestrictionStore;

  chartStore: ChartStore;

  constructor() {
    this.restrictionStore = new RestrictionStore();
    this.chartStore = new ChartStore();
    makeAutoObservable(this);
  }
}
