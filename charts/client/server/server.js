/* eslint-disable */
import express from 'express';
import cors from 'cors';
// import enforce from 'express-sslify';

const normalizePort = (port) => parseInt(port, 10);
const PORT = normalizePort(process.env.PORT || 3000);
console.log('Port', PORT);
const app = express();
app.use(cors());
// app.use(enforce.HTTPS({ trustProtoHeader: true }));

app.use(express.static(`${__dirname}/dist`));
app.get('*', (req, res) => {
  console.log(req.url);
  const file = `${__dirname}/dist/index.html`;
  res.sendFile(file);
});

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
