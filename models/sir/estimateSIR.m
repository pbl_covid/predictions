function estimateSIR(dateStart,dateEnd,csvFile,saveName,zadaneObostrzenia)
data = readtable(csvFile);

gamma=1/14; % 14 days

dayColumn = 8;
qDays = datenum(dateEnd)-datenum(dateStart);
firstDay = string(table2array(data(1,dayColumn)));
firstDay = strsplit(firstDay,{'T'});
firstDayToSave = datenum(dateStart) - datenum(firstDay(1));
firstDay = datenum(firstDay(1));

populationColumn = 27;
population = table2array(data(1,populationColumn));


%% Calculate days to simulation
oY = size(data,1);

if (datenum(dateStart) - firstDay > oY)
    daysAhead = datenum(dateEnd) - firstDay - oY;
else
    daysAhead = qDays;
end

%% Restrictions Data

firstRestrictionColumn = 9;
lastRestrictionColumn = 23;
restrictions = zeros(size(data,1),12);
for i=firstRestrictionColumn:1:lastRestrictionColumn
    restrictions(:,i-8) = table2array(data(1:end,i));
end

%% Eliminate NANs and INF from restrictions
[oY,oX] = size(restrictions);
for i=1:1:oX
    for j=1:1:oY
        if (isnan(restrictions(j,i)))
            restrictions(j,i) = restrictions(j-1,i);
        end
    end
end

data = table2array(data(:,4:7));


%% Initialize variables to SIR Model
beta = zeros (oY,1);
S = zeros (oY,1);
I = zeros (oY,1);
R = zeros (oY,1);
dS = zeros (oY,1);

%% Moving Average for Confirmed and Active data

Confirmed = zeros(qDays,1);
Confirmed(1,1) = data(firstDayToSave+1,1);

for i=2:1:oY-1
    data(i,4) = sum(data(i-1:i+1,4))/3;
    data(i,1) = sum(data(i-1:i+1,1))/3;
end

%% Initialize simulation from first Infected

startSimulation = 0;
for i=1:1:oY-1
    if (data(i,4) >= 1)
        startSimulation = i;
        break;
    end
end

Io = data(startSimulation,4);

S(startSimulation) = population - Io;
I(startSimulation) = Io;
R(startSimulation) = 0;

%% SIMULATION

    for i=startSimulation:1:oY-1
      
        [S(i+1),I(i+1),R(i+1),dS(i)] = sir_model (S(i),I(i),R(i),beta(i),gamma,population);
       
        if (data(i,4) > 0)
            fun = @(beta) abs((beta*S(i+1)*I(i+1)/population - gamma*I(i+1))+I(i+1) - data(i+1,4));
            beta(i+1) = fminsearch(fun,beta(i));
            
            if (beta(i+1) >=1)
                beta(i+1) = 1;
            end
            if (beta(i+1) <0)
                beta(i+1) = 0;
            end
            
        end      
    end

    %% Moving average for estimated beta

for i=2:1:oY-1 
   beta(i) = (beta(i-1) + beta(i) + beta(i+1)) / 3;
end
%% Split restrictions
splitrest = splitRestrictions(restrictions(:,1),4,true);
splitObostrzenia = splitRestrictions(zadaneObostrzenia(1),4,false);
stages = [4 4 3 5 3 4 3 5];
for i=2:1:8
    splitrest = [splitrest ,splitRestrictions(restrictions(:,i),stages(i),true)];
    splitObostrzenia = [splitObostrzenia,splitRestrictions(zadaneObostrzenia(i),stages(i),false)];
end

%zadaneObostrzenia = [0 0 1 0 0 0 1 0, zadaneObostrzenia(3:end)];
%% Estimate beta by solve system of linear equations

startSimulationBETA = startSimulation+100;
lengthSimulation = oY-startSimulationBETA;

oY = size(restrictions,1);
consVal = ones(oY,1);
restrictionsL = [splitrest consVal];
rozmiar = size(restrictionsL,2);

%x=mldivide (restrictionsL(startSimulationBETA:startSimulationBETA+lengthSimulation,1:rozmiar),beta(startSimulationBETA:startSimulationBETA+lengthSimulation))
%wynik = restrictionsL((startSimulationBETA:startSimulationBETA+lengthSimulation),1:rozmiar)*x;
%% Estimate beta by nonnegative

restrictionsNONNEGATIVE = [-1*splitrest consVal];
 A = restrictionsNONNEGATIVE(startSimulationBETA:startSimulationBETA+lengthSimulation,1:rozmiar);
 B = beta(startSimulationBETA:startSimulationBETA+lengthSimulation);
 x = lsqnonneg(A,B);
 wynik = restrictionsNONNEGATIVE((startSimulationBETA:startSimulationBETA+lengthSimulation),1:rozmiar-1)*x(1:end-1) + x(end);
 

 %% Estimate model SIR with new BETA
 
 estimateBeta = (beta(end) + beta(end-1) + beta(end-2)) / 3;
 estimateBeta = -1*(splitObostrzenia * x(1:end-1)) + x(end);
 if (estimateBeta <= 0 )
     estimateBeta = 0;
 end

 temp = [S(end),I(end),R(end)];
S = [0];
I = [0];
R = [0];
dS = [0];

S = temp(1);
I = temp(2);
R = temp(3);

 for i=1:1:qDays+1
    [S(i+1),I(i+1),R(i+1),dS(i)] = sir_model (S(i),I(i),R(i),estimateBeta,gamma,population);
 end
 

%% Calculate Infected per day from data
infectedData = zeros(1,oY);
for i=2:1:oY
    infectedData(i-1) = data(i,1)-data(i-1,1);
end

%% SAVE to CSV file

time = 0:1:qDays;

sizeY = size(dS(firstDayToSave:end),1);
dS = ceil(abs(dS));

temp = ones (sizeY, 1) * datenum(dateStart);
time = datestr((datenum(dateStart) + time'), 'mm-DD-YYYY');


firstDayToSave = datenum(dateStart) - datenum(firstDay(1));

temp = 0;

 for i=2:1:qDays+1
     temp = Confirmed(i-1,1);
     Confirmed(i,1) = dS(i) + temp;
     
 end
 
 
Date = time;
Infected = num2str(dS');
endCSV = table(Date ,Confirmed , Infected);

writetable(endCSV,[saveName '.csv']);
end