function splitrest = splitRestrictions(restrictions,stages,zadane)
splitrest = zeros (size(restrictions,1),stages);
if (zadane == true)
for i=1:1:size(restrictions,1)
    if (restrictions(i,1)==0)
        splitrest(i,1)=0;
    end
    if (restrictions(i,1)==1)
        splitrest(i,2)=1;
    end
    if (restrictions(i,1)==2)
        splitrest(i,3)=1;
    end
    if (restrictions(i,1)==3)
        splitrest(i,4)=1;
    end
    if (restrictions(i,1)==4)
        splitrest(i,5)=1;
    end
end
else
    for i=1:1:size(restrictions,1)
    if (restrictions(i)==0)
        splitrest(i,1)=0;
    end
    if (restrictions(i)==1)
        splitrest(i,2)=1;
    end
    if (restrictions(i)==2)
        splitrest(i,3)=1;
    end
    if (restrictions(i)==3)
        splitrest(i,4)=1;
    end
    if (restrictions(i)==4)
        splitrest(i,5)=1;
    end
end
end
end