function [S,I,R,dS] = sir_model(S,I,R,beta,gamma,N)

        dS = -beta*I*S/N ;
        dI = beta*I*S/N - gamma*I;
        dR = gamma*I;

        S = S + dS;
        I = I + dI;
        R = R + dR;
end