# SIR Model
> 
This part of project responsible for epidemic model SIR


## General info in step-by-step

- Get information from .csv
- Preparation of simulations for available data
- Optimization of beta parameter
- Quantization of restrictions
- Optimization beta by solve system of linear equation (only negative solve)
- Calculate beta to predicting
- Simulate prediction SIR
- Save results in .csv file

## Technologies
* Matlab - version 2020a3

## Inspiration

The part of application was written while I was studying at Silesian University of Technology. I have to create
SIR model for predicting the development of an epidemic.

## Author
Created by Jarosław Czerniak