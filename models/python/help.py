import pandas as pd
from pmdarima.arima import auto_arima
from statsmodels.tsa.vector_ar.var_model import VAR
import pyflux as pf
import numpy as np


def forecast(tmp_df, date_start,date_end):
    index_forecast = pd.date_range(start=date_start,end=date_end)
    #days_in_future = len(index_forecast)
    model = auto_arima(tmp_df, trace=False, error_action='ignore', suppress_warnings=True)
    model_fit = model.fit(tmp_df)
    forecast, confint = model_fit.predict(n_periods=len(index_forecast), return_conf_int=True)
    res={'Date': index_forecast, 'Predictions': forecast}
    forecast = pd.DataFrame(data=res)
    forecast=forecast.set_index('Date')
    return forecast


def arima_model(date_start, date_end,data):
    data['Date'] = data['LastUpdate'].str[:10]; data=data.set_index('Date'); data=data['Confirmed'];
    tmp_df = data[:date_start]; tmp_df = pd.DataFrame(tmp_df);
    tmp_df = tmp_df[tmp_df['Confirmed']!=0]; 
    fr=forecast(tmp_df, date_start, date_end); fr['Predictions'] = fr['Predictions'].astype(int)
    #fr.columns=['Date','Prediction']
    fr.loc[date_start,:].values[0]=tmp_df.loc[date_start,:].values[0]
    return fr



def var_model(date_start, date_end,data,tmp_vec):
    data['Date'] = data['LastUpdate'].str[:10];
    index_forecast = pd.date_range(start=date_start,end=date_end);
    data=data.set_index('Date'); 
    data=data[['Confirmed','Restriction_C1','Restriction_C2','Restriction_C3','Restriction_C4','Restriction_C5','Restriction_C6','Restriction_C7','Restriction_C8']]
    tmp_df = data[:list(data.index.values).index(date_start)+1]; tmp_df = pd.DataFrame(tmp_df);

    tmp_df = tmp_df[tmp_df['Confirmed'] != 0];
    endogenious=tmp_df['Confirmed']
    exogenious=tmp_df.loc[:, tmp_df.columns != 'Confirmed']
    
    columns=[]
    for col_name in data.columns: 
        columns.append(col_name)

    new_restrictions = pd.DataFrame([tmp_vec])
    new_restrictions=pd.DataFrame(np.repeat(new_restrictions.values, len(index_forecast), axis=0),columns=new_restrictions.columns,index=index_forecast.values)
    new_restrictions=new_restrictions.to_numpy()
    
    #model = VAR(tmp_df)
    model = VAR(endog=tmp_df,exog=exogenious)
    model_fit = model.fit(trend='ctt', method='ols')
    prediction = model_fit.forecast(model_fit.y, steps=len(index_forecast), exog_future=new_restrictions)
    df1=prediction[:,0]
    res = {'Date': index_forecast,'Predictions': df1 }
    df = pd.DataFrame(res, columns = ['Date', 'Predictions'])
    df2 = pd.DataFrame(res, columns = ['Date', 'Predictions'])
    df2=df.set_index('Date')
    df2.loc[date_start,:].values[0]=tmp_df.loc[date_start,:].values[0]
    
    
    def trendline(data, order=1):
        coeffs = np.polyfit(data.index.values, list(data), order)
        slope = coeffs[-2]
        return float(slope)
    
    slope = trendline(df['Predictions'])
    
    
    if slope < 0:
        tmp_vec=[5,5,5,5,5,5,5,5];
        new_restrictions = pd.DataFrame([tmp_vec])
        new_restrictions=pd.DataFrame(np.repeat(new_restrictions.values, len(index_forecast), axis=0),columns=new_restrictions.columns,index=index_forecast.values)
        new_restrictions=new_restrictions.to_numpy()
        
        prediction = model_fit.forecast(model_fit.y, steps=len(index_forecast), exog_future=new_restrictions)
        df1=prediction[:,0]
        res = {'Date': index_forecast,'Predictions': df1 }
        df = pd.DataFrame(res, columns = ['Date', 'Predictions'])
        df2 = pd.DataFrame(res, columns = ['Date', 'Predictions'])
        df2=df.set_index('Date')
        df2.loc[date_start,:].values[0]=tmp_df.loc[date_start,:].values[0]
        
        
    return(df2)



def forecast_arimax(tmp_df, date_start,date_end,tmp_vec):
    index_forecast = pd.date_range(start=date_start,end=date_end)
    #days_in_future = len(index_forecast)
    df_input=tmp_df
    columns = tmp_df.columns
    rows = index_forecast
    tmp_vec = np.insert(tmp_vec, 0, 0)
    df_tmp = pd.DataFrame([tmp_vec,tmp_vec])
    df_tmp=df_tmp.drop([0, 0])
    df_tmp = pd.DataFrame(np.repeat(df_tmp.values, len(index_forecast), axis=0),columns=tmp_df.columns,index=index_forecast.values)
    df_tmp=df_tmp.iloc[1:]
    tmp_df = pd.concat([tmp_df, df_tmp])
    model = pf.ARIMAX(data=df_input, formula='Confirmed~Restriction_C1+Restriction_C2+Restriction_C3+Restriction_C4+Restriction_C5+Restriction_C6+Restriction_C7+Restriction_C8', ar=1, ma=1, family=pf.Normal())
    model_fit=model.fit("MLE")
    model_fit.summary()
    forecast= model.predict(h=len(index_forecast),oos_data=tmp_df, intervals=False)
    df1=forecast.iloc[:,0]
    res = {'Date': index_forecast,'Predictions': df1 }
    df = pd.DataFrame(res, columns = ['Date', 'Predictions'])
    df=df.set_index('Date')
    #forecast = pd.DataFrame(forecast, index=index_forecast, columns=['pred'])
    return df



def arimax_model(date_start, date_end,data,tmp_vec):
    data['Date'] = data['LastUpdate'].str[:10];
    data=data.set_index('Date'); 
    data=data[['Confirmed','Restriction_C1','Restriction_C2','Restriction_C3','Restriction_C4','Restriction_C5','Restriction_C6','Restriction_C7','Restriction_C8']]
    tmp_df = data[:list(data.index.values).index(date_start)+1]; tmp_df = pd.DataFrame(tmp_df);
    tmp_df = tmp_df[tmp_df['Confirmed']!=0]; 
    fr=forecast_arimax(tmp_df, date_start, date_end,tmp_vec); 
    #fr['pred'] = fr['pred'].astype(int)
    fr.loc[date_start,:].values[0]=tmp_df.loc[date_start,:].values[0]
    return fr

