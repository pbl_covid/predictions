import pandas as pd
from help import arima_model, forecast, arimax_model, forecast_arimax, var_model
import matplotlib.pyplot as plt
import numpy as np

data = pd.read_csv('DEU_1.csv')

#przykładowy wejciowy wektor obostrzeń
tmp_vec=[0,0,0,0,0,0,0,0]

date_start='2020-08-01'
date_end='2020-11-30'

results = arima_model(date_start,date_end, data)
#results.to_csv("results_arima_1_bel.csv")

results = arimax_model(date_start,date_end,data,tmp_vec)
#results.to_csv("results_arimax_1_bel.csv")

results = var_model(date_start,date_end, data, tmp_vec)
#results.to_csv("results_var_1_bel.csv")


