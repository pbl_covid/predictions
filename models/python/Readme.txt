1. Plik help.py zawiera funkcje, które obejmują stworzenie modeli ARIMA, ARIMAX, VAR oraz predykcje. 
Funkcje przyjmują argumenty: date_start (data początku okresu predykcji), date_end (data końca okresu predykcji),
data (plik .csv zawierający dane dotyczące analizowanego kraju), vec (wektor z wartościami każdego z dostępnych
w aplikacji obostrzeń, które mogą być wybierane przez użytkownika)
2. Plik test.py zawiera przykładowe wywołanie funkcji odpowiednich dla każdego z modeli.
