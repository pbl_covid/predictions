# Python PblCovid19

# Configure dev env (commands should be runned into project dir)

## Linux
1. sudo apt-get install python3-pip python3-tk
2. sudo pip3 install virtualenv
3. virtualenv venv
4. source venv/bin/activate
5. ./venv/bin/activate
6. pip install -r requirements.txt

## Windows

1. Install Python3
2. `pip install virtualenv`
3. virtualenv venv
4. `./venv/bin/activate.ps1`
5. `pip install -r requirements.txt`

# Updating requirements.txt (required only when you add new pip dependency)

1. pip freeze > requirements.txt

# Run app

`python app.py`

# Debug app
`flask run`

# Docker support

Python api can be run as a docker container with `dockerfile` with default port set to 5000, commands to use:
1. `docker build -t python-api .`
2. `docker run --rm -d -p 5000:5000 python-api`

# How to add a new model endpoint
Make a new endpoint in `app.py` file.
```python
@app.route('/MODEL_NAME/<dateFrom>/<dateTo>', methods=['POST'])
def arima(dateFrom, dateTo):
    try:
        requestJson = request.json
        data = pd.json_normalize(requestJson['Data'])
        results = YOUR_MODEL(dateFrom, dateTo, data)
        responseJson = results.to_json()
        return responseJson
    except Exception as ex:
        logger.error(ex, exc_info=True)
        print(ex)
        return Response(str(ex), status=500)
```