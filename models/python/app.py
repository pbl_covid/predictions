from flask import Flask, request, Response
import pandas as pd
import json
from help import arima_model, forecast, arimax_model, forecast_arimax, var_model
import logging
import os
import traceback
from importlib.metadata import version

app = Flask(__name__)

logFormatter = logging.Formatter(
    "%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
logger = logging.getLogger()
logger.setLevel(10)

fileHandler = logging.FileHandler("logs.log")
fileHandler.setFormatter(logFormatter)
logger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
logger.addHandler(consoleHandler)


@app.route('/arima/<dateFrom>/<dateTo>', methods=['POST'])
def arima(dateFrom, dateTo):
    try:
        requestJson = request.json
        data = pd.json_normalize(requestJson['Data'])
        results = arima_model(dateFrom, dateTo, data)
        responseJson = results.to_json()
        return responseJson
    except Exception as ex:
        logger.error(ex, exc_info=True)
        print(ex)
        return Response(str(ex), status=500)


@app.route('/arimax/<dateFrom>/<dateTo>', methods=['POST'])
def arimax(dateFrom, dateTo):
    try:
        requestJson = request.json
        data = pd.json_normalize(requestJson['Data'])
        restrictions = requestJson['Restrictions']
        logger.debug(restrictions)
        results = arimax_model(dateFrom, dateTo, data, restrictions)
        responseJson = results.to_json()
        return responseJson
    except Exception as ex:
        logger.error(ex, exc_info=True)
        print(ex)
        return Response(str(ex), status=500)


@app.route('/var/<dateFrom>/<dateTo>', methods=['POST'])
def var(dateFrom, dateTo):
    try:
        requestJson = request.json
        data = pd.json_normalize(requestJson['Data'])
        restrictions = requestJson['Restrictions']
        logger.debug(restrictions)
        results = var_model(dateFrom, dateTo, data, restrictions)
        responseJson = results.to_json()
        return responseJson
    except Exception as ex:
        logger.error(ex, exc_info=True)
        print(ex)
        return Response(str(ex), status=500)


@app.route('/healthz', methods=['GET', 'POST'])
def healthz():
    print("Health check")
    return "OK"


@app.route("/", methods=['GET', 'POST'])
def default():
    print("Health check")
    return "OK"

@app.route("/packages", methods=['GET', 'POST'])
def packages():
    modules = {
        "pandas" : version('pandas'),
        'pmdarima' : version('pmdarima')
    }
    logger.debug(modules)
    return Response(str(modules))

if __name__ == "__main__":
    from waitress import serve
    serve(app, host="0.0.0.0", port=5000)
