interface PolicyDetailsDto {
  policy_type_code: string;
  policy_type_display: string;
  policyvalue: string;
  policyvalue_actual: any;
  flagged: boolean;
  is_general: boolean;
  notes: string;
  flag_value_display_field: string;
  policy_value_display_field: string;
}
interface StringencyDataDto {
  date_value: string;
  country_code: string;
  confirmed: number;
  deaths: number;
  stringency_actual: number;
  stringency: number;
}

interface PolicyResponseDto {
  policyActions: PolicyDetailsDto[];
  stringencyData: StringencyDataDto;
}

interface RestrictionPolicy {
  name: string;
  value: string;
}

interface RestrictionsForDay {
  restrictions: RestrictionPolicy;
  date: Date;
}
