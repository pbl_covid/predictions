interface CovidDataRow {
  provinceState: string;
  // countryRegion: string;
  city: string;
  cityCode: string;
  lastUpdate: string;
  confirmed: string;
  deaths: string;
  recovered: string;
  active: string;
  // incidentRate: String;
  // fatalityRatio: String;
}

interface CovidDataDbModel {
  Province: string;
  City: string;
  CityCode: string;
  LastUpdate: Date;
  Confirmed: number;
  Active: number;
  Deaths: number;
  Recovered: number;
  Restriction_C1: string;
  Restriction_C2: string;
  Restriction_C3: string;
  Restriction_C4: string;
  Restriction_C5: string;
  Restriction_C6: string;
  Restriction_C7: string;
  Restriction_C8: string;
  Restriction_E1: string;
  Restriction_E2: string;
  Restriction_E3: string;
  Restriction_E4: string;
  Restriction_H1: string;
  Restriction_H2: string;
  Restriction_H3: string;
  Restriction_H4: string;
  Restriction_H5: string;
  Restriction_M1: string;
}

interface CovidDataDbModelWithPopulation extends CovidDataDbModel {
  population: string;
}

interface AvailableCountryDto {
  Name: string;
  Code: string;
  ViewName: string;
}

interface CountryDetails {
  code: string;
  alpha3: string;
}
