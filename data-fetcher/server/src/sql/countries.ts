import { ConnectionPool } from 'mssql';
import { checkIfTableExistsAsync, createDbConnectionAsync } from './common';
import queries from './queries';
import { sanitizeSqlTableName } from './utility';

export const addAvailableCountry = async (
  connection: ConnectionPool,
  countryDisplayName: string,
  countryCode: string,
  alpha3: string
) => {
  if (!(await checkIfTableExistsAsync(connection, queries.tables.countries))) {
    await connection.query(queries.countries.createTable);
  }
  const query = queries.countries.add(
    countryDisplayName,
    sanitizeSqlTableName(countryCode),
    alpha3
  );
  await connection.query(query);
};

export const getAvailableCountriesAsync = async (): Promise<
  AvailableCountryDto[]
> => {
  const connection = await createDbConnectionAsync();
  const { recordset } = await connection.query<AvailableCountryDto>(
    queries.countries.getAll
  );
  const result = recordset.map((r) => r);
  await connection.close();

  return result;
};

export const updateLastRestrictionUpdateDateAsync = async (
  connection: ConnectionPool,
  restrictionsUpdateDate: Date,
  countryAlpha3: string
) => {
  const query = queries.restrictions.updateLastUpdateDate(
    countryAlpha3,
    restrictionsUpdateDate
  );

  await connection.query(query);
};
