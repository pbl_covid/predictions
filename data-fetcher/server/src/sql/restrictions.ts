import { ConnectionPool } from 'mssql';
import queries from './queries';
import { sanitizeSqlTableName } from './utility';

export const updateRestrictionsAsync = async (
  connection: ConnectionPool,
  countryTableName: string,
  date: Date,
  policies: RestrictionPolicy[]
): Promise<boolean> => {
  const query = queries.restrictions.update(
    date,
    sanitizeSqlTableName(countryTableName),
    policies
  );
  const res = await connection.query(query);
  if (res.rowsAffected.length >= 1) {
    return true;
  }
  return false;
};
