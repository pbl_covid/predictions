import { ConnectionPool } from 'mssql';
import { createDbConnectionAsync } from './common';
import queries from './queries';
import { sanitizeSqlTableName } from './utility';

const _getDataAsync = async (
  connection: ConnectionPool,
  table: string,
  startingDate: string,
  endingDate: string
): Promise<CovidDataDbModel[]> => {
  const query = queries.baseData.get(
    sanitizeSqlTableName(table),
    startingDate != null ? new Date(startingDate) : void 0,
    startingDate != null ? new Date(endingDate) : void 0
  );

  const { recordset } = await connection.query<CovidDataDbModel>(query);
  const result = recordset.map((r) => r);

  return result;
};

const getDataTableNameForAlphaAsync = async (
  connection: ConnectionPool,
  alpha: string
): Promise<string> => {
  const query = queries.countries.getForAlpha(alpha);
  const { recordset } = await connection.query<AvailableCountryDto>(query);
  const { Code, ViewName } = recordset[0];

  if (ViewName != null && ViewName != '') {
    return ViewName;
  } else {
    return Code;
  }
};

export const getDataAsync = async (
  alpha3: string,
  startingDate: string,
  endingDate: string
): Promise<CovidDataDbModel[]> => {
  const connection = await createDbConnectionAsync();
  const table = await getDataTableNameForAlphaAsync(connection, alpha3);
  const result = await _getDataAsync(
    connection,
    table,
    startingDate,
    endingDate
  );
  connection.close();
  return result;
};

export const insertDataAsync = async (
  connection: ConnectionPool,
  data: CovidDataRow[],
  tableName: string
) => {
  // TODO: run migration SQL
  const batchSize = 100;
  // const transaction = await connPool.transaction().begin();
  try {
    while (data.length > 0) {
      const batched = data.splice(0, batchSize);

      const query = queries.baseData.insert(
        sanitizeSqlTableName(tableName),
        batched
      );

      await connection.query(query);
    }

    // transaction.commit();
  } catch (error) {
    // transaction.rollback();
    console.error('SQL Data insert error', error);
    throw error;
  }
};

export const getLatestUpdateDateAsync = async (
  connection: ConnectionPool,
  country: string
): Promise<[boolean, Date]> => {
  const query = queries.baseData.lastUpdateDate(sanitizeSqlTableName(country));
  const res = await connection.query(query);
  if (res.recordset.length === 1) {
    return [true, new Date(res.recordset[0]['LastUpdate'])];
  }
  return [false, new Date()];
};

export const getLatestRestrictionUpdateDateAsync = async (
  connection: ConnectionPool,
  countryAlpha3: string
): Promise<[boolean, Date?]> => {
  const query = queries.restrictions.lastUpdateDate(countryAlpha3);
  const res = await connection.query(query);

  if (res.recordset.length === 1) {
    const lastUpdateDate = res.recordset[0]['RestrictionsLastUpdate'];
    if (lastUpdateDate != null) {
      return [true, new Date(lastUpdateDate)];
    }
    return [true, undefined];
  }

  return [false, undefined];
};
