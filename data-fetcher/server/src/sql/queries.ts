import { formatSqlDate, sanitizeRow } from './utility';

const createDataTableForCountryQuery = (tableName: string) =>
  `CREATE TABLE ${tableName} 
  (Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
  Province varchar(255), City varchar(255), CityCode varchar(255), Confirmed INT, Deaths INT, Recovered INT, Active INT, LastUpdate Date, 
  Restriction_C1 varchar(255) DEFAULT '0', Restriction_C2 varchar(255) DEFAULT '0', Restriction_C3 varchar(255) DEFAULT '0', Restriction_C4 varchar(255) DEFAULT '0', Restriction_C5 varchar(255) DEFAULT '0', Restriction_C6 varchar(255) DEFAULT '0', Restriction_C7 varchar(255) DEFAULT '0', Restriction_C8 varchar(255) DEFAULT '0', 
  Restriction_E1 varchar(255) DEFAULT '0', Restriction_E2 varchar(255) DEFAULT '0', Restriction_E3 varchar(255) DEFAULT '0', Restriction_E4 varchar(255) DEFAULT '0', 
  Restriction_H1 varchar(255) DEFAULT '0', Restriction_H2 varchar(255) DEFAULT '0', Restriction_H3 varchar(255) DEFAULT '0', Restriction_H4 varchar(255) DEFAULT '0', 
  Restriction_H5 varchar(255) DEFAULT '0', Restriction_M1 varchar(255))`;

const availableCountriesTableName = 'countries';
const createAvailableCountriesTableQuery =
  `CREATE TABLE ${availableCountriesTableName}` +
  ' (Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY, Name varchar(255), Code varchar(255), Alpha3 varchar(255), RestrictionsLastUpdate Date, ViewName varchar(255))';

const getAvailableCountriesQuery = `SELECT * FROM ${availableCountriesTableName}`;
const getCountriesForAlphaQuery = (alpha3: string) =>
  `${getAvailableCountriesQuery} WHERE Alpha3 like UPPER('${alpha3}')`;

const baseDataColumns =
  'Province, City, CityCode, Confirmed, Deaths, Recovered, Active, LastUpdate';

const baseDataWithRestrictionsColumns =
  baseDataColumns +
  ',' +
  'Restriction_C1, Restriction_C2, Restriction_C3, Restriction_C4, Restriction_C5, Restriction_C6, Restriction_C7, Restriction_C8,' +
  'Restriction_E1, Restriction_E2, Restriction_E3, Restriction_E4,' +
  'Restriction_H1, Restriction_H2, Restriction_H3, Restriction_H4, Restriction_H5,' +
  'Restriction_M1';

const addAvailableCountryQuery = (
  countryDisplayName: string,
  countryCode: string,
  alpha3: string
) =>
  `INSERT INTO ${availableCountriesTableName} (Name, Code, Alpha3) 
  VALUES ('${countryDisplayName}', '${countryCode}', '${alpha3}')`;

const updateRestrictionsQuery = (
  date: Date,
  countryTableName: string,
  policies: RestrictionPolicy[]
) => {
  const setValues = policies
    .map((p) => `Restriction_${p.name} = '${p.value}'`)
    .join(',');

  return `UPDATE ${countryTableName} SET ${setValues} WHERE LastUpdate='${formatSqlDate(
    date
  )}'`;
};

const getDataForCountryQuery = (
  countryTableName: string,
  startingDate?: Date,
  endingDate?: Date
) => {
  let query = `SELECT ${baseDataWithRestrictionsColumns} FROM ${countryTableName}`;

  if (startingDate != null && endingDate != null) {
    query += ` WHERE LastUpdate >= '${formatSqlDate(
      startingDate
    )}' AND LastUpdate <= '${formatSqlDate(endingDate)}'`;
  }
  query += ' ORDER BY LastUpdate';
  return query;
};

const insertDataForCountryQuery = (
  countryTableName: string,
  data: CovidDataRow[]
) => {
  data.forEach((row) => sanitizeRow(row));

  const sqlData = data
    .map(
      (d) =>
        `('${d.provinceState}','${d.city}','${d.cityCode}',${d.confirmed},${d.deaths},${d.recovered},${d.active},'${d.lastUpdate}')`
    )
    .join(',');

  return `INSERT INTO ${countryTableName} (${baseDataColumns}) VALUES ${sqlData}`;
};
const lastBaseDataUpdateDateQuery = (countryTableName: string) => {
  return `SELECT TOP 1 LastUpdate FROM ${countryTableName} ORDER BY LastUpdate DESC`;
};

const lastRestrictionsUpdateDateQuery = (countryAlpha3: string) => {
  return `SELECT TOP 1 RestrictionsLastUpdate FROM countries WHERE Alpha3='${countryAlpha3}'`;
};

const updateLastRestrictionsUpdateDateQuery = (
  countryAlpha3: string,
  date: Date
) => {
  return `UPDATE countries SET RestrictionsLastUpdate='${formatSqlDate(
    date
  )}' WHERE Alpha3='${countryAlpha3}'`;
};

const checkIfTableExistsResultName = 'BOOL';
const checkIfTableExists = (tableName: string) =>
  `IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='${tableName}')) ` +
  `SELECT 1 AS ${checkIfTableExistsResultName} ELSE SELECT 0 AS ${checkIfTableExistsResultName}`;

const queries = {
  tables: {
    countries: availableCountriesTableName,
    exists: checkIfTableExists,
    existsColumnName: checkIfTableExistsResultName
  },
  columns: {
    baseData: baseDataColumns,
    baseDataWithRestrictions: baseDataWithRestrictionsColumns
  },
  countries: {
    add: addAvailableCountryQuery,
    getAll: getAvailableCountriesQuery,
    getForAlpha: getCountriesForAlphaQuery,
    createTable: createAvailableCountriesTableQuery
  },
  baseData: {
    createTable: createDataTableForCountryQuery,
    get: getDataForCountryQuery,
    insert: insertDataForCountryQuery,
    lastUpdateDate: lastBaseDataUpdateDateQuery
  },
  restrictions: {
    update: updateRestrictionsQuery,
    lastUpdateDate: lastRestrictionsUpdateDateQuery,
    updateLastUpdateDate: updateLastRestrictionsUpdateDateQuery
  }
};

export default queries;
