import { ConnectionPool } from 'mssql';
import { addAvailableCountry } from './countries';
import queries from './queries';
import { sanitizeSqlTableName } from './utility';

export const createDbConnectionAsync = async (): Promise<ConnectionPool> => {
  let connPool: ConnectionPool;
  const connectionString = process.env.SQL_STRING;
  if (connectionString) {
    connPool = new ConnectionPool(connectionString);
  } else {
    connPool = new ConnectionPool({
      database: process.env.SQL_DATABASE!,
      user: process.env.SQL_USER!,
      password: process.env.SQL_PASSWORD!,
      port: Number(process.env.SQL_PORT),
      server: process.env.SQL_SERVER!
    });
  }

  if (!connPool.connected) {
    try {
      const connected = await connPool.connect();
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
  return connPool;
};

export async function checkIfTableExistsAsync(
  connection: ConnectionPool,
  tableName: string
): Promise<boolean> {
  const query = queries.tables.exists(sanitizeSqlTableName(tableName));
  const { recordset } = await connection.query(query);

  return (
    recordset.length === 1 && recordset[0][queries.tables.existsColumnName] == 1
  );
}

export const createTableForCountryAsync = async (
  connection: ConnectionPool,
  countryCode: string,
  countryAlpha3: string
): Promise<boolean> => {
  if (await checkIfTableExistsAsync(connection, countryCode)) {
    return false;
  }
  await addAvailableCountry(
    connection,
    countryCode,
    countryCode,
    countryAlpha3
  );
  const query = queries.baseData.createTable(sanitizeSqlTableName(countryCode));
  const { rowsAffected } = await connection.query(query);

  return rowsAffected.length === 1 && rowsAffected[0] === 1;
};
