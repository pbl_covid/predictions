import { parse } from 'csv-string';
import { Parser } from 'json2csv';

export const parseCsv = (csvContent: string): any => {
  return parse(csvContent, ',');
};

export const createCsv = <T>(data: any[]): string => {
  const parser = new Parser<T>();
  const csv = parser.parse(data);
  return csv;
};
