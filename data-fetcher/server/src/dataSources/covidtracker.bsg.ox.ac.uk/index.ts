import axios from 'axios';
import dateFormat from 'dateformat';
import { asyncRetry, msleep } from '../../utility';

const VALID_POLICIES = [
  'C1',
  'C2',
  'C3',
  'C4',
  'C5',
  'C6',
  'C7',
  'C8',
  'E1',
  'E2',
  'E3',
  'E4',
  'H1',
  'H2',
  'H3',
  'H4',
  'H5',
  'M1'
];
const EMPTY_POLICY = 'NONE';
const TIMEOUT = 30000;

const urls = {
  PolicyUrl: (code: string, date: Date) =>
    `https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/actions/${code}/${dateFormat(
      date,
      'yyyy-mm-dd'
    )}`
};

export const getRestrictionForDayAsync = async (
  countryAlpha3Code: string,
  date: Date
): Promise<[boolean, RestrictionPolicy[]]> => {
  try {
    const policies = await asyncRetry(() =>
      _getRestrictionForDayAsync(countryAlpha3Code, date)
    );
    return [true, policies];
  } catch (err) {
    console.error(err);
    return [false, []];
  }
};

const _getRestrictionForDayAsync = async (
  countryAlpha3Code: string,
  date: Date
): Promise<RestrictionPolicy[]> => {
  const url = urls.PolicyUrl(countryAlpha3Code, date);
  msleep(500); // dont overcook API
  const { data, status } = await axios.get<PolicyResponseDto>(url, {
    timeout: TIMEOUT
  });

  if (status === 200) {
    const validPolicies = data.policyActions
      .filter((p) => VALID_POLICIES.includes(p.policy_type_code.toUpperCase()))
      .map((p) => ({
        name: p.policy_type_code,
        value: p.policyvalue
      }));

    return validPolicies;
  } else {
    throw 'Error while retrieving policies';
  }
};
