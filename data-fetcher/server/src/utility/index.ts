export function msleep(n: number) {
  Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n);
}

export function sleep(n: number) {
  msleep(n * 1000);
}

export async function asyncRetry<T>(
  promiseFactory: () => Promise<T>,
  waitSeconds: number = 10
): Promise<T> {
  let retries = 3;
  let result: any = undefined;

  while (true) {
    try {
      result = await promiseFactory();
      break;
    } catch (error) {
      console.log(error);
      if (retries <= 0) {
        throw error;
      }
      sleep(waitSeconds);
    }

    --retries;
  }

  return result;
}

export const retry = (retryFunction: Function): any => {
  let retries = 3;
  let result: any = undefined;

  while (true) {
    try {
      result = retryFunction();
      break;
    } catch (error) {
      console.error(error);
      if (retries <= 0) {
        throw error;
      }
      sleep(10);
    }

    --retries;
  }

  return result;
};
