// import './aliases';
import dotenv from 'dotenv';
dotenv.config();
import { job, jobStatus, syncAllAsync } from './cron';
import express from 'express';
import { createCsv } from './csv';
import cors from 'cors';
import { getAvailableCountriesAsync } from './sql/countries';
import { getDataAsync } from './sql/baseData';
import Axios from 'axios';

const jobEnabled = (process.env.JOB_ENABLED || '').toLowerCase() == 'true';
const apiEnabled = (process.env.API_ENABLED || '').toLowerCase() == 'true';

const settings: {
  jobEnabled: boolean;
  apiEnabled: boolean;
  lastJobRun?: Date;
  lastJobFinished?: Date;
} = {
  jobEnabled,
  apiEnabled,
  lastJobRun: undefined,
  lastJobFinished: undefined,
};
const normalizePort = (port: any) => parseInt(port, 10);
const port = normalizePort(process.env.PORT || 3000);

const app = express();
app.use(cors());

if (jobEnabled) {
  app.post('/force', async (req, res) => {
    if (!jobStatus.isRunning) {
      settings.lastJobRun = new Date();
      jobStatus.isRunning = true;
      res.json('Force synchronization started...');
      syncAllAsync().then(() => {
        jobStatus.isRunning = false;
        settings.lastJobFinished = new Date();
      });
    } else {
      res.json('Force synchronization is already running...');
    }
  });

  job.start();
}

if (apiEnabled) {
  app.get('/countries', async (req, res) => {
    const countries = await getAvailableCountriesAsync();
    res.json(countries);
  });

  app.get('/csv', async (req, res) => {
    const { alpha, start, end } = req.query;
    const data: CovidDataDbModel[] = await getDataAsync(
      alpha as string,
      start as string,
      end as string
    );

    const population = await (
      await Axios.get(`https://restcountries.eu/rest/v2/alpha/${alpha}`)
    ).data.population;

    const csv = createCsv<CovidDataDbModelWithPopulation>(
      data.map((d) => ({ ...d, population }))
    );
    res.header('Content-Type', 'text/csv');
    res.send(csv);
  });
}

app.get('/healthz', async (req, res) => {
  const result = { ...settings };
  result.lastJobRun =
    job.lastDate() > settings.lastJobRun!
      ? job.lastDate()
      : settings.lastJobRun;
  console.log(result);
  res.json(result);
});

app.listen(
  port,
  () => `Covid service started on port ${port} with settings: ${settings}`
);
