import { CronJob } from 'cron';
import { getData, STARTING_DATE } from '../dataSources/covid19api';
import { ConnectionPool } from 'mssql';

import COUNTRY_DATA from '../countries.json';
import { getRestrictionForDayAsync } from '../dataSources/covidtracker.bsg.ox.ac.uk';
import {
  checkIfTableExistsAsync,
  createDbConnectionAsync,
  createTableForCountryAsync
} from '../sql/common';
import {
  getLatestRestrictionUpdateDateAsync,
  getLatestUpdateDateAsync,
  insertDataAsync
} from '../sql/baseData';
import { updateRestrictionsAsync } from '../sql/restrictions';
import { updateLastRestrictionUpdateDateAsync } from '../sql/countries';

const DAYS_STEP = Number(process.env.SYNCHRONIZATION_PERIOD || 7);

export const jobStatus = {
  isRunning: false
};

export const job = new CronJob(
  '0 0 23 * * *' /* 23:00 everyday */,
  async () => {
    if (!jobStatus.isRunning) {
      console.log('Cron job invoked.', new Date());
      jobStatus.isRunning = true;
      await syncAllAsync();
      jobStatus.isRunning = false;
    } else {
      console.log(
        'Cron job skipped, synchronization is already running.',
        new Date()
      );
    }
  },
  null,
  false
);

const getMaxSyncDate = () => {
  const today = new Date();
  today.setDate(today.getDate() - 1);
  return today;
};

function getNextDay(date: Date) {
  var result = new Date(date.getFullYear(), date.getMonth(), date.getDate());
  result.setDate(result.getDate() + 1);
  return result;
}

function getNextStepDate(date: Date, step: number | null = null) {
  const maxDate = getMaxSyncDate();
  var result = new Date(date.getFullYear(), date.getMonth(), date.getDate());
  result.setDate(result.getDate() + (step || DAYS_STEP));

  return result > maxDate ? maxDate : result;
}

async function updateDataFromDate(
  connection: ConnectionPool,
  startingDate: Date,
  country: string
) {
  const today = new Date();

  // check for last date in DB
  let sDate = startingDate;
  let eDate = getNextStepDate(sDate);
  while (!dateEquals(today, sDate) && sDate < today) {
    console.log(
      'Updating data between',
      sDate.toLocaleDateString(),
      eDate.toLocaleDateString()
    );
    const [success, data] = await getData(sDate, eDate, country);
    if (success) {
      await insertDataAsync(connection, data, country);
      sDate = getNextDay(eDate);
      eDate = getNextStepDate(sDate);
    } else {
      throw `Could not fetch data for ${country} on ${sDate}`;
    }
  }
}
const updateAllRestrictionsAsync = async () => {
  const connection = await createDbConnectionAsync();
  const updatedCountries = [];
  const failedCountries = [];

  for (const k in COUNTRY_DATA) {
    const country = (COUNTRY_DATA as any)[k] as CountryDetails;
    console.log('Synchronizing restrictions for:', country.code, new Date());
    console.log('Starting restrictions synchronization for:', country.code);

    try {
      await synchronizeRestrictionsAsync(
        connection,
        country.code,
        country.alpha3
      );
      updatedCountries.push(country.code);
    } catch (err) {
      console.error(err);
      failedCountries.push(country.code);
    }
  }
  connection.close();
  console.log('SUCCESFUL RESTRICTIONS MIGRATIONS:', updatedCountries);
  console.log('FAILED RESTRICTIONS MIGRATIONS:', failedCountries);
};

const updateRestrictionsFromDateAsync = async (
  connection: ConnectionPool,
  startingDate: Date,
  countryCode: string,
  countryAlpha3: string
): Promise<Date | null> => {
  const today = new Date();
  let lastUpdateDate: Date | null = null;
  let indexDate = startingDate;

  while (!dateEquals(today, indexDate) && indexDate < today) {
    console.log('Updating restrictions for ', indexDate.toLocaleDateString());

    const [success, policies] = await getRestrictionForDayAsync(
      countryAlpha3,
      indexDate
    );

    if (success) {
      if (policies.length > 0) {
        const result = await updateRestrictionsAsync(
          connection,
          countryCode,
          indexDate,
          policies
        );

        if (result) {
          lastUpdateDate = indexDate;
        }
      }
      indexDate = getNextDay(indexDate);
    } else {
      throw `Could not fetch data for ${countryCode} on ${indexDate}`;
    }
  }

  return lastUpdateDate;
};
export const syncAllAsync = async () => {
  await synchronizeAllDataAsync();
  await updateAllRestrictionsAsync();
};

export async function synchronizeAllDataAsync() {
  const connection = await createDbConnectionAsync();
  const updatedCountries = [];
  const failedCountries = [];

  for (const k in COUNTRY_DATA) {
    const country = (COUNTRY_DATA as any)[k] as CountryDetails;
    console.log('Synchronizing data for:', country.code, new Date());
    console.log('Starting data synchronization for:', country.code);

    try {
      await synchronizeDataAsync(connection, country.code, country.alpha3);
      updatedCountries.push(country.code);
    } catch (err) {
      console.error(err);
      failedCountries.push(country.code);
    }
  }
  connection.close();
  console.log('SUCCESFUL DATA MIGRATIONS:', updatedCountries);
  console.log('FAILED DATA MIGRATIONS:', failedCountries);
}

const synchronizeDataAsync = async (
  connection: ConnectionPool,
  countryCode: string,
  countryAlpha3: string
): Promise<void> => {
  if (!(await checkIfTableExistsAsync(connection, countryCode))) {
    await createTableForCountryAsync(connection, countryCode, countryAlpha3);
  }
  let [success, startingDate] = await getLatestUpdateDateAsync(
    connection,
    countryCode
  );

  if (success) {
    startingDate = getNextDay(startingDate);
  } else {
    startingDate = STARTING_DATE;
  }
  await updateDataFromDate(connection, startingDate, countryCode);
};

const synchronizeRestrictionsAsync = async (
  connection: ConnectionPool,
  countryCode: string,
  countryAlpha3: string
) => {
  if (!(await checkIfTableExistsAsync(connection, countryCode))) {
    await createTableForCountryAsync(connection, countryCode, countryAlpha3);
  }
  // get last update day
  let [success, startingDate] = await getLatestRestrictionUpdateDateAsync(
    connection,
    countryAlpha3
  );
  if (!success) {
    throw `Could not retrieve last restriction update on ${countryCode}`;
  }

  startingDate = startingDate ? getNextDay(startingDate) : STARTING_DATE;

  // update restrictions up to today
  const lastUpdateDate = await updateRestrictionsFromDateAsync(
    connection,
    startingDate,
    countryCode,
    countryAlpha3
  );

  // set last update day
  if (lastUpdateDate) {
    await updateLastRestrictionUpdateDateAsync(
      connection,
      lastUpdateDate,
      countryAlpha3
    );
  }
};

function dateEquals(dateA: Date, dateB: Date) {
  return (
    dateA.getFullYear() === dateB.getFullYear() &&
    dateA.getMonth() === dateB.getMonth() &&
    dateA.getDate() == dateB.getDate()
  );
}
