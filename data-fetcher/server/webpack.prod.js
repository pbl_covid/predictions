/* eslint-disable */
const path = require('path');
// const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  target: 'node',
  module: {
    rules: [
      {
        test: /\.ts/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  mode: 'production',
  entry: './src/server.ts',
  output: {
    path: path.resolve(__dirname, './prod/'),
    filename: 'server.js',
    publicPath: '/'
  }
};
