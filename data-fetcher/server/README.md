# ABOUT

API for downloading .csv COVID data from MSSQL database. It also has serwice worker running data synchronizing job everyday at 11 PM.

# ENVIRONMENT VARIABLES
__! Following variables can be also added to .env file instead__
- PORT - PORT used for exposing API
- API_ENABLED - values `true` or `false` => enables/disables API
- JOB_ENABLED - values `true` or `false` => enables/disables service worker
- SQL_STRING - MSSQL connection string 

_(if `SQL_STRING` will not be supplied, the following variables will apply)_

- SQL_DATABASE - MSSQL databse name
- SQL_USER - MSSQL user name
- SQL_PASSWORD - MSSQL user password
- SQL_PORT - MSSQL port
- SQL_SERVER - MSSQL server name

# HOW TO RUN MANUALLY

Requirements: __Node LTS__, __yarn__

1. Run following commands: 
- yarn install
- yarn start

2. __OR__ Create production build in `prod` folder:
- yarn install
- yarn build:prod

# HOW TO RUN WITH DOCKER
Requirements: __Docker 20.10__

Api can be also run in docker: 
- `docker build -t data-fetcher-api .`
- `docker run -p 5000:3000 -d data-fetcher-api`

# AFTER

It is advised to run synchronization job on the first run using POST request to `/force` and waiting until GET request `/healthz` returns date in `lastJobFinished` 