import React from 'react';
import { Input } from 'antd';

const { Search } = Input;

interface Props {
  searchCallback: (searchTerm: string) => void;
}

const SearchBar = ({ searchCallback }: Props): JSX.Element => {
  return (
    <Search
      placeholder="Search for country"
      allowClear
      enterButton="Search"
      size="large"
      onSearch={searchCallback}
    />
  );
};

export default SearchBar;
