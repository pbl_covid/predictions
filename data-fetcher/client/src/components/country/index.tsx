import React, { useEffect, useState } from 'react';
import { DatePicker, Card, Space, Button, Typography } from 'antd';
import { DownloadOutlined } from '@ant-design/icons';
import moment, { Moment } from 'moment';
import './style.scss';
import { getCsvFileForAll, getCsvFileForRange } from 'src/api';

const { RangePicker } = DatePicker;
const { Text } = Typography;
interface Props {
  countryDisplay: string;
  country: string;
}

const firstAvailableDate = moment('2020-01-22', 'YYYY-MM-DD');
const now = moment();

const Country = ({ country, countryDisplay }: Props): JSX.Element => {
  const [startDate, setStartDate] = useState<string>();
  const [endDate, setEndDate] = useState<string>();
  const [disabled, setDisabled] = useState(false);
  const [loading, setLoading] = useState(false);

  const rangeChange = (_: any, dates: string[]) => {
    const [start, end] = dates;
    setStartDate(start);
    setEndDate(end);
  };

  useEffect(() => {
    if (startDate && endDate) {
      setDisabled(false);
    } else if (!disabled) {
      setDisabled(true);
    }
  }, [startDate, endDate]);

  const disabledTime = (date: Moment) => {
    return date.isBefore(firstAvailableDate) || date.isAfter(now);
  };

  const downloadFile = () => {
    setLoading(true);
    getCsvFileForRange(country, startDate!, endDate!).finally(() =>
      setLoading((_) => false)
    );
  };

  const downloadFileForAll = () => {
    setLoading(true);
    getCsvFileForAll(country).finally(() => setLoading((_) => false));
  };

  return (
    <Card
      className="country-card"
      title={countryDisplay}
      style={{ width: 300 }}
    >
      <Space direction="vertical">
        <RangePicker
          showNow
          onChange={rangeChange}
          disabledDate={disabledTime as any}
        />
        <Button
          disabled={disabled || loading}
          onClick={downloadFile}
          type="primary"
          icon={<DownloadOutlined />}
          size={'large'}
        >
          Download For Range
        </Button>
        {disabled && (
          <Text style={{ fontStyle: 'italic' }}>
            Please enter valid date range...
          </Text>
        )}
        <Button
          disabled={loading}
          onClick={downloadFileForAll}
          type="primary"
          icon={<DownloadOutlined />}
          size={'large'}
        >
          Download All
        </Button>
      </Space>
    </Card>
  );
};

export default Country;
