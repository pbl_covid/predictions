import axios from 'axios';
import fileDownload from 'js-file-download';
const API_URL = process.env.REACT_APP_API_URL || 'http://localhost:5000';

const getCsvUrl = (alpha: string, startDate: string, endDate: string) =>
  `${API_URL}/csv?alpha=${alpha}&start=${startDate}&end=${endDate}`;

const getCsvForAllUrl = (alpha: string) => `${API_URL}/csv?alpha=${alpha}`;

export const getAllCountries = async (): Promise<CountryDto[]> => {
  const { data, status } = await axios.get<CountryDto[]>(
    `${API_URL}/countries`
  );
  if (status !== 200) {
    console.error('Failed when requesting available countries');
    return [];
  }

  return data;
};

export const getCsvFileForRange = async (
  countryCode: string,
  startDate: string,
  endDate: string
): Promise<void> => {
  await downloadFile(getCsvUrl(countryCode, startDate, endDate), countryCode);
};

export const getCsvFileForAll = async (countryCode: string): Promise<any> => {
  return downloadFile(getCsvForAllUrl(countryCode), countryCode);
};

async function downloadFile(url: string, country: string): Promise<void> {
  await axios.get(url).then((response) => {
    console.log(response);
    return fileDownload(response.data, `${country}.csv`);
  });
}
