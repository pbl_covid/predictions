interface CountryDto {
  Name: string;
  Code: string;
  ViewName: string;
  Alpha3: string;
}
