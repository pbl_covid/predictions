import React from 'react';
import './App.scss';
import 'antd/dist/antd.min.css';
import { Layout, Menu, Breadcrumb } from 'antd';
import CsvDownloadsPage from 'src/pages/csv-downloads';
const { Header, Content, Footer } = Layout;

function App(): JSX.Element {
  return (
    <Layout className="layout">
      <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
          <Menu.Item key="1">CSV Downloads</Menu.Item>
          <Menu.Item disabled key="2">
            Synchronization
          </Menu.Item>
        </Menu>
      </Header>
      <Content style={{ padding: '0 50px' }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>CSV Downloads</Breadcrumb.Item>
        </Breadcrumb>
        <div className="site-layout-content">
          <CsvDownloadsPage />
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>
        Data fetcher ©2020 Created by ppatch
      </Footer>
    </Layout>
  );
}

export default App;
