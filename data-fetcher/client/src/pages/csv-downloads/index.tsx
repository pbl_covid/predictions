import React, { useEffect, useState } from 'react';
import SearchBar from 'src/components/search-bar';
import Country from 'src/components/country';
import { getAllCountries } from 'src/api';

const CsvDownloadsPage = (): JSX.Element => {
  const [countries, setCountries] = useState<CountryDto[]>([]);
  const searchCallback = (search: string) => setState(search);
  const [search, setState] = useState('');
  const getCountries = (): CountryDto[] => {
    return countries.filter((c: CountryDto) => c.Name.includes(search));
  };

  useEffect(() => {
    getAllCountries().then((data) => setCountries(data));
  }, []);

  return (
    <>
      <SearchBar searchCallback={searchCallback} />
      {getCountries().map((c) => (
        <Country country={c.Alpha3} countryDisplay={c.Name} />
      ))}
    </>
  );
};

export default CsvDownloadsPage;
