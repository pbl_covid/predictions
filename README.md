# ABOUT

Application was created as a part of Project Base Learning on Silesian University of Technology with purpose of Modeling and predicting the development of a viral epidemic on the example of the SARS-CoV-2 pandemic

# INFRASTRUCTURE 
![Alt text](docs/system_infrastructure.png?raw=true "System Infrastructure")

# FOLDER STRUCTURE

- `charts`
Project folders for main API and main Web client
    - `api` Main API project
    - `client` Main Client project
- `data-fetcher` Reponsible for synchronizing COVID data with external APIs. Also used as the source of data for main API.
    - `server` Data fetcher API
    - `client` Data fetcher Web client
- `models` Prediction models
    - `sir` Sir model files written in matlab 
    - `python` Python service with implemented Var, Arima and Arimax models
- `docker` Docker compose for applicationa and matlab runtime images for ASP.NET Core
- `backups`
Database backup files for main MSSQL database 
- `docs`
System infrastructure diagram

# HOW TO RUN

1. Easiest way to run application is via `docker compose` tool. Instructions on how to set up whole project can be found in `docker/README.md`.

2. __Alternatively__ every service can be run separately, but take much more effort:

You will need runnning: 

_(each of the followig services has its own README file in its folder with additional instructions)_:
- MSSQL Databse with latest backup from `backups` folder
- Main API from `charts/api`
- Main Web client from `charts/client`
- Data fetcher API from `data-fetcher/server`
- (optionally) Data fetcher Web client `data-fetcher/client`
- Python models API from `models/python`
