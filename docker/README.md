# How to run:

__STEPS 2 AND 3 ARE ONLY REQUIRED ON THE FIRST STARTUP__
1. Run docker-compose: `docker-compose up -d`
It will run:
- Main Web Client on `http://localhost:5000`
- Main API on `http://localhost:5001`
- Data Fetcher API on `http://localhost:5002`
- Data Fetcher Web Client on `http://localhost:5003`
- Python API on `http://localhost:5004`
- MsSQL database on `localhost,1533`

2. Apply database backup from `backups` folder to MsSQL on localhost:1533 using credentials sa/polsl20201!
3. Run synchronization job by invoking __POST__ request to `http://localhost:5002/force` 
- wait until response on `http://localhost:5002/healthz` shows `lastJobFinished` with date
4. You are ready to go on `http://localhost:5000`
5. Additional info:
- To stop app use `docker-compose down`
- To show logs use `docker-compose logs -f`

# Matlab runtime images

In `/docker-images` there are 2 dockerfiles with ASP.NET Core 3.1 and MCR 2020b installed. It is used as a base image for `chart/api` API.

Other docker file (`aspnetcore3.1_matlab-runtime.DEBUG.dockerfile`) file can be used as a debugging image for Main API. 